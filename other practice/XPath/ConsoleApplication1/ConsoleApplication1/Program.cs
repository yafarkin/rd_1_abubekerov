﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("xp.xml");
            XmlElement xRoot = xDoc.DocumentElement;

            // выбор всех дочерних узлов
            XmlNodeList childnodes = xRoot.SelectNodes("user");
            
            foreach (XmlNode n in childnodes)
                Console.WriteLine(n.SelectSingleNode("@name").Value);

            XmlNode childnode = xRoot.SelectSingleNode("user[@name='Bill Gates']");
            if (childnode != null)
                Console.WriteLine(childnode.OuterXml);


            XmlNode childnode2 = xRoot.SelectSingleNode("user[company='Google']");
            if (childnode2 != null)
                Console.WriteLine(childnode2.OuterXml);


            XmlNodeList childnodes3 = xRoot.SelectNodes("//user/company");
            foreach (XmlNode n in childnodes3)
                Console.WriteLine(n.InnerText);

            Console.ReadKey();
        }
    }
}
