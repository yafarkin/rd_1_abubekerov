﻿USE Northwind
GO

SELECT (
		SELECT FirstName + ' ' + LastName
		FROM Employees
		WHERE EmployeeID = ord.EmployeeID
		) AS NAME
	,COUNT(OrderID)
FROM [dbo].[Orders] ord
GROUP BY EmployeeID
HAVING EmployeeID IS NOT NULL

GO