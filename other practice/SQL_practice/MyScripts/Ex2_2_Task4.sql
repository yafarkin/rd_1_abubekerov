﻿USE Northwind
GO

SELECT cst.CustomerID
	,emp.FirstName
	,emp.City
FROM [dbo].[Customers] cst
INNER JOIN [dbo].[Employees] emp ON emp.City = cst.City
GROUP BY cst.CustomerID
	,emp.FirstName
	,emp.City

GO