﻿USE Northwind
GO

SELECT e1.LastName as Employee
	,e2.LastName as Chief
FROM [dbo].[Employees] e1
LEFT JOIN [dbo].[Employees] e2 ON e1.ReportsTo = e2.EmployeeID
order by Chief

GO