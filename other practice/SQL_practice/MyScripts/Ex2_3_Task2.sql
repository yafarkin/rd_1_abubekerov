﻿USE Northwind
GO

SELECT cst.CompanyName
	,COUNT(ord.CustomerID) AS [Count]
FROM [dbo].[Customers] cst
LEFT JOIN [dbo].[Orders] ord ON cst.CustomerID = ord.CustomerID
GROUP BY cst.CompanyName
ORDER BY [Count]

GO