﻿USE Northwind
GO

SELECT CompanyName
FROM [dbo].[Customers] cu
WHERE NOT EXISTS (
		SELECT CustomerID
		FROM [dbo].[Orders] ord
		WHERE cu.CustomerID = ord.CustomerID
		)

GO