﻿USE Northwind
GO

SELECT 
(
select FirstName +' ' + LastName
from Employees
) as 'Продавец'

, COUNT(*) as 'Количество заказов'
FROM [Orders]
where EmployeeID IS NOT NULL
group by CustomerID
--HAVING EmployeeID IS NOT NULL

GO