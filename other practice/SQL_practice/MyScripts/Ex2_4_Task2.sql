﻿USE Northwind
GO

SELECT LastName
FROM [dbo].[Employees]
WHERE EmployeeID IN (
		SELECT EmployeeID
		FROM [dbo].[Orders]
		GROUP BY EmployeeID
		HAVING count(EmployeeID) > 150
		)

GO