﻿USE Northwind
GO

SELECT CompanyName
FROM [dbo].[Suppliers]
WHERE SupplierID IN (
		SELECT pr.SupplierID
		FROM [dbo].[Products] pr
		WHERE UnitsInStock = 0
		)

GO