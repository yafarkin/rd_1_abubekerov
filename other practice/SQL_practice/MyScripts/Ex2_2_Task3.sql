﻿USE Northwind
GO

SELECT ord.CustomerID
	,ord.EmployeeID
	,COUNT(OrderID)
FROM [dbo].[Orders] ord
WHERE cast(YEAR(OrderDate) AS VARCHAR(4)) = '1998'
GROUP BY CustomerID
	,EmployeeID

GO