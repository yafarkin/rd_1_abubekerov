﻿USE Northwind
GO

SELECT (emp.FirstName + ' ' + emp.LastName) AS FullName
	,rg.[RegionDescription]
FROM [dbo].[Employees] emp
INNER JOIN [dbo].[EmployeeTerritories] et ON et.EmployeeID = emp.EmployeeID
INNER JOIN [dbo].[Territories] trr ON trr.TerritoryID = et.TerritoryID
INNER JOIN [dbo].[Region] rg ON rg.[RegionID] = trr.RegionID
WHERE rg.[RegionDescription] = 'Western'
GROUP BY (emp.FirstName + ' ' + emp.LastName)
	,rg.[RegionDescription]

GO