﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(angularTask.Startup))]
namespace angularTask
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
