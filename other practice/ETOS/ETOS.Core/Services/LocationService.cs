﻿using System;
using System.Collections.Generic;
using ETOS.Core.DTO;
using ETOS.Core.Services.Abstract;
using ETOS.DAL.Interfaces;
using ETOS.DAL.Entities;
using AutoMapper;
using System.Linq;

namespace ETOS.Core.Services
{
    public class LocationService : ILocationService
    {
        #region Fields 

        private readonly IRepository<Location> _locationsRepository;

        #endregion

        #region Constructor
        public LocationService(IRepository<Location> locationRep)
        {
            _locationsRepository = locationRep;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Возвращает все локации
        /// </summary>
        /// <returns></returns>
        public List<DtoLocation> GetAllLocations()
        {
            var allLocations = _locationsRepository.GetAll();

            Mapper.Initialize(cfg => cfg.CreateMap<Location, DtoLocation>());
            var outLocationsList = Mapper.Map<IEnumerable<Location>, List<DtoLocation>>(allLocations);
                        
            return outLocationsList;
        }


        /// <summary>
        /// Возвращает только точки интереса
        /// </summary>
        /// <returns></returns>
        public List<DtoLocation> GetOnlyPoi()
        {
            var poiList = _locationsRepository.GetAll().Where(c=>c.IsPOI);
            
            Mapper.Initialize(cfg => cfg.CreateMap<Location, DtoLocation>());
            var outPoiList = Mapper.Map<IEnumerable<Location>, List<DtoLocation>>(poiList);

            return outPoiList;
        }



        #endregion
    }
}
