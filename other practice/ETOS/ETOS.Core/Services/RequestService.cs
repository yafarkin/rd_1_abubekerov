﻿using ETOS.Core.DTO;
using ETOS.Core.Services.Abstract;
using ETOS.DAL.Entities;
using ETOS.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;


namespace ETOS.Core.Services
{
    /// <summary>
    /// Сервис, реализующий функционал для работы с заявками пользователей системы на уровне бизнес-логики.
    /// </summary>
    public class RequestService : IRequestService
    {
        #region Fields 

        private readonly IRepository<Request> _requestRepository;

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор сервиса по работе с заявками
        /// </summary>
        public RequestService(IRepository<Request> requestRep)
        {
            _requestRepository = requestRep;
        }

        #endregion

        #region Methods
                   

        /// <summary>
        /// Метод,реализующий получение списка заявок с учётом фильтрации.
        /// </summary>
        /// <param name="filter">Условие фильтрования.</param>
        public List<DtoRequest> GetRequest(FilterSettings filter)
        {
            var filterList = _requestRepository.Find(x => (filter.RequestId == 0) || (x.Id == filter.RequestId));
            filterList.Where(x => (filter.AuthorId == 0) || (x.AuthorId == filter.AuthorId)).
                       Where(x => (filter.DeparturePointId == null) || (x.DeparturePointId == filter.DeparturePointId)).
                       Where(x => (filter.DestinationPointId == null) || (x.DestinationPointId == filter.DestinationPointId)).
                       Where(x => (filter.DepartureAddress == null) || (x.DepartureAddress == filter.DepartureAddress)).
                       Where(x => (filter.DestinationAddress == null) || (x.DestinationAddress == filter.DestinationAddress)).
                       Where(x => (filter.DepartureDateTime == DateTime.MinValue) || (x.DepartureDateTime == filter.DepartureDateTime)).
                       Where(x => (filter.CreatingDateTime == DateTime.MinValue) || (x.CreatingDateTime == filter.CreatingDateTime)).
                       Where(x => (filter.HasBaggage == false) || (x.HasBaggage == filter.HasBaggage)).
                       Where(x => (filter.Comment == null) || (x.Comment == filter.Comment)).
                       Where(x => (filter.Mileage == 0) || (x.Mileage == filter.Mileage)).
                       Where(x => (filter.Price == 0) || (x.Price == filter.Price)).
                       Where(x => (filter.StatusId ==0) || (x.StatusId == filter.StatusId));

            // Настройка AutoMapper
            Mapper.Initialize(cfg => cfg.CreateMap< Request, DtoRequest>());
            var outList = Mapper.Map<IEnumerable<Request>, List<DtoRequest>>(filterList);
            return outList;            
        }

        /// <summary>
        /// Метод отрабатывает вызов реализации добавления заявки через репозиторий
        /// </summary>
        /// <param name="item"></param>
        public void AddRequest(DtoRequest item)
        {
            var createItem = Mapper.Map<DtoRequest, Request>(item);
            _requestRepository.Create(createItem);
        }

        /// <summary>
        /// Метод отрабатывает вызов реализации удаления заявки через репозиторий
        /// </summary>
        /// <param name="item"></param>
        public void DeleteRequest(DtoRequest item)
        {
            var deleteItem = Mapper.Map<DtoRequest, Request>(item);
            _requestRepository.Delete(deleteItem.Id);
        }

        #endregion
    }
}
