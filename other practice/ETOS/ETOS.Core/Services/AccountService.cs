﻿using System.Linq;

using ETOS.Core.DTO;
using ETOS.Core.Services.Abstract;

using ETOS.DAL.Interfaces;

namespace ETOS.Core.Services
{
    /// <summary>
    /// Сервис, реализующий функционал для работы с учетными данными пользователей системы на уровне бизнес-логики.
    /// </summary>
    public class AccountService : IAccountService
    {
        #region Fields

        /// <summary>
        /// Экземпляр репозитория аккаунтов.
        /// </summary>
        private IAccountRepository _accountRepository;

        #endregion

        #region Constructor

        public AccountService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий авторизацию пользователя в системе.
        /// </summary>
        /// <param name="account">Информация пользователя</param>        
        /// <returns>true - в случае успешной авторизации, false - в противном случае. </returns>
        public bool Authorize(DtoAccount account)
        {
            return (_accountRepository
                   .Find(acc => (acc.Username == account.Username && acc.Password == account.Password))
                   .FirstOrDefault()) != null;
        }

        /// <summary>
        /// Возвращает Id пользователя по его логину в системе, если такой есть в БД
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="result">true - если найден такой пользователь, false - если нет</param>
        /// <returns></returns>
        public bool GetIdbyLogin(string login, out int result)
        {
            result = 0;
            var foundAccount = _accountRepository.Find(c => c.Username == login).FirstOrDefault();

            if (foundAccount != null)
            {
                result = foundAccount.Id;
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion
    }
}
