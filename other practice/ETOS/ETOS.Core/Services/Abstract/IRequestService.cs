﻿using ETOS.Core.DTO;
using System.Collections.Generic;

namespace ETOS.Core.Services.Abstract
{
    /// <summary>
    /// Интерфейс сервиса по работе с заявками пользователей.
    /// </summary>
    public interface IRequestService
    {
        List<DtoRequest> GetRequest(FilterSettings filter);

        void AddRequest(DtoRequest item);

        void DeleteRequest(DtoRequest item);
    }
}
