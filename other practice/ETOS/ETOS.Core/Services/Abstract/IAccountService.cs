﻿using ETOS.Core.DTO;

namespace ETOS.Core.Services.Abstract
{
    /// <summary>
    /// Интерфейс сервиса по работе с учетными записями пользователей.
    /// </summary>
    public interface IAccountService 
    {
        /// <summary>
        /// Метод, реализующий авторизацию пользователя в системе.
        /// </summary>
        /// <param name="account">Учетная запись для авторизации.</param>
        /// <returns>true - в случае успешной авторизации, false - в противном случае. </returns>
        bool Authorize(DtoAccount account);

        /// <summary>
        /// Возвращает Id пользователя по его логину в системе, если такой есть в БД
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="result">true - если найден такой пользователь, false - если нет</param>
        /// <returns></returns>
        bool GetIdbyLogin(string login, out int result);
    }
}
