﻿using ETOS.Core.DTO;
using ETOS.DAL.Entities;
using System.Collections.Generic;

namespace ETOS.Core.Services.Abstract
{
    public interface ILocationService
    {
        List<DtoLocation> GetAllLocations();

        List<DtoLocation> GetOnlyPoi();
    }
}
