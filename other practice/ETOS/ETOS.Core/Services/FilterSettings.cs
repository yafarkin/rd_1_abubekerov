﻿using System;
using ETOS.Core.Services.Abstract;

namespace ETOS.Core.Services
{
    /// <summary>
    /// Содержит поля для фильтрации
    /// </summary>
    public class FilterSettings 
    {
        public int AuthorId { get; set; }
        public string Comment { get; set; }
        public DateTime CreatingDateTime { get; set; }
        public string DepartureAddress { get; set; }
        public DateTime DepartureDateTime { get; set; }
        public int? DeparturePointId { get; set; }
        public string DestinationAddress { get; set; }
        public int? DestinationPointId { get; set; }
        public bool HasBaggage { get; set; }
        public double Mileage { get; set; }
        public decimal Price { get; set; }
        public int RequestId { get; set; }
        public int StatusId { get; set; }
    }
}
