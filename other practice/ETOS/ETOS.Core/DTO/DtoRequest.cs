﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ETOS.DAL.Entities;

namespace ETOS.Core.DTO
{
    /// <summary>
    /// Объект передачи данных между моделью уровня представления и уровнем доступа к данным.
    /// Представляет собой аккаунт пользователя.
    /// </summary>
    public class DtoRequest
    {
        /// <summary>
        /// Идентефикационный номер заявки
        /// </summary>
        public int RequestId { get; set; }

        /// <summary>
        /// Идентефикационный номер автора заявки
        /// </summary>
        public int AuthorId { get; set; }

        /// <summary>
        /// Код точки отправки
        /// </summary>
        public Nullable<int> DeparturePointId { get; set; }

        /// <summary>
        /// Код точки прибытия
        /// </summary>
        public Nullable<int> DestinationPointId { get; set; }

        /// <summary>
        /// Адрес точки отправки
        /// </summary>
        public string DepartureAddress { get; set; }
        /// <summary>
        /// Адрес точки прибытия
        /// </summary>
        public string DestinationAddress { get; set; }
        /// <summary>
        /// Время отправки
        /// </summary>
        public DateTime DepartureDateTime { get; set; }
        /// <summary>
        /// Время прибытия
        /// </summary>
        public DateTime CreatingDateTime { get; set; }
        /// <summary>
        /// Наличие багажа,да/нет
        /// </summary>
        public bool IsBaggage { get; set; }
        /// <summary>
        /// Комментарии к заявке
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Километраж
        /// </summary>
        public double Mileage { get; set; }
        /// <summary>
        /// Общая сумма за поездку
        /// </summary>
        public double Price { get; set; }
        /// <summary>
        /// Идентефикационный номер статуса
        /// </summary>
        public int StatusId { get; set; } 
    }
}
