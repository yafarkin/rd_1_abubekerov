﻿using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// Описывает сущность "Приоритет".
	/// Данная сущность описывает приориет обслуживания сотрудников.
	/// </summary>
	public class Priority : IEntity
	{
		#region Fields

		/// <summary>
		/// Код приоритета.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Название приоритета.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Описание приоритета.
		/// </summary>
		public string Description { get; set; }

		#endregion

		#region Navigation properties

		#endregion
	}
}
