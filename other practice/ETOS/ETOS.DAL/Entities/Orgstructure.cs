﻿using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// Описывает сущность "Оргструктура".
	/// Данная сущность обозначает оргструктуру, входящую в состав компании.
	/// </summary>
	public class Orgstructure : IEntity
	{
		#region Fields

		/// <summary>
		/// Код оргструктуры.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Название оргструктуры.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Код оргструктуры, являющейся родительской по отношению к данной.
		/// </summary>
		public int ? ParentOrgstructureId { get; set; }

		#endregion

		#region Navigation properties

		public virtual Orgstructure ParentOrgstructure { get; set; }

		#endregion

	}
}
