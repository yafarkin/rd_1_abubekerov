﻿using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// Описывает сущность "Локация".
	/// Данная сущность обозначает локацию, в которой может находиться сотрудник
	/// или место, непосредственно связанное с деятельностью фирмы, в которое сотрудник может заказать поездку.
	/// </summary>
	public class Location : IEntity
	{
		#region Fields

		/// <summary>
		/// Код локации.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Название локации.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Адрес локации.
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// Код локации, являющейся родительской по отношению к данной.
		/// </summary>
		public int ? ParentLocationId { get; set; }

		/// <summary>
		/// Флаг, определяющий является ли локация точкой интереса
		/// </summary>
		public bool IsPOI { get; set; }

		/// <summary>
		/// Системный код культуры локации.
		/// </summary>
		public int CultureId { get; set; }

		#endregion

		#region Navigation properties

		public virtual Location ParentLocation { get; set; }

		#endregion
	}
}
