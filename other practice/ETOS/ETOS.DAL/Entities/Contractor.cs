﻿using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// Описывает сущность "Подрядчик".
	/// Данная сущность описывает фирму, непосредственно занимающуюся транспортировкой сотрудников компании.
	/// </summary>
	public class Contractor : IEntity
	{
		#region Fields

		/// <summary>
		/// Код подрядчика.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Название подрядчика.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Номер телефона подрядчика.
		/// </summary>
		public string Phone { get; set; }

		/// <summary>
		/// Стоимость, устанавливаемая подрядчиком за километр поездки.
		/// </summary>
		public decimal Tariff { get; set; }

		#endregion

		#region Navigation properties

		#endregion
	}
}
