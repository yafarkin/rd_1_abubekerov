using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// ��������� �������� "���������".
	/// ������ �������� ������������ ����� ���������� ��������.
	/// </summary>
	public class Employee : IEntity
	{
		#region Fields

		/// <summary>
		/// ��������� ����� ����������.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// ������� ����������.
		/// </summary>
		public string Lastname { get; set; }

		/// <summary>
		/// ��� ����������.
		/// </summary>
		public string Firstname { get; set; }

		/// <summary>
		/// �������� ����������.
		/// </summary>
		public string Patronymic { get; set; }

		/// <summary>
		/// ����� �������� ����������.
		/// </summary>
		public string Phone { get; set; }

		/// <summary>
		/// ����� ����������� ����� ����������.
		/// </summary>
		public string Email { get; set; }
		
		/// <summary>
		/// ��� �������, � ������� ��������� ���������.
		/// </summary>
		public int LocationId { get; set; }

		/// <summary>
		/// ��� ������������, �� ������� ��������� ���������.
		/// </summary>
		public int OrgstructureId { get; set; }

		/// <summary>
		/// ��� ���������, ���������� �����������.
		/// </summary>
		public int PositionId { get; set; }

		#endregion

		#region Navigation properties

		public virtual Location Location { get; set; }
		public virtual Orgstructure Orgstructure { get; set; }
		public virtual Position Position { get; set; }

		#endregion
	}
}
