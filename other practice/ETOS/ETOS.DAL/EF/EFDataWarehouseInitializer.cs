﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

using ETOS.DAL.Entities;

namespace ETOS.DAL.EF
{
	/// <summary>
	/// Реализует инициализатор базы данных.
	/// </summary>
	public class EFDataWarehouseInitializer : DropCreateDatabaseIfModelChanges<EFDataWarehouseContext>
	{
		/// <summary>
		/// Метод, реализующий заполнение базы начальными данными при её создании.
		/// </summary>
		protected override void Seed(EFDataWarehouseContext database)
		{

			var _orgstructureRus = new Orgstructure { Name = "EPAM" };
			var _orgstructueSamOblast = new Orgstructure { Name = "Самарский регион", ParentOrgstructure = _orgstructureRus };
			var _orgstructueSam = new Orgstructure { Name = "Самарский DEV офис ", ParentOrgstructure = _orgstructueSamOblast };
			var _orgstructueSamDev = new Orgstructure { Name = "Software development", ParentOrgstructure = _orgstructueSam };
			var _orgstructueSamDevJava = new Orgstructure { Name = "Java R&D", ParentOrgstructure = _orgstructueSamDev };
			var _orgstructueSamDevNet = new Orgstructure { Name = ".Net Framework R&D", ParentOrgstructure = _orgstructueSamDev };
			var _orgstructueTlt = new Orgstructure { Name = "Тольяттинский DEV офис", ParentOrgstructure = _orgstructueSamOblast };
			var _orgstructueTltDev = new Orgstructure { Name = "Software development", ParentOrgstructure = _orgstructueTlt };
			var _orgstructueTltDevJava = new Orgstructure { Name = "Java R&D", ParentOrgstructure = _orgstructueTltDev };
			var _orgstructueTltDevNet = new Orgstructure { Name = ".Net Framework R&D", ParentOrgstructure = _orgstructueTltDev };
			var _orgstructueMskOblast = new Orgstructure { Name = "Московский регион", ParentOrgstructure = _orgstructureRus };
			var _orgstructueMsk = new Orgstructure { Name = "Московский DEV офис ", ParentOrgstructure = _orgstructueMskOblast };
			var _orgstructueMskDev = new Orgstructure { Name = "Software development", ParentOrgstructure = _orgstructueMsk };
			var _orgstructueMskDevJava = new Orgstructure { Name = "Java R&D", ParentOrgstructure = _orgstructueMskDev };
			var _orgstructueMskDevNet = new Orgstructure { Name = ".Net Framework R&D", ParentOrgstructure = _orgstructueMskDev };

			List<Orgstructure> _newOrgstructures = new List<Orgstructure>
			{
				_orgstructureRus,
				_orgstructueSamOblast,
				_orgstructueSam,
				_orgstructueSamDev,
				_orgstructueSamDevJava,
				_orgstructueSamDevNet,
				_orgstructueTlt,
				_orgstructueTltDev,
				_orgstructueTltDevJava,
				_orgstructueTltDevNet,
				_orgstructueMskOblast,
				_orgstructueMsk
			};

			database.Orgstructures.AddRange(_newOrgstructures);


			var _rusLoc = new Location { Name = "Россия", CultureId = 12, IsPOI = false };
			var _smroblLoc = new Location { Name = "Самарская область", CultureId = 12, IsPOI = false, ParentLocation = _rusLoc };
			var _mskoblLoc = new Location { Name = "Московская область", CultureId = 12, IsPOI = false, ParentLocation = _rusLoc };
			var _smrLoc = new Location { Name = "Самара", CultureId = 12, IsPOI = false, ParentLocation = _smroblLoc };
			var _mskLoc = new Location { Name = "Москва", CultureId = 12, IsPOI = false, ParentLocation = _mskoblLoc };
			var _tltLoc = new Location { Name = "Тольятти", CultureId = 12, IsPOI = false, ParentLocation = _smroblLoc };
			var _tltEPAMLoc = new Location { Name = "EPAM_TLT", Address = "г.Тольятти ул.Юбилейная 31Е", CultureId = 12, IsPOI = true, ParentLocation = _tltLoc };
			var _smrEPAMLoc = new Location { Name = "EPAM_SMR", Address = "г.Самара ул.Мичурина 21", CultureId = 12, IsPOI = true, ParentLocation = _smrLoc };
			var _mskEPAMLoc = new Location { Name = "EPAM_MSK", Address = "г.Москва ул.Генераторов 6", CultureId = 12, IsPOI = true, ParentLocation = _mskLoc };
			var _airportLoc = new Location { Name = "Аэропорт Курумоч", Address = "Самарская обл. аэропорт Курумоч", CultureId = 12, IsPOI = true, ParentLocation = _smroblLoc };
			var _busLoc = new Location { Name = "Автовокзал Аврора", Address = "Тольятти ул.70 лет октября Автостанция Аврора", CultureId = 12, IsPOI = true, ParentLocation = _tltLoc };
			var _clientLoc = new Location { Name = "компания Рога и Копыта", Address = "Тольятти ул.Юбилейная 178", CultureId = 12, IsPOI = true, ParentLocation = _tltLoc };


			List<Location> _newLocations = new List<Location>
			{
				_rusLoc,
				_smroblLoc,
				_mskoblLoc,
				_smrLoc,
				_mskLoc,
				_tltLoc,
				_tltEPAMLoc,
				_smrEPAMLoc,
				_mskEPAMLoc,
				_airportLoc,
				_busLoc,
				_clientLoc
			};
			database.Locations.AddRange(_newLocations);

			var priorities = new List<Priority>
			{
				new Priority { Id = 1, Name = "Highest", Description = "Наивысший приоритет" },
				new Priority { Id = 2, Name = "High", Description = "Высокий приоритет" },
				new Priority { Id = 3, Name = "Normal", Description = "Средний приоритет" },
				new Priority { Id = 4, Name = "Low", Description = "Низкий приоритет" }
			};

			database.Priorities.AddRange(priorities);

			//var _veryFast = new Priority { Name = "Очень срочный" };
			//var _fast = new Priority { Name = "Срочный" };
			//var _normal = new Priority { Name = "Обычный" };

			//List<Priority> _newPriority = new List<Priority> { _veryFast, _fast, _normal };
			//database.Priorities.AddRange(_newPriority);


			var _vip = new TransportType { Name = "VIP" };
			var _standart = new TransportType { Name = "Стандарт" };
			var _busines = new TransportType { Name = "Бизнес класс" };

			List<TransportType> _newTransportTypesPriority = new List<TransportType> { _vip, _standart, _busines };
			database.TransportTypes.AddRange(_newTransportTypesPriority);


			var _pCEO = new Position { Name = "Руководитель", IsManager = true, PriorityId = 1, TransportType = _vip };
			var _pSecretary = new Position { Name = "Секретать", IsManager = false, PriorityId = 2, TransportType = _busines };
			var _pAdmin = new Position { Name = "Системный администратор", IsManager = false, PriorityId = 3, TransportType = _standart };
			var _pAccountant = new Position { Name = "Бухгалтер", IsManager = false, PriorityId = 3, TransportType = _standart };
			var _pDev = new Position { Name = "Программист", IsManager = false, PriorityId = 3, TransportType = _standart };
			var _pEngineer = new Position { Name = "Инженер", IsManager = false, PriorityId = 3, TransportType = _standart };


			List<Position> _newPositions = new List<Position> { _pCEO, _pSecretary, _pAdmin, _pAccountant, _pDev, _pEngineer };
			database.Positions.AddRange(_newPositions);

			var _emp1 = new Employee { Firstname = "Любовь", Lastname = "Викторовна", Patronymic = "Железнова", Email = "Kor@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+79274633746", Position = _pSecretary };
			var _emp2 = new Employee { Firstname = "Владимир", Lastname = "Сергеевич", Patronymic = "Ветров", Email = "Vertor@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+79745333746", Position = _pAdmin };
			var _emp3 = new Employee { Firstname = "Игорь", Lastname = "Степанович", Patronymic = "Сидоров", Email = "Basic@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+79782546746", Position = _pAccountant };
			var _emp4 = new Employee { Firstname = "Василий", Lastname = "Владленович", Patronymic = "Абрасимов", Email = "Landing@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+72763776356", Position = _pDev };
			var _emp5 = new Employee { Firstname = "Евгений", Lastname = "Петрович", Patronymic = "Волоцуев", Email = "Vol@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+72794857346", Position = _pCEO };
			var _emp6 = new Employee { Firstname = "Глеб", Lastname = "Викторович", Patronymic = "Кондратьев", Email = "Kondrl@mail.ru", Location = _tltEPAMLoc, Orgstructure = _orgstructueTltDevNet, Phone = "+77682394834", Position = _pEngineer };

			var _newEmployees = new List<Employee> { _emp1, _emp2, _emp3, _emp4, _emp5, _emp6 };
			database.Employees.AddRange(_newEmployees);


			var _acc1 = new Account { Employee = _emp1, Username = "Love@mail.ru", Password = "1234" };
			var _acc2 = new Account { Employee = _emp2, Username = "Vlad@mail.ru", Password = "1234" };
			var _acc3 = new Account { Employee = _emp3, Username = "Igor@mail.ru", Password = "1234" };
			var _acc4 = new Account { Employee = _emp4, Username = "Vas@mail.ru", Password = "1234" };
			var _acc5 = new Account { Employee = _emp5, Username = "Evgeny@mail.ru", Password = "1234" };
			var _acc6 = new Account { Employee = _emp6, Username = "Gleb@mail.ru", Password = "1234" };
			var _newAccounts = new List<Account> { _acc1, _acc2, _acc3, _acc4, _acc5, _acc6 };
			database.Accounts.AddRange(_newAccounts);

			var statuses = new List<Status>
			{
				new Status { Id = 1, Name = "OnApproval", Description = "Заявка на согласовании" },
				new Status { Id = 2, Name = "VehicleSelection", Description = "Заявка на этапе подбора автомобиля" },
				new Status { Id = 3, Name = "WasDeclined", Description = "Заявка отклонена" },
				new Status { Id = 4, Name = "WasCancelledByUser", Description = "Заявка отменена пользователем" },
				new Status { Id = 5, Name = "WasAccepted", Description = "Заявка подтверждена" },
				new Status { Id = 6, Name = "WasCompletedSuccessfully", Description = "Заявка успешно завершена" },
				new Status { Id = 7, Name = "WasCompletedPoorly", Description = "Заявка завершена неуспешно" }
			};

			database.Statuses.AddRange(statuses);

			//var _stNeedApprove = new Status { Name = "На согласовании" };
			//var _stCarSearch = new Status { Name = "Поиск машины" };
			//var _stDeclined = new Status { Name = "Отклонена" };
			//var _stUserDeclined = new Status { Name = "Отменена пользователем" };
			//var _stAccepted = new Status { Name = "Подтверждена" };
			//var _stFinishedFailed = new Status { Name = "Завершена неуспешно" };
			//var _stFinishedOk = new Status { Name = "Завершена успешно" };
			//var _newStatuses = new List<Status> { _stNeedApprove, _stCarSearch, _stDeclined, _stUserDeclined, _stAccepted, _stFinishedFailed, _stFinishedOk };
			//database.Statuses.AddRange(_newStatuses);

			var _req1 = new Request { Author = _emp2, CreatingDateTime = new DateTime(2016, 11, 1, 12, 00, 00), DepartureDateTime = new DateTime(2016, 11, 1, 16, 00, 00), DeparturePoint = _emp2.Location, DestinationAddress = "г.Тольятти Дзержинского 45", Comment = "Необходимо забрать новое ПО у клиента дома", HasBaggage = false, Mileage = 0, Price = 0, StatusId = 1 };
			var _req2 = new Request { Author = _emp3, Comment = "", CreatingDateTime = new DateTime(2016, 11, 2, 13, 00, 00), DepartureDateTime = new DateTime(2016, 11, 2, 14, 00, 00), DeparturePoint = _emp2.Location, DestinationPoint = _airportLoc, HasBaggage = true, Mileage = 0, Price = 0, StatusId = 2 };
			var _req3 = new Request { Author = _emp4, Comment = "", CreatingDateTime = new DateTime(2016, 11, 2, 14, 00, 00), DepartureDateTime = new DateTime(2016, 11, 2, 17, 00, 00), DeparturePoint = _emp2.Location, DestinationPoint = _clientLoc, HasBaggage = true, Mileage = 20, Price = 100, StatusId = 6 };
			var _req4 = new Request { Author = _emp5, Comment = "", CreatingDateTime = new DateTime(2016, 11, 1, 15, 00, 00), DepartureDateTime = new DateTime(2016, 11, 1, 19, 00, 00), DeparturePoint = _emp2.Location, DestinationPoint = _busLoc, HasBaggage = false, Mileage = 0, Price = 0, StatusId = 2 };
			var _newRequests = new List<Request> { _req1, _req2, _req3, _req4 };
			database.Requests.AddRange(_newRequests);

			database.SaveChanges();
		}
	}
}
