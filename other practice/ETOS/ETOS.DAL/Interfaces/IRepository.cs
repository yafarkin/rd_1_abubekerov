﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ETOS.DAL.Interfaces
{
    /// <summary>
    /// Provides common operations for all entities
    /// </summary>
    /// <typeparam name="TEntity">specific entity</typeparam>
    public interface IRepository<TEntity> : IDisposable where TEntity : IEntity 
    {
        IEnumerable<TEntity> GetAll();
        TEntity Get(int id);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        void Create(TEntity item);
        void Update(TEntity item);
        void Delete(int id);
    }    
}
