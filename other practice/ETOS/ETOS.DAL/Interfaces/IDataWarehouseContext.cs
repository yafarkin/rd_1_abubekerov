﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

using ETOS.DAL.Entities;

namespace ETOS.DAL.Interfaces
{
	/// <summary>
	/// Интерфейс, имплементируемый хранилищами данных.
	/// </summary>
	public interface IDataWarehouseContext
	{
		DbSet<Account> Accounts { get; set; }
		DbSet<Employee> Employees { get; set; }
		DbSet<Location> Locations { get; set; }
		DbSet<Orgstructure> Orgstructures { get; set; }
		DbSet<Position> Positions { get; set; }
		DbSet<Priority> Priorities { get; set; }
		DbSet<TransportType> TransportTypes { get; set; }
		DbSet<Request> Requests { get; set; }
		DbSet<Status> Statuses { get; set; }
		DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

		void Dispose();
		int SaveChanges();
	}
}
