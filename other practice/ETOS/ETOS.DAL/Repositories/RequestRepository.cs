﻿using ETOS.DAL.Entities;
using ETOS.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace ETOS.DAL.Repositories
{
    /// <summary>
    /// Репозиторий для работы с заявками
    /// </summary>
    public class RequestsRepository : IRepository<Request>
    {
        #region Fields
        private readonly IDataWarehouseContext _db;
        #endregion

        #region Constructor
        public RequestsRepository(IDataWarehouseContext context)
        {
            _db = context;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Creates new item in Requests
        /// </summary>
        /// <param name="item">item</param>
        public void Create(Request item)
        {
            _db.Requests.Add(item);
            _db.SaveChanges();
        }


        /// <summary>
        /// Removes item by id
        /// </summary>
        /// <param name="id">id of item</param>
        public void Delete(int id)
        {
            var req = _db.Requests.Find(id);
            if (req != null)
                _db.Requests.Remove(req);
            _db.SaveChanges();
        }

        /// <summary>
        /// Returns list of Requests using conditions
        /// </summary>
        /// <param name="predicate">conditions</param>
        /// <returns>List of Requests</returns>
        public IEnumerable<Request> Find(Expression<Func<Request, bool>> predicate)
        {
            return _db.Requests.Where(predicate).ToList();
        }

        /// <summary>
        /// Returns Item using specified ID
        /// </summary>
        /// <param name="id">id of item</param>
        /// <returns></returns>
        public Request Get(int id)
        {
            return _db.Requests.Find(id);
        }

        /// <summary>
        /// Returns Requests entity
        /// </summary>
        /// <returns>Requests dbset</returns>
        public IEnumerable<Request> GetAll()
        {
            return _db.Requests;
        }

        /// <summary>
        /// Sets item as modified 
        /// </summary>
        /// <param name="item">item </param>
        public void Update(Request item)
        {
            _db.Entry(item).State = EntityState.Modified;
            _db.SaveChanges();
        }


        private bool _disposed = false;

        /// <summary>
        /// Disposing database
        /// </summary>
        /// <param name="disposing">if dispose need flag</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            this._disposed = true;
        }

        /// <summary>
        /// Disposing
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }

}
