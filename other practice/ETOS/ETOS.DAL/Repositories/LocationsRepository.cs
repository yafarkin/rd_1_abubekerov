﻿using ETOS.DAL.Entities;
using ETOS.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace ETOS.DAL.Repositories
{
   public  class LocationsRepository : IRepository<Location>
    {
        #region Fields
        private readonly IDataWarehouseContext _db;
        #endregion

        #region Constructor
        public LocationsRepository(IDataWarehouseContext context)
        {
            _db = context;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Creates new item in Locations
        /// </summary>
        /// <param name="item">item</param>
        public void Create(Location item)
        {
            _db.Locations.Add(item);
            _db.SaveChanges();
        }


        /// <summary>
        /// Removes item by id
        /// </summary>
        /// <param name="id">id of item</param>
        public void Delete(int id)
        {
            var loc = _db.Locations.Find(id);
            if (loc != null)
                _db.Locations.Remove(loc);
            _db.SaveChanges();
        }

        /// <summary>
        /// Returns list of Locations using conditions
        /// </summary>
        /// <param name="predicate">conditions</param>
        /// <returns>List of Locations</returns>
        public IEnumerable<Location> Find(Expression<Func<Location, bool>> predicate)
        {
            return _db.Locations.Where(predicate).ToList();
        }

        /// <summary>
        /// Returns Item using specified ID
        /// </summary>
        /// <param name="id">id of item</param>
        /// <returns></returns>
        public Location Get(int id)
        {
            return _db.Locations.Find(id);
        }

        /// <summary>
        /// Returns Locations entity
        /// </summary>
        /// <returns>Locations dbset</returns>
        public IEnumerable<Location> GetAll()
        {
            return _db.Locations;
        }

        /// <summary>
        /// Sets item as modified 
        /// </summary>
        /// <param name="item">item </param>
        public void Update(Location item)
        {
            _db.Entry(item).State = EntityState.Modified;
            _db.SaveChanges();
        }


        private bool _disposed = false;

        /// <summary>
        /// Disposing database
        /// </summary>
        /// <param name="disposing">if dispose need flag</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            this._disposed = true;
        }

        /// <summary>
        /// Disposing
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
