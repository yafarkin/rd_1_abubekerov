﻿using ETOS.DAL.Interfaces;
using ETOS.DAL.Entities;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ETOS.DAL.Repositories
{
    /// <summary>
    /// Обеспечивает работу с данными аккаунтов
    /// </summary>
    public class AccountsRepository : IAccountRepository
    {

        #region Fields

        private readonly IDataWarehouseContext _db;

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор репозитория для аккаунтов
        /// </summary>
        /// <param name="context"></param>
        public AccountsRepository(IDataWarehouseContext context)
        {
            _db = context;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Creates new item in Accounts
        /// </summary>
        /// <param name="item">item</param>
        public void Create(Account item)
        {
            _db.Accounts.Add(item);
        }

        /// <summary>
        /// Removes item by id
        /// </summary>
        /// <param name="id">id of item</param>
        public void Delete(int id)
        {
            Account acc = _db.Accounts.Find(id);
            if (acc != null)
                _db.Accounts.Remove(acc);
        }

        /// <summary>
        /// Returns list of Accounts using conditions
        /// </summary>
        /// <param name="predicate">conditions</param>
        /// <returns>List of Accounts</returns>
        public IEnumerable<Account> Find(Expression<Func<Account, bool>> predicate)
        {
            return _db.Accounts.Where(predicate).ToList();
        }

        /// <summary>
        /// Returns Item using specified ID
        /// </summary>
        /// <param name="id">id of item</param>
        /// <returns></returns>
        public Account Get(int id)
        {
            return _db.Accounts.Find(id);
        }

        /// <summary>
        /// Returns accounts entity
        /// </summary>
        /// <returns>Accounts dbset</returns>
        public IEnumerable<Account> GetAll()
        {
            return _db.Accounts;
        }

        /// <summary>
        /// Sets item as modified 
        /// </summary>
        /// <param name="item">item </param>
        public void Update(Account item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }




        private bool disposed = false;

        /// <summary>
        /// Disposing database
        /// </summary>
        /// <param name="disposing">if dispose need flag</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Disposing
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Saves changes to database
        /// </summary>
        public void Save()
        {
            _db.SaveChanges();
        }

        #endregion
    }
}
