﻿
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ETOS.Core.Services;
using ETOS.Core.Services.Abstract;
using ETOS.WebUI.Controllers;
using ETOS.WebUI.Security;
using ETOS.WebUI.Security.Abstract;
using ETOS.Core.DTO;
using System.Web.Mvc;

namespace ETOS.Tests
{
    /// <summary>
    /// Сводное описание для CoreTests
    /// </summary>
    [TestClass]
    public class CoreTests
    {
        /// <summary>
        /// Метод, реализующий проверку наличия положительного результата авторизации
        /// в случае ввода корректных данных.
        /// </summary>
        [TestMethod]
        public void CanLoginWithValidCredentials()
        {
            // Arrange - создание mock-экземпляра сервиса аккаунтов.
            Mock<IAuthProvider> mock = new Mock<IAuthProvider>();

            mock.Setup(s => s.Authenticate("Love1", "1234", false)).Returns(true);

            DtoAccount account = new DtoAccount { Username = "Love1", Password = "1234" };

            // Arrange - создание экземпляра контроллера.
            AccountController target = new AccountController(mock.Object);

            // Act - авторизация с корректными учетными данными.
            ActionResult result = target.Login(account, "/MyURL");

            // Assert.
            Assert.IsInstanceOfType(result, typeof(RedirectResult));
            Assert.AreEqual("/MyURL", ((RedirectResult)result).Url);

        }

        /// <summary>
        /// Метод, реализующий проверку наличия отрицательного результата авторизации
        /// в случае ввода некорректных данных.
        /// </summary>
        [TestMethod]
        public void CannotLoginWithInvalidCredentials()
        {
            // Arrange - создание mock-экземпляра сервиса аккаунтов.
            Mock<IAuthProvider> mock = new Mock<IAuthProvider>();

            mock.Setup(s => s.Authenticate("badUser", "badPassword", false)).Returns(false);

            DtoAccount account = new DtoAccount { Username = "badUser", Password = "badPassword" };

            // Arrange - создание экземпляра контроллера.
            AccountController target = new AccountController(mock.Object);

            // Act - авторизация с корректными учетными данными.
            ActionResult result = target.Login(account, "/MyURL");

            // Assert.
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(((ViewResult)result).ViewData.ModelState.IsValid);

        }
    }
}
