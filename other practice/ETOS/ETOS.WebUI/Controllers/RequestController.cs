﻿using System.Collections.Generic;
using ETOS.WebUI.ViewModels;
using System.Web.Mvc;
using ETOS.Core.DTO;
using ETOS.Core.Services;
using static ETOS.WebUI.ViewModels.RequestsViewModels;
using ETOS.Core.Services.Abstract;
using System;

namespace ETOS.WebUI.Controllers
{
    /// <summary>
    /// Контроллер, обеспечивающий работу с заявками на уровне представления.
    /// </summary>
    [Authorize]
    public class RequestController : Controller
    {
        #region Fields

        IRequestService _requestService;
        ILocationService _locationService;
        IAccountService _accountService;

        #endregion

        #region Constructor

        public RequestController(IRequestService reqService, ILocationService locService, IAccountService accountService)
        {
            _requestService = reqService;
            _locationService = locService;
            _accountService = accountService;
        }

        #endregion 

        #region Methods

        /// <summary>
        /// Get-версия метода, возвращающая представление с полным перечнем заявок.
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            var model = new ShortRequestsViewModel();

            var requestList = new List<DtoRequest>();

            var filter = new FilterSettings();

            model.RequestsList = _requestService.GetRequest(filter);

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ShortRequestsViewModel model)
        {
            return View(model);
        }


        /// <summary>
        /// Get-версия метода, возвращающая представление с полным перечнем заявок.
        /// </summary>
        [HttpGet]
        public ActionResult Create()
        {
            CreateRequestViewModel model = new CreateRequestViewModel();
            model.newRequest = new DtoRequest();
            model.toPoint = model.btwPoint = model.fromPoint = new SelectList(_locationService.GetOnlyPoi(), "From");

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CreateRequestViewModel model)
        {

            if (!ModelState.IsValid) return View(model);


            var login = User.Identity.Name;

            int userId;

            if (_accountService.GetIdbyLogin(login, out userId))
            {
                model.newRequest.AuthorId = userId;
                model.newRequest.CreatingDateTime = DateTime.Now;

                _requestService.AddRequest(model.newRequest);
            }
            else
            {
                return View(model);
            }

            return RedirectToAction("Index", "Home");

        }

        /// <summary>
        /// Get-версия метода, возвращающая представление для просмотра деталей заявки.
        /// </summary>
        [HttpGet]
        public ActionResult Detail()
        {
            return View();
        }

        #endregion
    }
}