﻿using System.Web.Mvc;
using System.Web.Security;

using ETOS.Core.DTO;

using ETOS.WebUI.Security.Abstract;

namespace ETOS.WebUI.Controllers
{
    /// <summary>
    /// Контроллер, обеспечивающий работу с пользовательскими данными на уровне представления.
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        #region Fields

        /// <summary>
        /// Экземпляр провайдера авторизации.
        /// </summary>
        private IAuthProvider _authProvider;

        #endregion

        #region Constructor

        public AccountController(IAuthProvider authProvider)
        {
            _authProvider = authProvider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, возвращающий представление с данными пользователя.
        /// </summary>
        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get-версия метода, возвращающая представление входа в систему.
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Post-версия метода, реализующая авторизацию в системе (на уровне представления).
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(DtoAccount account, string returnUrl)
        {


            if (ModelState.IsValid)
            {
                if (_authProvider.Authenticate(account.Username, account.Password, account.Persistent))
                {
                    return Redirect(returnUrl ?? Url.Action("Index", "Request"));

                }
                else
                {
                    ModelState.AddModelError("", "Неправильный пароль или логин");
                }
            }
            return View(account);
        }

        /// <summary>
        /// Метод, реализующий выход пользователя из системы.
        /// </summary>
        [Authorize]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Account");
        }

        #endregion
    }
}