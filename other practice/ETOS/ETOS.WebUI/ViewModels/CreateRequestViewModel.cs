﻿using ETOS.Core.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETOS.WebUI.ViewModels
{
    public class CreateRequestViewModel
    {      
        [Display(Name = "Откуда")]
        public SelectList fromPoint { get; set; }

        [Display(Name = "Промежуточный пункт")]
        public SelectList btwPoint { get; set; }

        [Display(Name = "Куда")]
        public SelectList toPoint { get; set; }

        public string 

        Точка отправки
        Промежуточная точка
        Точка прибытия
        Дата и время отправления
        Багаж(есть или нет)
        Комментарий


        public DtoRequest newRequest { get; set; }
    }
}