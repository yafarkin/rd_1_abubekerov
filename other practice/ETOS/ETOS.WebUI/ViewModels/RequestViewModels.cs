﻿using ETOS.Core.Common;
using ETOS.Core.DTO;
using ETOS.Core.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETOS.WebUI.ViewModels
{
    public class RequestViewModels
    {

        /// <summary>
        /// модель для передачи данных между контроллером и view для страницы Мои заявки - Request -> Index
        /// </summary>
        public class MyRequestsModel
        {
            //список заявок которые надо отобразить
            public IEnumerable<DtoRequest> Requests { get; set; }

            // условия фильтра которые задал пользователь
            public IFilter filter { get; set; }

            public MyRequestsModel()
            {
                Requests = new List<DtoRequest>();
                filter = new RequestFilter();
            }
        }

    }
}