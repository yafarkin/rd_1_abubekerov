﻿using System.Collections.Generic;
using ETOS.Core.DTO;
using ETOS.Core.Services;

namespace ETOS.WebUI.ViewModels
{
    /// <summary>
    /// ViewModel-и для страницы с заявками
    /// </summary>
    public class RequestsViewModels
    {                      
        /// <summary>
        /// Модель для страницы мои заявки - краткий вид
        /// </summary>
        public class ShortRequestsViewModel
        {
            /// <summary>
            /// Список заявок
            /// </summary>
            public List<DtoRequest> RequestsList { get; set; }

            /// <summary>
            /// Фильтр для заявок
            /// </summary>
            public FilterSettings Filter { get; set; }
        }
    }
}