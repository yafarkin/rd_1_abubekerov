﻿SET IDENTITY_INSERT dbo.Employee ON

DECLARE @Employee TABLE(
    [EmployeeId]     INT           NOT NULL,
    [FirstName]      NCHAR (50) NOT NULL,
    [MiddleName]     NCHAR (50) NULL,
    [LastName]       NCHAR (50) NOT NULL,
    [Phone]          NCHAR (16) NOT NULL,
    [Email]          NCHAR (50) NOT NULL,
    [LocationId]     INT           NOT NULL,
    [PositionId]     INT           NOT NULL,
    [OrgstructureId] INT           NOT NULL
	)

INSERT INTO @Employee ([EmployeeId], [FirstName], [MiddleName], [LastName], [Phone], [Email], [LocationId], [PositionId], [OrgstructureId]) VALUES
(1, N'Евгений', N'Петрович', N'Волоцуев', N'+72794857346', N'Vol@mail.ru', 7, 1, 8),
(2, N'Любовь', N'Викторовна', N'Железнова', N'+79274633766', N'Kor@mail.ru', 7, 1, 8)

MERGE INTO [Employee] AS [target]
USING @Employee AS [source] ON [target].[EmployeeId] = [source].[EmployeeId]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[FirstName] = [source].[FirstName],
			[target].[MiddleName] = [source].[MiddleName],
			[target].[LastName]	= [source].[LastName],
			[target].[Phone] = [source].[Phone],
			[target].[Email] = [source].[Email],
			[target].[LocationId] = [source].[LocationId],
			[target].[PositionId] = [source].[PositionId],
			[target].[OrgstructureId] = [source].[OrgstructureId]
			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([EmployeeId], [FirstName], [MiddleName], [LastName], [Phone], [Email], [LocationId], [PositionId], [OrgstructureId]) 
	VALUES ([source].[EmployeeId], [source].[FirstName], [source].[MiddleName], [source].[LastName], [source].[Phone], [source].[Email], [source].[LocationId], [source].[PositionId], [source].[OrgstructureId]);

SET IDENTITY_INSERT dbo.Employee OFF
GO