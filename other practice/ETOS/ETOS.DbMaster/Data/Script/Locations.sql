﻿SET IDENTITY_INSERT dbo.Location ON

DECLARE @Location TABLE(
    [LocationId]       INT            NOT NULL,
    [Name]             NVARCHAR (150) NOT NULL,
    [Address]          NVARCHAR (255) NULL,
    [ParentLocationId] INT            NULL,
    [IsPOI]            BIT            NOT NULL,
    [Culture]          INT            NOT NULL
	)

INSERT INTO @Location ([LocationId], [Name], [Address], [ParentLocationId], [IsPOI], [Culture]) VALUES
(1, N'Россия', NULL, NULL, 0, 12),
(2, N'Самарская область', NULL, 1, 0, 12),
(3, N'Московская область', NULL, 1, 0, 12),
(4, N'Тольятти', NULL, 2, 0, 12),
(5, N'Самара', NULL, 2, 0, 12),
(6, N'Москва', NULL, 3, 0, 12),
(7, N'EPAM_TLT', N'г.Тольятти ул.Юбилейная 31Е', 4, 1, 12),
(8, N'Аэропорт Курумоч', N'Самарская обл. аэропорт Курумоч', 2, 1, 12),
(9, N'компания Рога и Копыта', N'Тольятти ул.Юбилейная 178', 4, 1, 12),
(10, N'Автовокзал Аврора', N'Тольятти ул.70 лет октября Автостанция Аврора', 4, 1, 12),
(11, N'EPAM_SMR', N'г.Самара ул.Мичурина 21Е', 5, 1, 12),
(12, N'EPAM_MSK', N'г.Москва ул.Генераторов 6', 6, 1, 12)

MERGE INTO [Location] AS [target]
USING @Location AS [source] ON [target].[LocationId] = [source].[LocationId]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Name] = [source].[Name],
			[target].[Address] = [source].[Address],
			[target].[ParentLocationId]	= [source].[ParentLocationId],
			[target].[IsPOI] = [source].[IsPOI],
			[target].[Culture] = [source].[Culture]
			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LocationId], [Name], [Address], [ParentLocationId], [IsPOI], [Culture]) 
	VALUES ([source].[LocationId], [source].[Name], [source].[Address], [source].[ParentLocationId], [source].[IsPOI], [source].[Culture]);

SET IDENTITY_INSERT dbo.Location OFF
GO