﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Reflection
{
    public class User
    {
        public int Num { get; set; }
    }

    class Program
    {

        

        static void Main(string[] args)
        {
            Type myType = Type.GetType("Reflection.User", false, true);

            foreach (MemberInfo mi in myType.GetMembers())
            {
                Console.WriteLine(mi.DeclaringType + " " + mi.MemberType + " " + mi.Name);
            }

            Console.ReadLine();
        }
    }
}
