﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RemoteDevicesWebApp.Startup))]
namespace RemoteDevicesWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
