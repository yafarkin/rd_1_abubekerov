﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Ninject;

namespace Ninja
{
    class Program
    {

        public interface IAction
        {
            void DoSomething();
        }

        public class SuperAction : IAction
        {
            public void DoSomething()
            {
                Console.WriteLine("SuperAction");
            }
        }

        public class Dog
        {
            private IAction action;

            public Dog(IAction _action)
            {
                action = _action;
                action.DoSomething();
                Console.WriteLine("_Dog");
            }
        }


        public class Human
        {
            private IAction action;

            public Human(IAction _action)
            {
                action = _action;
                action.DoSomething();
                Console.WriteLine("_Human");
            }
        }


        static void Main(string[] args)
        {
            var kernel = new StandardKernel();
            kernel.Bind<IAction>().To<SuperAction>();

            var man = kernel.Get<Human>();
            var animal = kernel.Get<Dog>();

            Console.ReadKey();
        }
    }
}
