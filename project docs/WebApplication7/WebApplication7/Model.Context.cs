﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication7
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class taxiprojectEntities : DbContext
    {
        public taxiprojectEntities()
            : base("name=taxiprojectEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Accounts> Accounts { get; set; }
        public virtual DbSet<Contractors> Contractors { get; set; }
        public virtual DbSet<EmployeeBalance> EmployeeBalance { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<Limits> Limits { get; set; }
        public virtual DbSet<Locations> Locations { get; set; }
        public virtual DbSet<Orgstructures> Orgstructures { get; set; }
        public virtual DbSet<Permissions> Permissions { get; set; }
        public virtual DbSet<Positions> Positions { get; set; }
        public virtual DbSet<Priorities> Priorities { get; set; }
        public virtual DbSet<ProvidedServices> ProvidedServices { get; set; }
        public virtual DbSet<Requests> Requests { get; set; }
        public virtual DbSet<Routes> Routes { get; set; }
        public virtual DbSet<RstLifeCycles> RstLifeCycles { get; set; }
        public virtual DbSet<Services> Services { get; set; }
        public virtual DbSet<Statuses> Statuses { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<TransportTypes> TransportTypes { get; set; }
    }
}
