﻿CREATE TABLE [dbo].[Contractors] (
   [ContractorId] INT NOT NULL PRIMARY KEY, 
    [Name] NCHAR(150) NOT NULL, 
    [Phone] NCHAR(16) NOT NULL, 
    [Tariff] FLOAT NOT NULL
);

