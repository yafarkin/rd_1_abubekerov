﻿CREATE TABLE [dbo].[Limits]
(
	[LimitId] INT NOT NULL PRIMARY KEY, 
    [EmployeeId] INT NULL, 
    [FullPriceLimit] FLOAT NULL, 
    [TripCountLimit] FLOAT NULL
)
