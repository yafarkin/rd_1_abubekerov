﻿CREATE TABLE [dbo].[TransportTypes]
(
	[TransportTypeId] INT NOT NULL PRIMARY KEY, 
    [Name] NCHAR(30) NOT NULL
)
