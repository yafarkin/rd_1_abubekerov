﻿CREATE TABLE [dbo].[ProvidedServices] (
   [ContractorId] INT NOT NULL,
	[TransportTypeId] INT NOT NULL,
	PRIMARY KEY ([ContractorId], [TransportTypeId]),
	[MinimalNumberOfSeats] INT NOT NULL,
	[MaximalNumberOfSeats] INT NOT NULL,
	[Price] FLOAT NOT NULL    
);

