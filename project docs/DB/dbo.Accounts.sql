﻿CREATE TABLE [dbo].[Accounts] (
    [AccountId]         INT NOT NULL,
    [EmployeeId]      INT        NOT NULL,
    [Login]     NCHAR(16)        NOT NULL,
    [Password] NCHAR(20) NOT NULL, 
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([AccountId] ASC),
    CONSTRAINT [FK_Users_Users] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([AccountId])
);

