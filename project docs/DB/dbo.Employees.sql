﻿CREATE TABLE [dbo].[Employees] (
    [EmployeeId] INT NOT NULL , 
    [FirstName] NCHAR(50) NOT NULL, 
    [MiddleName] NCHAR(50) NOT NULL, 
    [LastName] NCHAR(50) NOT NULL, 
    [Phone] NCHAR(16) NOT NULL, 
    [Email] NCHAR(50) NOT NULL, 
    [LocationId] INT NOT NULL, 
    [PositionId] INT NOT NULL, 
    [PermissionTypeId] INT NOT NULL, 
    [OrgstructureId] INT NOT NULL, 
    PRIMARY KEY ([EmployeeId])
);

