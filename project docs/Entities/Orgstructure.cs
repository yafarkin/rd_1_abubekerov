﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class Orgstructure
    {
        public int OrgstructureId { get; set; }
        public string Name { get; set; }        
        public ICollection<Orgstructure> OrgstructureParentId { get; set; }

    }
}
