﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class Contractor
    {
        public int ContractorId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public float Tariff { get; set; }     
    }
}
