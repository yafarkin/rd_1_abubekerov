﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class Location
    {
        public int LocationId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }        
        public ICollection<Location> ParentLocationId { get; set; }
        public bool IsPOI { get; set; }
        public int Culture { get; set; }        
    }
}
