﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class EmployeeBalance
    {        
        public ICollection<Employee> EmployeeId { get; set; }
        public int TripsBalance { get; set; }
        public int MoneyBalance { get; set; }
    }
}
