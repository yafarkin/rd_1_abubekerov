﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class Request
    {
        public int RequestId { get; set; }
        public int AuthorId { get; set; }
        public int DeparturePoint { get; set; }
        public int DestinationPoint { get; set; }
        public string DepartureAddress { get; set; }
        public string DestinationAddress { get; set; }
        public DateTime DepartureDateTime { get; set; }
        public DateTime CreatingDateTime { get; set; }
        public bool IsBaggage { get; set; }
        public string Comment { get; set; }
        public float Mileage { get; set; }
        public float Price { get; set; }
    }
}
