﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class Route
    {
        public int RoutePointId { get; set; }
        public int RequestId { get; set; }
        public int DestinationPointId { get; set; }
        public string DestinationAddress { get; set; }
        public int NextRoutePointId { get; set; }
    }
}