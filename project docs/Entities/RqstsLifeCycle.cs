﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class RqstsLifeCycle
    {
        public int EventId { get; set; }
        public int RequestId { get; set; }
        public DateTime EventDateTime { get; set; }
        public int StatusId { get; set; }
        public int AuthorId { get; set; }
    }
}