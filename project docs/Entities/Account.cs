﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class Account
    {
        public int AccountId { get; set; }
        public int EmployeeId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}