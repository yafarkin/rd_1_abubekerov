﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class Permission
    {
        public int PermissionId { get; set; }
        public bool IsCommonUser { get; set; }
        public bool IsSecretary { get; set; }
        public bool IsAccountant { get; set; }
        public bool IsManager { get; set; }
        public bool IsAdministrator { get; set; }
        public int EmployeeId { get; set; }
    }
}