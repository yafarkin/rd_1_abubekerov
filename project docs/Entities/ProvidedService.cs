﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class ProvidedService
    {        
        public ICollection<Contractor> ContractorId { get; set; }
        public ICollection<TransportType> TransportTypeId { get; set; }        
        public int MinimalNumberOfSeats { get; set; }
        public int MaximalNumberOfSeats { get; set; }
        public int Price { get; set; }
      
    }
}
