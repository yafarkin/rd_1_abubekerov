﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class Service
    {
        public ICollection<Contractor> ContractorId { get; set; }
        public ICollection<Location> LocationId { get; set; }        
    }
}
