﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTransportationOrderingSystem.Domain.Entities
{
    public class Limit
    {
        public int LimitId { get; set; }
        public int EmployeeId { get; set; }
        public float FullPriceLimit { get; set; }
        public float TripCountLimit { get; set; }
    }
}