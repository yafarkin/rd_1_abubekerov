﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
:r ..\Script\TransportTypes.sql
:r ..\Script\Statuses.sql
:r ..\Script\Priorities.sql
:r ..\Script\Locations.sql
:r ..\Script\Positions.sql
:r ..\Script\Orgstructures.sql
:r ..\Script\Employees.sql
:r ..\Script\Accounts.sql
:r ..\Script\Requests.sql
