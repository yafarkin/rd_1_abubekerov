﻿SET IDENTITY_INSERT dbo.Status ON

DECLARE @Status TABLE(
    [StatusId]   INT     NOT NULL,
    [Name] CHAR (30) NOT NULL
	)

INSERT INTO @Status ([StatusId], [Name]) VALUES
(1, 'На согласовании'),
(2, 'Поиск машины'),
(3, 'Завершена успешно'),
(4, 'Отклонена'),
(5, 'Отменена пользователем'),
(6, 'Подтверждена'),
(7, 'Завершена неуспешно')

MERGE INTO [Status] AS [target]
USING @Status AS [source] ON [target].[StatusId] = [source].[StatusId]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Name] = [source].[Name]
			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([StatusId], [Name]) 
	VALUES ([source].[StatusId], [source].[Name]);

SET IDENTITY_INSERT dbo.Status OFF
GO