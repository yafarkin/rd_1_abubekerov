﻿SET IDENTITY_INSERT dbo.Priority ON

DECLARE @Priority TABLE(
    [PriorityId] INT           NOT NULL,
    [Name]       NCHAR (30) NOT NULL
	)

INSERT INTO @Priority ([PriorityId], [Name]) VALUES
(1, N'Высокий'),
(2, N'Средний'),
(3, N'Низкий')

MERGE INTO [Priority] AS [target]
USING @Priority AS [source] ON [target].[PriorityId] = [source].[PriorityId]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Name] = [source].[Name]
			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([PriorityId], [Name]) 
	VALUES ([source].[PriorityId], [source].[Name]);

SET IDENTITY_INSERT dbo.Priority OFF
GO