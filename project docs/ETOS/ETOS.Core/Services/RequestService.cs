﻿using ETOS.Core.DTO;
using ETOS.Core.Services.Abstract;
using ETOS.DAL.Entities;
using ETOS.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;


namespace ETOS.Core.Services
{
    /// <summary>
    /// Сервис, реализующий функционал для работы с заявками пользователей системы на уровне бизнес-логики.
    /// </summary>
    public class RequestService : IRequestService
    {
        #region Fields 

        private readonly IRepository<Request> _requestRepository;

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор сервиса по работе с заявками
        /// </summary>
        public RequestService(IRepository<Request> requestRep)
        {
            _requestRepository = requestRep;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод,реализующий получение списка заявок с учётом фильтрации.
        /// </summary>
        /// <param name="filter">Условие фильтрования.</param>
        public IEnumerable<DtoRequest> GetRequest(FilterService filter)
        {
            var filterList = _requestRepository.Find(x => (filter.RequestId == 0) || (x.Id == filter.RequestId));
            filterList.Where(x => (filter.AuthorFirstName == null) || (x.Author.Firstname == filter.AuthorFirstName)).
                       Where(x => (filter.AuthorLastName == null) || (x.Author.Lastname == filter.AuthorLastName)).
                       Where(x => (filter.DeparturePoint == null) || (x.DeparturePoint.Name == filter.DeparturePoint)).
                       Where(x => (filter.DestinationPoint == null) || (x.DestinationPoint.Name == filter.DestinationPoint)).
                       Where(x => (filter.DepartureAddress == null) || (x.DepartureAddress == filter.DepartureAddress)).
                       Where(x => (filter.DestinationAddress == null) || (x.DestinationAddress == filter.DestinationAddress)).
                       Where(x => (filter.DepartureDateTime == DateTime.MinValue) || (x.DepartureDateTime == filter.DepartureDateTime)).
                       Where(x => (filter.CreatingDateTime == DateTime.MinValue) || (x.CreatingDateTime == filter.CreatingDateTime)).
                       Where(x => (filter.HasBaggage == false) || (x.HasBaggage == filter.HasBaggage)).
                       Where(x => (filter.Comment == null) || (x.Comment == filter.Comment)).
                       Where(x => (filter.Mileage == 0) || (x.Mileage == filter.Mileage)).
                       Where(x => (filter.Price == 0) || (x.Price == filter.Price)).
                       Where(x => (filter.StatusName == null) || (x.Status.Description == filter.StatusName));
            IEnumerable<DtoRequest> outList = new List<DtoRequest>();
            outList = filterList.Select(x => new DtoRequest
            {
                RequestId = x.Id,
                AuthorFirstName = x.Author.Firstname,
                AuthorLastName = x.Author.Lastname,
                DeparturePointName = (x.DeparturePoint == null) ? x.DepartureAddress : x.DeparturePoint.Name,
                DestinationPointName = (x.DestinationPoint == null) ? x.DestinationAddress : x.DestinationPoint.Name,
                DepartureAddress = x.DepartureAddress,
                DestinationAddress = x.DestinationAddress,
                DepartureDateTime = x.DepartureDateTime,
                CreatingDateTime = x.CreatingDateTime,
                StatusName = x.Status.Description,
                HasBaggage = x.HasBaggage,
                Comment = x.Comment,
                Mileage = x.Mileage,
                Price = x.Price
            });

            // Настройка AutoMapper
            //Mapper.Initialize(cfg => cfg.CreateMap< Request, DtoRequest>());
            //var outList = Mapper.Map<IEnumerable<Request>, List<DtoRequest>>(filterList);
            return outList;
        }

        /// <summary>
        /// Метод отрабатывает вызов реализации добавления заявки через репозиторий
        /// </summary>
        /// <param name="item"></param>
        public void AddRequest(DtoRequest item)
        {
            var createItem = Mapper.Map<DtoRequest, Request>(item);
            _requestRepository.Create(createItem);
        }

        /// <summary>
        /// Метод отрабатывает вызов реализации удаления заявки через репозиторий
        /// </summary>
        /// <param name="item"></param>
        public void DeleteRequest(DtoRequest item)
        {
            var deleteItem = Mapper.Map<DtoRequest, Request>(item);
            _requestRepository.Delete(deleteItem.Id);
        }

        #endregion
    }
}
