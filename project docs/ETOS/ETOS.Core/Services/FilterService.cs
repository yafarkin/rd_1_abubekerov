﻿using System;
using ETOS.Core.Services.Abstract;

namespace ETOS.Core.Services
{
    /// <summary>
    /// Класс,содержащий поля для фильтрации
    /// </summary>
    public class FilterService
    {
        public int RequestId { get; set; }
        public string AuthorFirstName { get; set; }
        public string AuthorLastName { get; set; }
        public string Comment { get; set; }
        public DateTime CreatingDateTime { get; set; }
        public string DepartureAddress { get; set; }
        public DateTime DepartureDateTime { get; set; }
        public string DeparturePoint { get; set; }
        public string DestinationAddress { get; set; }
        public string DestinationPoint { get; set; }
        public bool HasBaggage { get; set; }
        public double Mileage { get; set; }
        public decimal Price { get; set; }
        public string StatusName { get; set; }
    }
}
