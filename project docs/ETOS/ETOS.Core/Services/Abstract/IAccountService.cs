﻿using ETOS.Core.DTO;

namespace ETOS.Core.Services.Abstract
{
    /// <summary>
    /// Интерфейс сервиса по работе с учетными записями пользователей.
    /// </summary>
    public interface IAccountService 
    {
        /// <summary>
        /// Метод, реализующий авторизацию пользователя в системе.
        /// </summary>
        /// <param name="account">Учетная запись для авторизации.</param>
        /// <returns>true - в случае успешной авторизации, false - в противном случае. </returns>
        bool Authorize(DtoAccount account);
    }
}
