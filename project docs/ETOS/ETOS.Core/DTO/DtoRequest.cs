﻿using System;

namespace ETOS.Core.DTO
{
    /// <summary>
    /// Объект передачи данных между моделью уровня представления и уровнем доступа к данным.
    /// Представляет собой аккаунт пользователя.
    /// </summary>
    public class DtoRequest
    {
        /// <summary>
        /// Идентефикационный номер заявки
        /// </summary>
        public int RequestId { get; set; }

        /// <summary>
        /// Имя автора заявки
        /// </summary>
        public string AuthorFirstName { get; set; }

        /// <summary>
        /// Фамилия автора заявки
        /// </summary>
        public string AuthorLastName { get; set; }

        /// <summary>
        /// Адрес точки отправки согласно коду
        /// </summary>
        public string DeparturePointName { get; set; }

        /// <summary>
        /// Адрес точки прибытия согласно коду
        /// </summary>
        public string DestinationPointName { get; set; }

        /// <summary>
        /// Адрес точки отправки
        /// </summary>
        public string DepartureAddress { get; set; }
        /// <summary>
        /// Адрес точки прибытия
        /// </summary>
        public string DestinationAddress { get; set; }
        /// <summary>
        /// Время отправки
        /// </summary>
        public DateTime DepartureDateTime { get; set; }
        /// <summary>
        /// Время создания заявки
        /// </summary>
        public DateTime CreatingDateTime { get; set; }
        /// <summary>
        /// Наличие багажа,да/нет
        /// </summary>
        public bool HasBaggage { get; set; }
        /// <summary>
        /// Комментарии к заявке
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Километраж
        /// </summary>
        public double Mileage { get; set; }
        /// <summary>
        /// Общая сумма за поездку
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Статус
        /// </summary>
        public string StatusName { get; set; }
    }
}
