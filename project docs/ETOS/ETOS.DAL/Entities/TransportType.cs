﻿using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// Описывает сущность "Вид транспорта".
	/// Данная сущность описывает категорию, класс транспортного средства, необходимого для сотрудников.
	/// </summary>
	public class TransportType : IEntity
	{
		#region Fields

		/// <summary>
		/// Код вида транспорта.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Название вида транспорта.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Описание вида транспорта.
		/// </summary>
		public string Description { get; set; }

		#endregion
	}
}
