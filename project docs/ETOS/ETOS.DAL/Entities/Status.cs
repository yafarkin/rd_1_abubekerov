﻿using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// Описывает сущность "Статус".
	/// Данная сущность описывает общий или промежуточный статус выполнения заявки.
	/// </summary>
	public class Status : IEntity
	{
		#region Fields

		/// <summary>
		/// Код статуса.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Название статуса.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Описание статуса.
		/// </summary>
		public string Description { get; set; }

		#endregion
	}
}
