﻿using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// Описывает сущность "Разрешения".
	/// Данная сущность определяет перечень функциональных возможностей, доступных сотруднику в системе.
	/// </summary>
	public class Permission : IEntity
	{
		#region Fields

		/// <summary>
		/// Код разрешения.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Табельный номер сотрудника, которому устанавливаются разрешения.
		/// </summary>
		public int EmployeeId { get; set; }

		/// <summary>
		/// Флаг, определяющий доступ обычного пользователя.
		/// </summary>
		public bool isUser { get; set; }

		/// <summary>
		/// Флаг, определяющий доступ секретаря.
		/// </summary>
		public bool isSecretary { get; set; }

		/// <summary>
		/// Флаг, определяющий доступ бухгалтера.
		/// </summary>
		public bool isAccountant { get; set; }

		/// <summary>
		/// Флаг, определяющий доступ руководящего лица.
		/// </summary>
		public bool isManager { get; set; }

		/// <summary>
		/// Флаг, определяющий доступ администратора системы.
		/// </summary>
		public bool isAdministrator { get; set; }

		#endregion
	}
}
