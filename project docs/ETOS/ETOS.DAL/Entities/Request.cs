﻿using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// Описывает сущность "Заявка".
	/// Данная сущность представляет собой сформированную и отправленную сотрудником заявку на вызов транспорта.
	/// </summary>
	public class Request : IEntity
	{
		#region Fields

		/// <summary>
		/// Код заявкт
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Табельный номер отправителя заявки.
		/// </summary>
		public int AuthorId { get; set; }

		/// <summary>
		/// Код точки отправки.
		/// Не указывается в том случае, если точка отправки не является точкой интереса (POI).
		/// </summary>
		public int? DeparturePointId { get; set; }

		/// <summary>
		/// Код точки прибытия.
		/// Не указывается в том случае, если точка прибытия не является точкой интереса (POI).
		/// </summary>
		public int? DestinationPointId { get; set; }

		/// <summary>
		/// Адрес места отправки.
		/// Указывается в том случае, если место отправки не является точкой интереса (POI).
		/// </summary>
		public string DepartureAddress { get; set; }

		/// <summary>
		/// Адрес места прибытия.
		/// Указывается в том случае, если место прибытия не является точкой интереса (POI).
		/// </summary>
		public string DestinationAddress { get; set; }

		/// <summary>
		/// Предполагаемые дата и время отправки.
		/// </summary>
		public DateTime DepartureDateTime { get; set; }

		/// <summary>
		/// Дата и время создания заявки.
		/// </summary>
		public DateTime CreatingDateTime { get; set; }

		/// <summary>
		/// Флаг, определяющий наличие багажа.
		/// </summary>
		public bool HasBaggage { get; set; }

		/// <summary>
		/// Комментарий к заявке.
		/// </summary>
		public string Comment { get; set; }

		/// <summary>
		/// Общий километраж заявки.
		/// </summary>
		public double Mileage { get; set; }

		/// <summary>
		/// Общая стоимость заявки.
		/// </summary>
		public decimal Price { get; set; }

		/// <summary>
		/// Код статуса выполнения заявки.
		/// </summary>
		public int StatusId { get; set; }

		#endregion

		#region Navigation properties

		public virtual Status Status { get; set; }
		public virtual Employee Author { get; set; }
		public virtual Location DeparturePoint { get; set; }
		public virtual Location DestinationPoint { get; set; }

		#endregion
	}
}
