using System;

using ETOS.DAL.Interfaces;

namespace ETOS.DAL.Entities
{
	/// <summary>
	/// ��������� �������� "�������".
	/// ������ �������� ������������ ����� ������� ������, ����������� ������� ���������� ��� ����� � �������.
	/// </summary>
	public class Account : IEntity
	{
		#region Fields

		/// <summary>
		/// ��� ��������.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// ��������� ����� ����������, �������� ����������� �������.
		/// </summary>
		public int EmployeeId { get; set; }

		/// <summary>
		/// ��� ������� ������.
		/// </summary>
		public string Username { get; set; }

		/// <summary>
		/// ������ ������� ������.
		/// </summary>
		public string Password { get; set; }

		#endregion

		#region Navigation properites

		public virtual Employee Employee { get; set; }

		#endregion
	}
}
