﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

using ETOS.DAL.Entities;
using ETOS.DAL.Interfaces;

namespace ETOS.DAL.EF
{
	/// <summary>
	/// Обеспечивает соединение с базой данных и работу с её контекстом.
	/// </summary>
	public class EFDataWarehouseContext : DbContext, IDataWarehouseContext
	{
		#region DbSets

		public virtual DbSet<Account> Accounts { get; set; }
		public virtual DbSet<Employee> Employees { get; set; }
		public virtual DbSet<Location> Locations { get; set; }
		public virtual DbSet<Orgstructure> Orgstructures { get; set; }
		public virtual DbSet<Position> Positions { get; set; }
		public virtual DbSet<Priority> Priorities { get; set; }
		public virtual DbSet<TransportType> TransportTypes { get; set; }
		public virtual DbSet<Request> Requests { get; set; }
		public virtual DbSet<Status> Statuses { get; set; }
		public virtual DbSet<Permission> Permissions { get; set; }
		public virtual DbSet<RoutePoint> Routes { get; set; }
		public virtual DbSet<RequestLifecycleEvent> Lifecycles { get; set; }

		#endregion

		#region Constructor

		public EFDataWarehouseContext() : base("name=ETOSDataWarehouse")
		{
			Database.SetInitializer(new EFDataWarehouseInitializer());
		}

		#endregion

		#region Methods

		/// <summary>
		/// Метод, реализующий настройку базы данных при её создании.
		/// </summary>
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

			modelBuilder.Entity<Status>().ToTable("Statuses");
			modelBuilder.Entity<RoutePoint>().ToTable("Routes");
			modelBuilder.Entity<RequestLifecycleEvent>().ToTable("RequestLifecycles");

			modelBuilder.Entity<Contractor>().Property(c => c.Phone).HasMaxLength(16);
			modelBuilder.Entity<Employee>().Property(e => e.Phone).HasMaxLength(16);
		}

		#endregion
	}
}

