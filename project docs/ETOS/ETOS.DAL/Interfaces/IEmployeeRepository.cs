﻿using ETOS.DAL.Entities;
using System;



namespace ETOS.DAL.Interfaces
{
    /// <summary>
    /// Provides specific actions for Account Entity
    /// </summary>
    interface IEmployeeRepository : IRepository<Employee>
    {
       
    }
}
