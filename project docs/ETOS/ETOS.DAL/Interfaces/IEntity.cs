﻿using System;

namespace ETOS.DAL.Interfaces
{
	/// <summary>
	/// Определяет сущности базы данных.
	/// </summary>
	public interface IEntity
	{
		int Id { get; set; }
	}
}
