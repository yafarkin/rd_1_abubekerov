﻿module Model {
    export class Request {
        RequestId: number;
        AuthorFirstName: string;
        AuthorLastName: string;
        DeparturePointName: string;
        DestinationPointName: string;
        DepartureAddress: string;
        DestinationAddress: string;
        DepartureDateTime: Date;
        CreatingDateTime: Date;
        HasBaggage: boolean;
        Comment: string;
        Mileage: number;
        Price: number;
        StatusName: string;
    }
}