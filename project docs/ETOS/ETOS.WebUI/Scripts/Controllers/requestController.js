/// <reference path='angular.d.ts' />
/// <reference path='model.ts' />
var Requests;
(function (Requests) {
    var Controller = (function () {
        function Controller($scope, $http) {
            this.httpService = $http;
            this.refreshRequests($scope);
            var controller = this;
        }
        Controller.prototype.getAllRequests = function (successCallback) {
            this.httpService.get('/api/requests').success(function (data, status) {
                successCallback(data);
            });
        };
        Controller.prototype.refreshRequests = function (scope) {
            this.getAllRequests(function (data) {
                scope.requests = data;
            });
        };
        return Controller;
    }());
    Requests.Controller = Controller;
})(Requests || (Requests = {}));
//# sourceMappingURL=requestController.js.map