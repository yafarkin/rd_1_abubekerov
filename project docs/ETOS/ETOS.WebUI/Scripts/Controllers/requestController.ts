﻿/// <reference path='angular.d.ts' />
/// <reference path='model.ts' />

module Requests {

    export interface Scope {
        sortType: string;
        sortReverse: boolean;
        searchRequest: string;
        requests: Model.Request[];
    }

    export class Controller {
        private httpService: any;

        constructor($scope: Scope, $http: any) {
            this.httpService = $http;

            this.refreshRequests($scope);

            var controller = this;
        }

        getAllRequests(successCallback: Function): void {
            this.httpService.get('/api/requests').success(function (data, status) {
                successCallback(data);
            });
        }
        refreshRequests(scope: Scope) {
            this.getAllRequests(function (data) {
                scope.requests = data;
            });
        }

    }
}
