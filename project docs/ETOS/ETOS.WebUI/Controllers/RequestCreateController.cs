﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ETOS.WebUI.Controllers
{
    public class RequestCreateController : ApiController
    {
        // GET: api/RequestCreate
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/RequestCreate/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/RequestCreate
        public void Post([FromBody]JObject value)
        {
            var v1 = value.GetValue("v1");
            var v2 = value.Value<string>("v2");
            var v3 = value.Value<string>("v3");
            var v4 = value.Value<string>("v4");
            var v5 = value.Value<string>("v5");
        }

        // PUT: api/RequestCreate/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/RequestCreate/5
        public void Delete(int id)
        {
        }
    }
}
