﻿using ETOS.Core.Services;
using ETOS.Core.Services.Abstract;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using static ETOS.WebUI.ViewModels.RequestsViewModels;

namespace ETOS.WebUI.Controllers
{
    /// <summary>
    /// Контроллер для работы с заявками работников
    /// </summary>
    [Authorize]
    public class RequestsController : ApiController
    {

        #region Fields

        IRequestService _requestService;
        #endregion 

        #region Constructor

        public RequestsController(IRequestService reqService)
        {
            _requestService = reqService;
        }

        #endregion 

        // GET api/<controller>
        [HttpGet]
        public IEnumerable<RequestsViewModel> GetAll()
        {
            IEnumerable<RequestsViewModel> model = new List<RequestsViewModel>();
            var filter = new FilterService();

            var mapList = _requestService.GetRequest(filter);
            model = mapList.Select(x => new RequestsViewModel
            {
                RequestId = x.RequestId,
                AuthorFirstName = x.AuthorFirstName,
                AuthorLastName = x.AuthorLastName,
                DeparturePointName = x.DeparturePointName,
                DestinationPointName = x.DestinationPointName,
                DepartureAddress = x.DepartureAddress,
                DestinationAddress = x.DestinationAddress,
                DepartureDateTime = x.DepartureDateTime,
                CreatingDateTime = x.CreatingDateTime,
                HasBaggage = x.HasBaggage,
                Comment = x.Comment,
                Mileage = x.Mileage,
                Price = x.Price,
                StatusName = x.StatusName
            });
            return model;
        }
    }
}