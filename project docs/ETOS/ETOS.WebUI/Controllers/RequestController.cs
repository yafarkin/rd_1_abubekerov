﻿using System.Collections.Generic;
using ETOS.WebUI.ViewModels;
using System.Web.Mvc;
using ETOS.Core.DTO;
using ETOS.Core.Services;
using static ETOS.WebUI.ViewModels.RequestsViewModels;
using ETOS.Core.Services.Abstract;
using System.Linq;

namespace ETOS.WebUI.Controllers
{
    /// <summary>
    /// Контроллер, обеспечивающий работу с заявками на уровне представления.
    /// </summary>
    [Authorize]
    public class RequestController : Controller
    {
        #region Fields

        IRequestService _requestService;
        #endregion 

        #region Constructor

        public RequestController(IRequestService reqService)
        {
            _requestService = reqService;
        }

        #endregion 

        #region Methods

        /// <summary>
        /// Get-версия метода, возвращающая представление с полным перечнем заявок.
        /// </summary>
        public ViewResult Index()
        {
            //IEnumerable<RequestsViewModel> model = new List<RequestsViewModel>();
            //var filter = new FilterService();

            //var mapList = _requestService.GetRequest(filter);
            //model = mapList.Select(x => new RequestsViewModel
            //{
            //    RequestId = x.RequestId,
            //    AuthorFirstName = x.AuthorFirstName,
            //    AuthorLastName = x.AuthorLastName,
            //    DeparturePointName = x.DeparturePointName,
            //    DestinationPointName = x.DestinationPointName,
            //    DepartureAddress = x.DepartureAddress,
            //    DestinationAddress = x.DestinationAddress,
            //    DepartureDateTime = x.DepartureDateTime,
            //    CreatingDateTime = x.CreatingDateTime,
            //    HasBaggage = x.HasBaggage,
            //    Comment = x.Comment,
            //    Mileage = x.Mileage,
            //    Price = x.Price,
            //    StatusName = x.StatusName
            //});
            //return View(model);
            return View();
        }

        //[HttpPost]
        //public ActionResult Index(RequestsViewModel model)
        //{
        //    return View(model);
        //}


        ///// <summary>
        ///// Get-версия метода, возвращающая представление для создания новой заявки.
        ///// </summary>
        //[HttpGet]
        //public ActionResult Create()
        //{
        //    return View();
        //}

        ///// <summary>
        ///// Get-версия метода, возвращающая представление для просмотра деталей заявки.
        ///// </summary>
        //[HttpGet]
        //public ActionResult Detail()
        //{
        //    return View();
        //}

        #endregion
    }
}