﻿using System;

namespace ETOS.WebUI.ViewModels
{
    /// <summary>
    /// ViewModel-и для страницы с заявками
    /// </summary>
    public class RequestsViewModels
    {

        /// <summary>
        /// Модель для страницы мои заявки - краткий вид
        /// </summary>
        public class RequestsViewModel
        {
            /// <summary>
            /// Список заявок
            /// </summary>
            /// <summary>
            /// Идентефикационный номер заявки
            /// </summary>
            public int RequestId { get; set; }

            /// <summary>
            /// Имя автора заявки
            /// </summary>
            public string AuthorFirstName { get; set; }
            /// <summary>
            /// Фамилия автора заявки
            /// </summary>
            public string AuthorLastName { get; set; }

            /// <summary>
            /// Адрес отправки по коду точки
            /// </summary>
            public string DeparturePointName { get; set; }

            /// <summary>
            /// Адрес точки прибытия по коду
            /// </summary>
            public string DestinationPointName { get; set; }

            /// <summary>
            /// Адрес точки отправки
            /// </summary>
            public string DepartureAddress { get; set; }
            /// <summary>
            /// Адрес точки прибытия
            /// </summary>
            public string DestinationAddress { get; set; }
            /// <summary>
            /// Время отправки
            /// </summary>
            public DateTime DepartureDateTime { get; set; }
            /// <summary>
            /// Время создания заявки
            /// </summary>
            public DateTime CreatingDateTime { get; set; }
            /// <summary>
            /// Наличие багажа,да/нет
            /// </summary>
            public bool HasBaggage { get; set; }
            /// <summary>
            /// Комментарии к заявке
            /// </summary>
            public string Comment { get; set; }
            /// <summary>
            /// Километраж
            /// </summary>
            public double Mileage { get; set; }
            /// <summary>
            /// Общая сумма за поездку
            /// </summary>
            public decimal Price { get; set; }
            /// <summary>
            /// Идентефикационный номер статуса
            /// </summary>
            public string StatusName { get; set; }

            /// <summary>
            /// Фильтр для заявок
            /// </summary>
        }
    }
}