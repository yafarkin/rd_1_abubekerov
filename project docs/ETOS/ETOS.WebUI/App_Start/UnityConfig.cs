using ETOS.Core.Services;
using ETOS.Core.Services.Abstract;
using ETOS.DAL.EF;
using ETOS.DAL.Entities;
using ETOS.DAL.Interfaces;
using ETOS.DAL.Repositories;
using ETOS.WebUI.Security;
using ETOS.WebUI.Security.Abstract;
using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;

namespace ETOS.WebUI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IAccountService, AccountService>();
            container.RegisterType<IAccountRepository, AccountsRepository>();
            container.RegisterType<IAuthProvider, FormAuthProvider>();

            container.RegisterType<IRequestService, RequestService>();
            container.RegisterType<IRepository<Request>, RequestsRepository>();
            container.RegisterType<IDataWarehouseContext, EFDataWarehouseContext>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}