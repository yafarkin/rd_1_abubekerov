﻿using ETOS.Core.Services;
using ETOS.Core.Services.Abstract;
using ETOS.DAL.Entities;
using ETOS.DAL.Interfaces;
using ETOS.DAL.Repositories;
using ETOS.WebUI.Security;
using ETOS.WebUI.Security.Abstract;
using Microsoft.Practices.Unity;
using System.Web.Http;

namespace ETOS.WebUI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
