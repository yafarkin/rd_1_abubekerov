﻿using System;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using ETOS.DAL.EF;
using ETOS.DAL.Entities;
using ETOS.DAL.Repositories;

namespace ETOS.Tests
{
	[TestClass]
	public class DALTests
	{
		/// <summary>
		/// Тест, реализующий проверку корректности добавления информации о новых сотрудниках в базу данных.
		/// </summary>
		[TestMethod]
		public void TestCreateUser()
		{
			// Arrange.
			var expectedName = "Егор";
			var expectedLastName = "Квасов";

			// Act.
			using (var database = new EFDataWarehouseContext())
			{
				using (var employeeRepository = new EmployeesRepository(database))
				{
					var newOrgstructue = database.Orgstructures.Find(1);
					var newLocation = database.Locations.Find(1);
					var newPriority = database.Priorities.Find(1);
					var newTransportType = database.TransportTypes.Find(1);
					var newPosition = database.Positions.Find(1);


					var newEmployee = new Employee
					{
						Email = "ceo@epam.com",
						Firstname = expectedName,
						Lastname = expectedLastName,
						Patronymic = "Напиткович",
						Phone = "+79276493847",
						Location = newLocation,
						Orgstructure = newOrgstructue,
						Position = newPosition
					};
 
					employeeRepository.Create(newEmployee);
					employeeRepository.Save();

					var actualResult = database.Employees.Find(newEmployee.Id);

					// Assert.
					Assert.AreEqual(expectedName, actualResult.Firstname);
					Assert.AreEqual(expectedLastName, actualResult.Lastname);
				}
			}
		}

		[TestMethod]
		// Проверка что данные удаляются из БД
		public void TestFindDeleteMethod()
		{
			//arrange
			var db = new EFDataWarehouseContext();


			EmployeesRepository Employee = new EmployeesRepository(db);

			//act   
			var newEmployee = Employee.Find(emp => (emp.Firstname == "Егор")).FirstOrDefault();

			if (newEmployee != null)
			{
				int id = newEmployee.Id;
				Employee.Delete(id);
				Employee.Save();
			}

			newEmployee = Employee.Find(emp => (emp.Firstname == "Егор")).FirstOrDefault();

			// assert
			Assert.AreEqual(null, newEmployee);


		}
	}
}
