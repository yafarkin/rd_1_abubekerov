﻿using System;


namespace Task1
{
    /// <summary>
    /// Provides person's information and calculation CRC
    /// </summary>    
    class Person
    {
        int crc;
        int age;
        string name;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                CalcCrc();
            }
        }
        public int Age
        {
            get
            {
                return age;
            }
                
            set
            {
                age = value;
                CalcCrc();
            }
        }
        public int Crc
        {
            private set
            {
                crc = value;
            }

            get
            {                
                return crc;
            }
        }

        /// <summary>
        /// Constructor sets fields name and age
        /// </summary>   
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
            CalcCrc();
        }

        /// <summary>
        /// Calculate person's crc using age and name's lengths
        /// </summary>   
        void CalcCrc()
        {
            Crc = Age * Name.Length;
        }

    }

    class Program
    {

        static void Main(string[] args)
        {
            string name;
            int age;

            do
            {
                Console.WriteLine("Enter name:");
                name = Console.ReadLine().Trim();
            } while (name == "");

            do
            {
                Console.WriteLine("Enter age:");
            } while (!int.TryParse(Console.ReadLine(), out age) || age <= 0 || age > 100);

            Person man = new Person(name, age);

            var crc = man.Crc;
            var res_name = man.Name;
            var res_age = man.Age;
            var res_crc = man.Crc;

            Console.WriteLine("{0} Person: {0}Name - {1}{0}Age - {2}{0}CRC - {3}", Environment.NewLine, res_name, res_age, res_crc);

            Console.ReadKey();

        }
    }
}

