﻿using System;
using System.Collections.Generic;


namespace Task2
{
    /// <summary>
    /// Provides company's parameters like pofit , products and subcompanies
    /// </summary> 
    abstract class EPAM_company
    {
        private static decimal profit;
        private static int subcompaniesCount;
        private static List<string> productsNames = new List<string> { };

        protected string productName { get; set; }
        protected decimal productPrice { get; set; }


        public static int SubcompaniesCount
        {
            get
            {
                return subcompaniesCount;
            }

            protected set
            {
                subcompaniesCount = value;
            }
        }

        public static List<string> ProductsNames
        {
            get
            {
                return productsNames;
            }           
        }

        public static decimal Profit
        {
            get
            {
                return profit;
            }           
        }

        protected void profitAdd(decimal value)
        {
            profit+=value;
        }

        protected void productsNamesAdd(string value)
        {
            ProductsNames.Add(value);
        }


        abstract protected void CalcProfit();

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="productName">name of the product </param>
        /// <param name="productPrice">price of the product</param>        
        public EPAM_company(string productName, decimal productPrice)
        {
            this.productName = productName;
            this.productPrice = productPrice;
            SubcompaniesCount++;
        }

    }

    /// <summary>
    /// Provides EPAM subcompany : Toys 
    /// </summary>    
    class Toys : EPAM_company
    {
        int toysCount;

        /// <summary>
        /// Toys Constructor sets parameters and calculates profit
        /// </summary>
        /// <param name="productName">name of the product </param>
        /// <param name="productPrice">price of the product</param>   
        /// <param name="toysCount">count of toys released</param>  
        public Toys(string productName, decimal productPrice, int toysCount) : base(productName, productPrice)
        {
            this.toysCount = toysCount;
            CalcProfit();
        }

        /// <summary>
        /// Calculates speciefic profit  for Toys company 
        /// </summary>        
        sealed protected override void CalcProfit()
        {
            profitAdd(productPrice* toysCount);
            productsNamesAdd(productName);
        }
    }

    class FantasticSoftware : EPAM_company
    {
        const int addedValue = 3000;
        int difficulty;

        /// <summary>
        /// FantasticSoftware Constructor sets parameters and calculates profit
        /// </summary>
        /// <param name="productName">name of the product </param>
        /// <param name="productPrice">price of the product</param>   
        /// <param name="difficulty">difficulty of the software product to create</param>  
        public FantasticSoftware(string productName, decimal productPrice, int difficulty) : base(productName, productPrice)
        {
            this.difficulty = difficulty;
            CalcProfit();
        }

        /// <summary>
        /// Calculates speciefic profit  for FantasticSoftware company using added Value
        /// </summary> 
        sealed protected override void CalcProfit()
        {
            profitAdd(productPrice* difficulty + addedValue);
            productsNamesAdd(productName);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            decimal price;
            int count, difficulty;

            Console.WriteLine("Welcome to EPAM reports system: {0}{0}Please enter data for next EPAM subcompanies{0}{0} Toys company released a new product - Barbie.", Environment.NewLine);

            do
            {
                Console.WriteLine("Enter price of a one doll:");
            } while (!decimal.TryParse(Console.ReadLine(), out price));

            do
            {
                Console.WriteLine("Enter count of dolls was released :");
            } while (!int.TryParse(Console.ReadLine(), out count));

            Toys barbie = new Toys("Barbie", price, count);


            Console.WriteLine("FantasticProducts company released a new product - TaxiProvider");

            do
            {
                Console.WriteLine("Enter price of this product:");
            } while (!decimal.TryParse(Console.ReadLine(), out price));

            do
            {
                Console.WriteLine("Enter the difficulty of this project [0..10]:");
            } while (!int.TryParse(Console.ReadLine(), out difficulty) || difficulty < 0 || difficulty > 10);
            

            FantasticSoftware TaxiProj = new FantasticSoftware("TaxiProvider", price, difficulty);
            
            Console.WriteLine("{0} Thank you {0}{0} EPAM products report:", Environment.NewLine);
            Console.WriteLine("Subcompanies count : {0}", EPAM_company.SubcompaniesCount);
            foreach (string s in EPAM_company.ProductsNames) Console.WriteLine("---{0}{1}", s, Environment.NewLine);
            
            Console.WriteLine("Profit: {0:C}", EPAM_company.Profit);

            Console.ReadLine();

        }
    }
}
