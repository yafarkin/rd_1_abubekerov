﻿using System;


namespace Task4_Matrix
{
    public static class MatrixOperations
    {
        /// <summary>
        /// Throw exception if matrix referenced to null 
        /// </summary>
        /// <param name="matrix">Matrix A</param>             
        /// <exception cref="ArgumentNullException">Thrown when input matrix referenced to Null</exception>
        private static void CheckMatrixIfNull(Arrays matrix)
        {
            if (matrix == null) throw new ArgumentNullException("input matrix", "  Error: NULL reference");
        }

        /// <summary>
        /// Throw exception if matrix A size not equal to B matrix size
        /// </summary>
        /// <param name="matA">Matrix A</param>             
        /// <param name="matB">Matrix B</param>             
        /// <exception cref="FormatException">Thrown matrix A size not equal to B matrix size</exception>
        private static void CheckIfMatrixEqualSize(Arrays matA,Arrays matB)
        {
            if (matA.MaxRows != matB.MaxRows || matA.MaxCols != matB.MaxCols) throw new System.FormatException("Can't sum - size of matrix A and B are different");
        }

        /// <summary>
        /// Provides Transposition of matrix ^ T 
        /// </summary>
        /// <param name="matA">Matrix A</param>        
        /// <returns>Arrays object as result of Transposition </returns>
        /// <exception cref="ArgumentNullException">Thrown when input matrix referenced to Null</exception>
        public static Arrays Transposition(Arrays matA)
        {
            CheckMatrixIfNull(matA);

             Arrays res = new Arrays(matA.MaxCols, matA.MaxRows);

            for (int i = 0; i < res.MaxRows; i++)
            {
                for (int j = 0; j < res.MaxCols; j++)
                {
                    res[i, j] = matA[j, i];
                }
            }

            return res;

        }

        /// <summary>
        /// Provides multiplication of 2 matrix. A x B 
        /// </summary>
        /// <param name="matA">Matrix A</param>
        /// <param name="matB">Matrix B</param>
        /// <returns>Arrays object as result of multiplication </returns>
        /// <exception cref="ArgumentNullException">Thrown when input matrix referenced to Null</exception>
        /// <exception cref="System.FormatException">Thrown when size of matrix A and B are different</exception>
        public static Arrays Multiplication(Arrays matA, Arrays matB)
        {
            CheckMatrixIfNull(matA);
            CheckMatrixIfNull(matB);
            
            if (matA.MaxCols != matB.MaxRows) throw new FormatException("Error: Colums count of A not equal to rows count of B - can't multiplicate");

            Arrays r = new Arrays(matA.MaxRows, matB.MaxCols);

            for (int i = 0; i < matA.MaxRows; i++)
            {
                for (int j = 0; j < matB.MaxCols; j++)
                {
                    for (int k = 0; k < matB.MaxRows; k++)
                    {
                        r[i, j] += matA[i, k] * matB[k, j];
                    }
                }
            }
            return r;
        }

        /// <summary>
        /// Provides multiplication of 2 matrix. A x B 
        /// </summary>
        /// <param name="matrix">Matrix A</param>
        /// <param name="n">factor</param>
        /// <returns>Arrays object as result of multiplication by nubmer 'n' </returns>
        /// <exception cref="ArgumentNullException">Thrown when input matrix referenced to Null</exception>
        public static Arrays MultiplicationN(Arrays matrix, int n)
        {
            CheckMatrixIfNull(matrix);

            Arrays r = new Arrays(matrix.MaxRows, matrix.MaxCols);

            for (int i = 0; i < matrix.MaxRows; i++)
            {
                for (int j = 0; j < matrix.MaxCols; j++)
                {
                    r[i, j] = matrix[i, j] * n;
                }
            }
            return r;
        }

        /// <summary>
        /// Provides sum of 2 matrix. A + B 
        /// </summary>
        /// <param name="matA">Matrix A</param>
        /// <param name="matB">Matrix B</param>
        /// <returns>Arrays object as result of sum </returns>
        /// <exception cref="ArgumentNullException">Thrown when input matrix referenced to Null</exception>
        /// <exception cref="System.FormatException">Thrown when size of matrix A and B are different</exception>
        public static Arrays Sum(Arrays matA, Arrays matB)
        {
            CheckMatrixIfNull(matA);
            CheckMatrixIfNull(matB);
            CheckIfMatrixEqualSize(matA, matB);

            Arrays r = new Arrays(matA.MaxRows, matB.MaxCols);


            for (int i = 0; i < matA.MaxRows; i++)
            {
                for (int j = 0; j < matB.MaxCols; j++)
                {
                    r[i, j] = matA[i, j] + matB[i, j];
                }
            }
            return r;
        }

        /// <summary>
        /// Provides Subtraction of 2 matrix. A - B 
        /// </summary>
        /// <param name="matA">Matrix A</param>
        /// <param name="matB">Matrix B</param>
        /// <returns>Arrays object as result of Subtraction </returns>
        /// <exception cref="ArgumentNullException">Thrown when input matrix referenced to Null</exception>
        /// <exception cref="System.FormatException">Thrown when size of matrix A and B are different</exception>
        public static Arrays Subtraction(Arrays matA, Arrays matB)
        {
            CheckMatrixIfNull(matA);
            CheckMatrixIfNull(matB);

            CheckIfMatrixEqualSize(matA, matB);

            Arrays r = new Arrays(matA.MaxRows, matB.MaxCols);


            for (int i = 0; i < matA.MaxRows; i++)
            {
                for (int j = 0; j < matB.MaxCols; j++)
                {
                    r[i, j] = matA[i, j] - matB[i, j];
                }
            }
            return r;
        }


        /// <summary>
        /// Provides equality of 2 matrix. A and B 
        /// </summary>
        /// <param name="matA">Matrix A</param>
        /// <param name="matB">Matrix B</param>
        /// <returns>bool A equal B </returns>
        /// <exception cref="ArgumentNullException">Thrown when input matrix referenced to Null</exception>
        public static bool Equal(Arrays matA, Arrays matB)
        {
            CheckMatrixIfNull(matA);
            CheckMatrixIfNull(matB);
            
            if (matA.MaxRows != matB.MaxRows || matA.MaxCols != matB.MaxCols) return false;

            for (int i = 0; i < matA.MaxRows; i++)
            {
                for (int j = 0; j < matB.MaxCols; j++)
                {
                    if (matA[i, j] != matB[i, j]) return false;
                }
            }

            return true;
        }
    }
}
