﻿using System;


namespace Task4_Matrix
{
    /// <summary>
    /// Provides work with 2D arrays
    /// </summary>
    public class Arrays
    {
   
        Array array;
        int maxRows, maxCols;
        public int MaxRows
        {
            get
            {
                return maxRows;
            }

            private set
            {
                maxRows = value;
            }
        }
        public int MaxCols
        {
            get
            {
                return maxCols;
            }

            private set
            {
                maxCols = value;
            }
        }

        /// <summary>
        /// Array's indexator
        /// </summary>
        public int this[int row, int col]
        {
            get { return (int)array.GetValue(row, col); }
            set { array.SetValue(value, row, col); }
        }

        /// <summary>
        /// Arrays constructor creates array of specified size and fill it by values. random or 0
        /// </summary>
        /// <param name="rows">new array rows count</param>        
        ///  <param name="cols">new array columns count</param>                
        ///  <param name="randomValuesFlag">true - random initial values, false - 0 values</param>        
        ///  <exception cref="ArgumentNullException">Thrown when matrix rows/columns are out of range 0..10</exception>
        public Arrays(int rows, int cols, bool randomValuesFlag = false)
        {
            var rnd = new Random(Guid.NewGuid().GetHashCode());

            if (rows <=0 || rows >10 || cols<=0 || cols >10)
            {
                throw new ArgumentNullException("input size", "  Error: size of matrix must be 0..10");
            }

            MaxRows = rows;
            MaxCols = cols;

            array = Array.CreateInstance(typeof(int), MaxRows, MaxCols);
           
            for (int i = 0; i < MaxRows; i++)
            {
                for (int j = 0; j < MaxCols; j++)
                {
                    if (randomValuesFlag) this[i, j] = rnd.Next(0, 10); else this[i, j] = 0;
                }
            }
        }


    }
}
