﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Task4_Matrix
{


    /// <summary>
    /// MainWindow.xaml logic
    /// </summary>
    public partial class MainWindow : Window
    {
        Arrays matrixA, matrixB, matrixC;

        /// <summary>
        /// Provides output Arrays object to console
        /// </summary>
        private void ShowToConsole(Arrays matrix)
        {
            for (int i = 0; i < matrix.MaxRows; i++)
            {
                for (int j = 0; j < matrix.MaxCols; j++)
                {
                    Console.Write(" {0} ", matrix[i, j]);
                }
                Console.Write(Environment.NewLine);
            }
        }

        /// <summary>
        /// Provides output Arrays object to textbox
        /// </summary>
        private void ShowMatrixToTextBlock(TextBox txtbx, Arrays matrix)
        {
            txtbx.Clear();
            for (int i = 0; i < matrix.MaxRows; i++)
            {
                for (int j = 0; j < matrix.MaxCols; j++)
                {
                    txtbx.Text += string.Format(" {0} ", matrix[i, j]);
                }
                txtbx.Text += Environment.NewLine;
            }
        }


        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Generates new values of matrix A
        /// </summary>     
        private void btnGenerateA_Click(object sender, RoutedEventArgs e)
        {
            int rows;
            int cols;
            if (int.TryParse(txtbxRowsA.Text, out rows) && int.TryParse(txtbxColsA.Text, out cols))
            {
                if (rows > 0 && rows <= 10 && cols > 0 && cols <= 10)
                {
                    try
                    {
                        matrixA = new Arrays(rows, cols, true);
                    }
                    catch (ArgumentNullException ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                    ShowMatrixToTextBlock(txtmA, matrixA);
                    txtmC.Clear();
                }
                else
                {
                    MessageBox.Show("Value must be in range [1..10]");
                }
            }
        }

        /// <summary>
        /// Run operation
        /// </summary>     
        private void btnGenerateC_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                matrixC = null;

                if (rbMpx.IsChecked == true) matrixC = MatrixOperations.Multiplication(matrixA, matrixB);
                if (rbSum.IsChecked == true) matrixC = MatrixOperations.Sum(matrixA, matrixB);
                if (rbSubtr.IsChecked == true) matrixC = MatrixOperations.Subtraction(matrixA, matrixB);

                if (rbTA.IsChecked == true || rbTB.IsChecked == true)
                {
                    if (rbTA.IsChecked == true) matrixC = MatrixOperations.Transposition(matrixA);
                    else matrixC = MatrixOperations.Transposition(matrixB);
                };

                if (rbMpxAn.IsChecked == true || rbMpxBn.IsChecked == true)
                {
                    int factor;
                    if (int.TryParse(txtbxN.Text, out factor) && factor >= -100 && factor <= 100)
                    {
                        if (rbMpxAn.IsChecked == true) matrixC = MatrixOperations.MultiplicationN(matrixA, factor);
                        else matrixC = MatrixOperations.MultiplicationN(matrixB, factor);

                        ShowMatrixToTextBlock(txtmC, matrixC);
                    }
                    else
                    {
                        MessageBox.Show("Wrong factor: n>=-100 and n<=100");
                    }
                }

                if (matrixC != null) ShowMatrixToTextBlock(txtmC, matrixC);


                if (rbEqual.IsChecked == true)
                {
                    if (MatrixOperations.Equal(matrixA, matrixB)) MessageBox.Show("Matrix Equal"); else MessageBox.Show("Matrix are different");
                    txtmC.Clear();
                };
            }
            catch (ArgumentNullException exc)
            {
                MessageBox.Show(exc.Message);
            }
            catch (FormatException exc)
            {
                MessageBox.Show(exc.Message);
            }

        }

        private void txtbxN_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789 - ".IndexOf(e.Text) < 0;
        }

        private void txtbxRowsA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        /// <summary>
        /// Generates new values of matrix B
        /// </summary>     
        private void btnGenerateB_Click(object sender, RoutedEventArgs e)
        {
            int rows;
            int cols;
            if (int.TryParse(txtbxRowsB.Text, out rows) && int.TryParse(txtbxColsB.Text, out cols))
            {
                if (rows > 0 && rows <= 10 && cols > 0 && cols <= 10)
                {
                    try
                    {
                        matrixB = new Arrays(rows, cols, true);
                    }
                    catch (ArgumentNullException ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                    ShowMatrixToTextBlock(txtmB, matrixB);
                    txtmC.Clear();
                }
                else
                {
                    MessageBox.Show("Value must be in range [1..10]");
                }
            }
        }

    }
}
