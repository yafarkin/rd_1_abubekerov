﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task4_Matrix;


namespace Task4MatrixTest
{
    [TestClass]
    public class Task4MatrixTests
    {
        [TestMethod]
        //public static MatrixOperations Multiplication(Arrays matA, Arrays matB)
        public void Multiplication_2x3_3x2()
        {
            // arrange 
            Arrays matrixA = new Arrays(3, 2, false);
            Arrays matrixB = new Arrays(2, 3, false);
            Arrays expected = new Arrays(3, 3, false);
            Arrays matrixC;

            matrixB[0, 0] = 7;
            matrixB[0, 1] = 8;
            matrixB[0, 2] = 9;
            matrixB[1, 0] = 10;
            matrixB[1, 1] = 11;
            matrixB[1, 2] = 12;

            matrixA[0, 0] = 1;
            matrixA[0, 1] = 4;
            matrixA[1, 0] = 2;
            matrixA[1, 1] = 5;
            matrixA[2, 0] = 3;
            matrixA[2, 1] = 6;

            expected[0, 0] = 47;
            expected[0, 1] = 52;
            expected[0, 2] = 57;
            expected[1, 0] = 64;
            expected[1, 1] = 71;
            expected[1, 2] = 78;
            expected[2, 0] = 81;
            expected[2, 1] = 90;
            expected[2, 2] = 99;


            //  act

            matrixC = MatrixOperations.Multiplication(matrixA, matrixB);



            // assert

            Assert.AreEqual(matrixC.MaxCols, expected.MaxCols);
            Assert.AreEqual(matrixC.MaxRows, expected.MaxRows);

            for (int i = 0; i < matrixC.MaxRows; i++)
            {
                for (int j = 0; j < matrixC.MaxCols; j++)
                {
                    Assert.AreEqual(matrixC[i, j], expected[i, j]);
                }
            }

        }

        [TestMethod]
        //public static MatrixOperations Transposition(Arrays matA)
        public void Transposition_2x3()
        {
            // arrange 
            Arrays matrixA = new Arrays(2, 3, false);
            Arrays expected = new Arrays(3, 2, false);
            Arrays matrixC;

            matrixA[0, 0] = 2;
            matrixA[0, 1] = 9;
            matrixA[0, 2] = 8;
            matrixA[1, 0] = 5;
            matrixA[1, 1] = 3;
            matrixA[1, 2] = 2;

            expected[0, 0] = 2;
            expected[0, 1] = 5;
            expected[1, 0] = 9;
            expected[1, 1] = 3;
            expected[2, 0] = 8;
            expected[2, 1] = 2;


            //  act

            matrixC = MatrixOperations.Transposition(matrixA);



            // assert

            Assert.AreEqual(matrixC.MaxCols, expected.MaxCols);
            Assert.AreEqual(matrixC.MaxRows, expected.MaxRows);

            for (int i = 0; i < matrixC.MaxRows; i++)
            {
                for (int j = 0; j < matrixC.MaxCols; j++)
                {
                    Assert.AreEqual(matrixC[i, j], expected[i, j]);
                }
            }

        }

        [TestMethod]
        //public static MatrixOperations MultiplicationN(Arrays matA)
        public void MultiplicationN_2x3()
        {
            // arrange             
            Arrays matrix = new Arrays(2, 3, false);
            Arrays expected = new Arrays(2, 3, false);
            Arrays matrixC;

            matrix[0, 0] = 9;
            matrix[0, 1] = 7;
            matrix[0, 2] = 2;
            matrix[1, 0] = 6;
            matrix[1, 1] = 8;
            matrix[1, 2] = 2;


            expected[0, 0] = 45;
            expected[0, 1] = 35;
            expected[0, 2] = 10;
            expected[1, 0] = 30;
            expected[1, 1] = 40;
            expected[1, 2] = 10;


            //  act

            matrixC = MatrixOperations.MultiplicationN(matrix, 5);


            // assert

            Assert.AreEqual(matrixC.MaxCols, expected.MaxCols);
            Assert.AreEqual(matrixC.MaxRows, expected.MaxRows);

            for (int i = 0; i < matrixC.MaxRows; i++)
            {
                for (int j = 0; j < matrixC.MaxCols; j++)
                {
                    Assert.AreEqual(matrixC[i, j], expected[i, j]);
                }
            }

        }

        [TestMethod]
        //public static MatrixOperations Sum(Arrays matA,Arrays matB)
        public void Sum_2x3()
        {
            // arrange             
            Arrays matrixA = new Arrays(2, 3, false);
            Arrays matrixB = new Arrays(2, 3, false);
            Arrays expected = new Arrays(2, 3, false);
            Arrays matrixC;

            matrixA[0, 0] = 1;
            matrixA[0, 1] = 2;
            matrixA[0, 2] = 3;
            matrixA[1, 0] = 4;
            matrixA[1, 1] = 5;
            matrixA[1, 2] = 6;

            matrixB[0, 0] = 1;
            matrixB[0, 1] = 2;
            matrixB[0, 2] = 3;
            matrixB[1, 0] = 4;
            matrixB[1, 1] = 5;
            matrixB[1, 2] = 6;


            expected[0, 0] = 2;
            expected[0, 1] = 4;
            expected[0, 2] = 6;
            expected[1, 0] = 8;
            expected[1, 1] = 10;
            expected[1, 2] = 12;


            //  act

            matrixC = MatrixOperations.Sum(matrixA, matrixB);



            // assert

            Assert.AreEqual(matrixC.MaxCols, expected.MaxCols);
            Assert.AreEqual(matrixC.MaxRows, expected.MaxRows);

            for (int i = 0; i < matrixC.MaxRows; i++)
            {
                for (int j = 0; j < matrixC.MaxCols; j++)
                {
                    Assert.AreEqual(matrixC[i, j], expected[i, j]);
                }
            }

        }
        //public static bool Equal(Arrays matA, Arrays matB)
        [TestMethod]
        //public static MatrixOperations Sum(Arrays matA,Arrays matB)
        public void Subtraction_2x3()
        {
            // arrange             
            Arrays matrixA = new Arrays(2, 3, false);
            Arrays matrixB = new Arrays(2, 3, false);
            Arrays expected = new Arrays(2, 3, false);
            Arrays matrixC;

            matrixA[0, 0] = 1;
            matrixA[0, 1] = 2;
            matrixA[0, 2] = 3;
            matrixA[1, 0] = 40;
            matrixA[1, 1] = 5;
            matrixA[1, 2] = 6;

            matrixB[0, 0] = 1;
            matrixB[0, 1] = 2;
            matrixB[0, 2] = 3;
            matrixB[1, 0] = 4;
            matrixB[1, 1] = 5;
            matrixB[1, 2] = 6;


            expected[0, 0] = 0;
            expected[0, 1] = 0;
            expected[0, 2] = 0;
            expected[1, 0] = 36;
            expected[1, 1] = 0;
            expected[1, 2] = 0;


            //  act

            matrixC = MatrixOperations.Subtraction(matrixA, matrixB);



            // assert

            Assert.AreEqual(matrixC.MaxCols, expected.MaxCols);
            Assert.AreEqual(matrixC.MaxRows, expected.MaxRows);

            for (int i = 0; i < matrixC.MaxRows; i++)
            {
                for (int j = 0; j < matrixC.MaxCols; j++)
                {
                    Assert.AreEqual(matrixC[i, j], expected[i, j]);
                }
            }

        }


        [TestMethod]
        //public static bool Equal(Arrays matA, Arrays matB)
        public void Equal_2x3()
        {
            // arrange             
            Arrays matrixA = new Arrays(2, 3, false);
            Arrays matrixB = new Arrays(2, 3, false);
            bool expected = true;
            bool res = false;

            matrixA[0, 0] = 1;
            matrixA[0, 1] = 2;
            matrixA[0, 2] = 3;
            matrixA[1, 0] = 4;
            matrixA[1, 1] = 5;
            matrixA[1, 2] = 6;

            matrixB[0, 0] = 1;
            matrixB[0, 1] = 2;
            matrixB[0, 2] = 3;
            matrixB[1, 0] = 4;
            matrixB[1, 1] = 5;
            matrixB[1, 2] = 6;


            //  act
            res = MatrixOperations.Equal(matrixA, matrixB);



            // assert
            Assert.AreEqual(res, expected);


        }
    }
}

