﻿using System;


namespace Task3_extention
{
    /// <summary>
    /// Class provides extention methods for standart "string" class
    /// </summary> 
    public static class myStringExtention
    {
        /// <summary>
        /// Calculate count of words in string, using spaces as separator for words
        /// </summary> 
        /// <param name="str"> string with words </param>
        /// <returns>integer count of words</returns>
        public static int GetWordCount(this string str)
        {
            var wordsCnt = 0;
            var prevChar = 'a';

            str=str.Replace('\t', ' ');
            str = str.Trim();
            if (str.Length == 0) return 0;

            foreach (char c in str)
            {                
                if (c == ' ' && c!=prevChar) wordsCnt++;
                prevChar = c;
            }
            return ++wordsCnt;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter similar words:");
            var data = Console.ReadLine();
            var wordsCnt = data.GetWordCount();

            Console.WriteLine("String : {0}{2}Words count:{1}", data, wordsCnt, Environment.NewLine);
            Console.ReadKey();
        }
    }
}
