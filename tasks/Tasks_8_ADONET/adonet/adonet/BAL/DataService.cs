﻿using adonet.DAL;
using PagedList;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using static adonet.BAL.Models.HomeViewModels;

namespace adonet.BAL
{
    /// <summary>
    /// Класс обеспечивает операции с данными из БД
    /// </summary>
    public class DataService
    {
        private readonly DbContext _dbSource;

        public DataService()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            _dbSource = new DbContext(connectionString);
        }

        /// <summary>
        /// Возвращает новую model в которой содержатся данные по нужному запросу (во входящей model)
        /// </summary>        
        public IndexPageViewModel GetDataFromPage(IndexPageViewModel model)
        {
            model.Pageinfo.TotalItems = _dbSource.GetRowsCount(model.ChoosedCountry, model.ChoosedCity);
            model.DbData = _dbSource.GetNewPage(model.Pageinfo.PageSize, model.Pageinfo.PageNumber, model.ChoosedCountry, model.ChoosedCity).OrderBy(c => c.OrderDate);

            if (model.Pageinfo.TotalItems == 0) model.Pageinfo.PageNumber = 0;

            var citiesList = _dbSource.GetCities().ToList();
            model.Cities = new SelectList(citiesList, "Cities");
            
            var countriesList = _dbSource.GetCountries().ToList();
            model.Countries = new SelectList(countriesList, "Countries");

            return model;
        }
    }
}