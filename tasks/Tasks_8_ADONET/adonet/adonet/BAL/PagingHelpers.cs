﻿using System.Text;
using System.Web.Mvc;
using adonet.BAL.Models;
using Antlr.Runtime.Misc;

namespace adonet.BAL
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PageInfo pageInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();


            int limit;

            if (pageInfo.TotalPages >= 10)
            {
                limit = 10;
            }
            else
            {
                limit = pageInfo.TotalPages;
            }

            var curPage = pageInfo.PageNumber;


            int startPos;

            if (pageInfo.PageNumber >= 4)
            {
                startPos = pageInfo.PageNumber - 3;
            }
            else
            {
                startPos =1;
            }

            int endPos;

            if (pageInfo.PageNumber >= pageInfo.TotalPages)
            {
                endPos = pageInfo.TotalPages;
            }else
            {
                endPos = limit + startPos - 1;
                if (endPos > pageInfo.TotalPages) endPos = pageInfo.TotalPages;
            }



            for (int i = startPos; i <= endPos; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                // если текущая страница, то выделяем ее,
                // например, добавляя класс
                if (i == pageInfo.PageNumber)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }



            return MvcHtmlString.Create(result.ToString());
        }
    }
}