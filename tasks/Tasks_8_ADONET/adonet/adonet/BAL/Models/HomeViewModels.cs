﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using adonet.DAL;
using PagedList;

namespace adonet.BAL.Models
{
    public class PageInfo
    {
        /// <summary>
        /// номер текущей страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// кол-во объектов на странице
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// всего записей
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// всего страниц
        /// </summary>
        public int TotalPages  
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }
    }

    public class HomeViewModels
    {


        public class IndexPageViewModel
        {
            public IEnumerable<EmployeesOrders> DbData { get; set; }



            [Display(Name = "Страна")]
            public SelectList Countries { get; set; }

            [Display(Name = "Город")]
            public SelectList Cities { get; set; }
         
            /// <summary>
            /// текущая страница
            /// </summary>
            public int CurPageNum { get; set; }

            [Display(Name = "Кол-во записей")]
            [Range(1, 50, ErrorMessage = "Введите значение от 1 до 50")]          
            public int PagesOnView { get; set; }

            public string ChoosedCity { get; set; }
            public string ChoosedCountry { get; set; }


            public PageInfo Pageinfo;
        }
    }
}