﻿using System.Web.Mvc;
using adonet.BAL;
using adonet.BAL.Models;
using static adonet.BAL.Models.HomeViewModels;

namespace adonet.Controllers
{
    public class HomeController : Controller
    {
        private static readonly int PagesOnViewDefault = 15;
        private readonly DataService _dserv = new DataService();
        private static readonly PageInfo DefaultPageInfo = new PageInfo() {PageSize = PagesOnViewDefault };
        private static IndexPageViewModel _model = new IndexPageViewModel() {Pageinfo = DefaultPageInfo , PagesOnView = PagesOnViewDefault };


        [HttpGet]
        public ActionResult Index(int? page)
        {
            _model.Pageinfo.PageNumber = page ?? 1;
            _model = _dserv.GetDataFromPage(_model);
            
            return View(_model);
        }

        [HttpPost]
        public ActionResult Index(IndexPageViewModel model)
        {

            if (!ModelState.IsValid) return View(_model);
            
            model.Pageinfo = _model.Pageinfo;
            model.Pageinfo.PageSize = model.PagesOnView;


            if (model.ChoosedCountry != _model.ChoosedCountry || model.ChoosedCity != _model.ChoosedCity)
            {
                model.Pageinfo.PageNumber = 1;
            }
            else
            {
                model.Pageinfo.PageNumber = _model.Pageinfo.PageNumber;
            }

            _model = _dserv.GetDataFromPage(model);

            
            

            return View(_model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}