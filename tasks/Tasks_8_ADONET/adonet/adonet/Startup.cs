﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(adonet.Startup))]
namespace adonet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
