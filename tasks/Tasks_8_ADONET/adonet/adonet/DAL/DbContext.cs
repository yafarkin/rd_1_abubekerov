﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace adonet.DAL
{
    public class DbContext
    {
        private readonly string _connectionString;

        /// <summary>
        /// конструктор принимает connection string к БД
        /// </summary>
        /// <param name="connectionString"></param>
        public DbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Возвращает список EmployeesOrders по заданному запросу
        /// </summary>      
        [HandleError()]
        public List<EmployeesOrders> GetNewPage(int rowsOnPage, int pageNum, string targetCountry = null, string targetCity = null)
        {
            var resultList = new List<EmployeesOrders>();

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    const string sqlExpression = "paging";

                    var command = new SqlCommand(sqlExpression, connection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };


                    var rowsOnPageParam = new SqlParameter
                    {
                        ParameterName = "@n",
                        Value = rowsOnPage
                    };

                    var pageNumParam = new SqlParameter
                    {
                        ParameterName = "@p",
                        Value = pageNum
                    };

                    var cityParam = new SqlParameter
                    {
                        ParameterName = "@city",
                        Value = targetCity
                    };

                    var countryParam = new SqlParameter
                    {
                        ParameterName = "@country",
                        Value = targetCountry
                    };

                    command.Parameters.Add(rowsOnPageParam);
                    command.Parameters.Add(pageNumParam);
                    command.Parameters.Add(cityParam);
                    command.Parameters.Add(countryParam);

                    var result = command.ExecuteReader();

                    if (!result.HasRows) return resultList;

                    while (result.Read())
                    {
                        resultList.Add(new EmployeesOrders()
                        {
                            Employee = result.GetString(0),
                            ProductName = result.GetString(1),
                            OrderDate = result.GetDateTime(2),
                            CustomerId = result.GetString(3),
                            City = result.GetString(4),
                            Country = result.GetString(5)
                        });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

            }
            return resultList;
        }

        /// <summary>
        /// Возвращает количество строк для запроса с заданым фильтром
        /// </summary>  
        [HandleError()]
        public int GetRowsCount(string сountry = null, string сity = null)
        {
            var total = 0;

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    const string sqlExpression = "pagingTotal";

                    var command = new SqlCommand(sqlExpression, connection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    var cityParam = new SqlParameter
                    {
                        ParameterName = "@city",
                        Value = сity
                    };

                    var countryParam = new SqlParameter
                    {
                        ParameterName = "@country",
                        Value = сountry
                    };


                    command.Parameters.Add(cityParam);
                    command.Parameters.Add(countryParam);

                    if (!int.TryParse(command.ExecuteScalar().ToString(), out total)) return 0;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

            }
            return total;
        }

        /// <summary>
        /// Получаем список всех возможных стран из БД
        /// </summary>        
        [HandleError()]
        public List<string> GetCountries()
        {
            List<string> resultList = new List<string>();

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    const string sqlExpression = "getCountries";

                    var command = new SqlCommand(sqlExpression, connection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    var result = command.ExecuteReader();

                    if (!result.HasRows) return null;

                    while (result.Read())
                    {
                        resultList.Add(result.GetString(0));
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();
                }
            }
            return resultList;
        }

        /// <summary>
        /// Получаем список всех возможных городов из БД
        /// </summary> 
        [HandleError()]
        public List<string> GetCities()
        {
            List<string> resultList = new List<string>();

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    const string sqlExpression = "getCities";

                    var command = new SqlCommand(sqlExpression, connection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    var result = command.ExecuteReader();

                    if (!result.HasRows) return null;

                    while (result.Read())
                    {
                        resultList.Add(result.GetString(0));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

            }
            return resultList;
        }
    }


}