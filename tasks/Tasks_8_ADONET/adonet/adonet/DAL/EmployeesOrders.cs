﻿using System;

namespace adonet.DAL
{
    public class EmployeesOrders
    {
        public string Employee { get; set; }
        public string ProductName { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerId { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}