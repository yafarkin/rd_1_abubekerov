USE [Northwind]
GO

/****** Object:  StoredProcedure [dbo].[getCountries]    Script Date: 01/26/2017 18:04:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if object_id('dbo.getCountries', 'p') is not null
DROP PROCEDURE [dbo].[getCountries] 
GO


CREATE PROCEDURE [dbo].[getCountries] 
AS
 select emp.Country
 from Employees emp
 group by emp.Country

GO


