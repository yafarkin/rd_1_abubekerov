USE [Northwind]
GO

/****** Object:  StoredProcedure [dbo].[getCities]    Script Date: 01/26/2017 18:03:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if object_id('dbo.getCities', 'p') is not null
DROP PROCEDURE [dbo].[getCities] 
GO

CREATE PROCEDURE [dbo].[getCities] 
AS
 select emp.City
 from Employees emp
 group by emp.City

GO


