USE [Northwind]
GO

/****** Object:  StoredProcedure [dbo].[paging]    Script Date: 01/26/2017 17:52:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


if object_id('dbo.paging', 'p') is not null
DROP PROCEDURE [dbo].[paging] 
GO

CREATE PROCEDURE [dbo].[paging] 
@n int -- число записей на страницу
, @p int =1 -- номер страницы, по умолчанию - первая
, @country nvarchar(15) = null
, @city nvarchar(15) = null
AS
SELECT * FROM
 (SELECT *, 
   CASE WHEN num % @n = 0 THEN num/@n ELSE num/@n + 1 END AS page_num, 
   CASE WHEN total % @n = 0 THEN total/@n ELSE total/@n + 1 END AS num_of_pages
  FROM 
  (SELECT *, 
         ROW_NUMBER() OVER(ORDER BY Employee DESC) AS num, 
         COUNT(*) OVER() AS total FROM 
		 (
		 select (emp.FirstName + ' ' + emp.LastName) as Employee, prod.ProductName, ord.OrderDate, ord.CustomerID, emp.City, emp.Country
 from Employees emp
INNER JOIN Orders ord ON ord.EmployeeID=emp.EmployeeID
INNER JOIN [Order Details] ordDet ON ord.OrderID=ordDet.OrderID
INNER JOIN Products prod ON prod.ProductID = ordDet.ProductID
where ( emp.Country = @country OR @country IS NULL  ) AND (emp.City = @city OR @city IS NULL )	
		 )Z
  ) X
 ) Y
WHERE page_num = @p;

GO


