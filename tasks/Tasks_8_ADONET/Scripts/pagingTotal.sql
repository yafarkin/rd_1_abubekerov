USE [Northwind]
GO

/****** Object:  StoredProcedure [dbo].[pagingTotal]    Script Date: 01/26/2017 17:52:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

if object_id('dbo.pagingTotal', 'p') is not null
DROP PROCEDURE [dbo].[pagingTotal] 
GO


CREATE PROCEDURE [dbo].[pagingTotal] 
@country nvarchar(15) = null -- фильтр по стране
, @city nvarchar(15) = null -- фильтр по городу
AS
SELECT COUNT(*) 
FROM
( 
	select (emp.FirstName + ' ' + emp.LastName) as Employee, prod.ProductName, ord.OrderDate, ord.CustomerID, emp.City, emp.Country
	from Employees emp
	INNER JOIN Orders ord ON ord.EmployeeID=emp.EmployeeID
	INNER JOIN [Order Details] ordDet ON ord.OrderID=ordDet.OrderID
	INNER JOIN Products prod ON prod.ProductID = ordDet.ProductID
	where ( emp.Country = @country OR @country IS NULL  ) AND (emp.City = @city OR @city IS NULL )	
)Z 

GO


