﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyGenericCollection;

namespace MyGenericCollectionTest
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void DelegateAddTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>();
            var conditionObj = "test_obj";
            var expectedObj = conditionObj;
            var resultObj = "";
            var expectedAction = MyGenericCollection<string>.Actions.Added;            
            var resultAction = new MyGenericCollection<string>.Actions { }; 
            
            //act            
            TestCollection.ClassAction += (string obj, MyGenericCollection<string>.Actions action) =>
            {
                resultObj = obj;
                resultAction = action;
            };

            TestCollection.Add(conditionObj);

            // assert
            Assert.AreEqual(expectedObj, resultObj);
            Assert.AreEqual(expectedAction, resultAction);
        }

        [TestMethod]
        public void DelegateDeleteTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>();
            var conditionObj = "test_obj";
            var expectedObj = conditionObj;
            var resultObj = "";
            var expectedAction = MyGenericCollection<string>.Actions.Deleted;
            var resultAction = new MyGenericCollection<string>.Actions { };

            //act            
            

            TestCollection.Add(conditionObj);

            TestCollection.ClassAction += (string obj, MyGenericCollection<string>.Actions action) =>
            {
                resultObj = obj;
                resultAction = action;
            };

            TestCollection.Remove(conditionObj);

            // assert
            Assert.AreEqual(expectedObj, resultObj);
            Assert.AreEqual(expectedAction, resultAction);
        }

        [TestMethod]
        public void DelegateUpdateTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>();
            var conditionObj = "test_obj";
            var expectedObj = conditionObj;
            var resultObj = "";
            var expectedAction = MyGenericCollection<string>.Actions.Updated;
            var resultAction = new MyGenericCollection<string>.Actions { };

            //act            


            TestCollection.Add("");

            TestCollection.ClassAction += (string obj, MyGenericCollection<string>.Actions action) =>
            {
                resultObj = obj;
                resultAction = action;
            };

            TestCollection[0] = conditionObj;

            // assert
            Assert.AreEqual(expectedObj, resultObj);
            Assert.AreEqual(expectedAction, resultAction);
        }

        [TestMethod]
        public void DelegateGetTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>();
            var conditionObj = "test_obj";
            var expectedObj = conditionObj;
            var resultObj = "";
            var expectedAction = MyGenericCollection<string>.Actions.Get;
            var resultAction = new MyGenericCollection<string>.Actions { };

            //act            


            TestCollection.Add(conditionObj);

            TestCollection.ClassAction += (string obj, MyGenericCollection<string>.Actions action) =>
            {
                resultObj = obj;
                resultAction = action;
            };

            var testvar = TestCollection[0];

            // assert
            Assert.AreEqual(expectedObj, resultObj);
            Assert.AreEqual(expectedAction, resultAction);
        }

        [TestMethod]
        //public int Count()
        public void CountTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>();
            var expectedCount = 2;


            //act

            TestCollection.Add("first");
            TestCollection.Add("second");

            // assert
            Assert.AreEqual(expectedCount,TestCollection.Count);
        }

        [TestMethod]
        //public void Add()
        public void AddTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<int>();
            var expectedValue1 = 2;
            var expectedValue2 = 6;


            //act
            TestCollection.Add(2);
            TestCollection.Add(6);


            // assert
            Assert.AreEqual(expectedValue1, TestCollection[0]);
            Assert.AreEqual(expectedValue2, TestCollection[1]);
        }

        [TestMethod]
        //public void Clear()
        public void ClearTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<int>();

            //act
            TestCollection.Add(2);
            TestCollection.Clear();
            // assert
            Assert.AreEqual(0, TestCollection.Count);
            
        }

        [TestMethod]
        //public bool Contains()
        public void ContainsTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>();

            //act
            TestCollection.Add("Bob");
            TestCollection.Add("Brown");
            TestCollection.Add("Sweet");
            TestCollection.Add("Cat");
                      

            // assert
            Assert.AreEqual(true, TestCollection.Contains("Cat"));
        }


        [TestMethod]
        //   public void CopyTo(MyGenericCollection<T> array, int arrayIndex)
        [ExpectedException(typeof(System.ArgumentOutOfRangeException))]
        public void CopyToTExceptionTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>() { "Bob","Sam","Alex","Serega"};
            var TestCollection2 = new MyGenericCollection<string>();

            //act            
            TestCollection.CopyTo(TestCollection2, 2);

            // assert
            Assert.AreEqual(TestCollection[0], TestCollection2[2]);
            
        }

        [TestMethod]
        //   public void CopyTo(MyGenericCollection<T> array, int arrayIndex)       
        public void CopyToTTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>() { "Bob", "Sam" };
            var TestCollection2 = new MyGenericCollection<string>() { "Bill", "Sue", "Gleb", "Nickey","Stieve","Mike","Tom" };

            //act            
            TestCollection.CopyTo(TestCollection2, 2);

            // assert
            Assert.AreEqual(TestCollection[0], TestCollection2[2]);
            Assert.AreEqual(TestCollection[1], TestCollection2[3]);
        }

        [TestMethod]
        //public void CopyTo(T[] array, int arrayIndex)
        public void CopyToTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>() { "Bob", "Sam", "Alex", "Serega" };
            string[] arraycopy = new string[] {"","","","","","","","" };

            //act            
            TestCollection.CopyTo(arraycopy, 2);

            // assert
            Assert.AreEqual(TestCollection[0], arraycopy[2]);
            Assert.AreEqual(TestCollection[1], arraycopy[3]);
        }


        [TestMethod]
        //public bool Remove()
        public void RemoveTest()
        {
            // arrange
            var TestCollection = new MyGenericCollection<string>() { "Bob", "Sam", "Alex", "Serega"};
            bool isExist = false;

            //act            
            TestCollection.Remove("Sam");
            
            foreach(string i in TestCollection)
            {
                if (i == "Sam")
                {
                    isExist = true;
                    break;
                }
            }


            // assert
            Assert.AreEqual(false, isExist);            
        }
    }
}
