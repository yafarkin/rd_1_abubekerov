﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


/*Разработать класс - коллекцию, generic
Реализующую интерфейсы ICollection<T> и IEnumerable<T>
Внутри класс должен использовать для хранения только стандартные массивы.
Предусмотреть делегат для вызова при изменениях в коллекции. Делегат должен передавать изменяемый объект и тип события (определен через enum) - Added, Deleted, Updated, Get.
Написать юнит тесты для класса.
Помним про XML комментарии.*/

namespace MyGenericCollection
{
    public class MyGenericCollection<T> : ICollection<T>, IEnumerable<T>
    {
        public enum Actions { Added, Deleted, Updated, Get };
        public delegate void Changings(T obj, Actions state);              
        public event Changings ClassAction = delegate { };
        private T[] myCollection;
        private int count = 0;
        private int capacity = 0;

        readonly int blockSizeToResizeCollection = 16;
        /// <summary>
        /// Constructor specifies capacity of collection
        /// </summary>
        /// <param name="Capacity">capacity</param>
        public MyGenericCollection(int Capacity)
        {
            capacity = Capacity;
            T[] myCollection = new T[capacity];
        }


        /// <summary>
        /// Property: capacity of collection
        /// </summary>        
        public int Capacity
        {
            get
            {
                return capacity;
            }

            private set
            {
                capacity = value;
            }
        }

        /// <summary>
        /// Empty constructor creates collection
        /// </summary>
        public MyGenericCollection()
        {
            myCollection = new T[] { };
        }

        /// <summary>
        /// Property: elements count of collection
        /// </summary>        
        public int Count
        {
            get
            {
                return count;
            }

            private set
            {
                count = value;
            }
        }

        /// <summary>
        /// Property: Is collection read only ?
        /// </summary>  
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Add item to collection
        /// </summary>
        /// <param name="item">new item</param>
        public void Add(T item)
        {
            if (Count + 1 > myCollection.Length)
            {
                Array.Resize(ref myCollection, myCollection.Length + blockSizeToResizeCollection);
                Capacity = myCollection.Length;
            }

            myCollection[++Count - 1] = item;

            ClassAction(item, Actions.Added);
        }

        /// <summary>
        /// Clears collection
        /// </summary>
        public void Clear()
        {
            Count = 0;
            Capacity = 0;
            Array.Resize(ref myCollection, 0);
        }

        /// <summary>
        /// existing of item in collection
        /// </summary>
        /// <param name="item"></param>
        /// <returns> true or false</returns>
        public bool Contains(T item)
        {
            return myCollection.Contains(item);
        }

        /// <summary>
        /// Copying collection elements to specified T array
        /// </summary>
        /// <param name="array"> destination array</param>
        /// <param name="arrayIndex">start position in destination array</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array.Count() < arrayIndex + Count)
            {
                throw new ArgumentOutOfRangeException("array is smaller than need");
            }

            for (int i = 0; i < Count; i++)
            {
                array[i + arrayIndex] = myCollection[i];
            }
        }

        /// <summary>
        /// Copying collection to another collection
        /// </summary>
        /// <param name="array"> destination MyGenericCollection<T> collection </param>
        /// <param name="arrayIndex">start position in destination  collection</param>
        public void CopyTo(MyGenericCollection<T> array, int arrayIndex)
        {
            if (array.Count < arrayIndex + Count)
            {
                throw new ArgumentOutOfRangeException("array is smaller than need");
            }

            for (int i = 0; i < Count; i++)
            {
                array[i + arrayIndex] = myCollection[i];
            }
        }

        /// <summary>
        /// Indexator for collection
        /// </summary>
        /// <param name="index">index</param>
        /// <returns></returns>
        public T this[int index]
        {
            set
            {
                if (index < Count)
                {
                    myCollection[index] = value;

                    ClassAction(myCollection[index], Actions.Updated);

                }
                else
                {
                    throw new ArgumentOutOfRangeException("index is out of range");
                }
            }

            get
            {
                ClassAction(myCollection[index], Actions.Get);

                return myCollection[index];
            }
        }

        /// <summary>
        /// Remove item from collection
        /// </summary>
        /// <param name="item">item to find and remove</param>
        /// <returns>true if item was found and remover, false is item not found in collection</returns>
        public bool Remove(T item)
        {
            bool flag = false;

            for (int i = 0; i < Count; i++)
            {
                if (myCollection[i].Equals(item))
                {
                    flag = true;
                }

                if (flag)
                {
                    if (i != Count - 1)
                    {
                        myCollection[i] = myCollection[i + 1];
                    }
                    else
                    {
                        myCollection[Count - 1] = default(T);
                        Count--;                        
                    }

                    ClassAction(item, Actions.Deleted);
                }
            }

            return flag;

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return myCollection.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return myCollection[i];
            }
        }
    }
}
