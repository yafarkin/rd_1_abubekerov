﻿using System;


namespace MyGenericCollection
{
    class Program
    {
        private static void Show_Message(string item, MyGenericCollection<string>.Actions act)
        {
            Console.WriteLine("Object -{0} : {1}", item, act);
        }

        static void Main(string[] args)
        {

            var SuperCollection = new MyGenericCollection<string> { };
            SuperCollection.ClassAction += Show_Message;

            Console.WriteLine("Add elements");
            SuperCollection.Add("Java");            
            SuperCollection.Add("Python");            
            SuperCollection.Add("Ruby");
            

            Console.WriteLine("{0}Show elements:",Environment.NewLine);
            foreach (string i in SuperCollection)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("{0}Clear collection{0}", Environment.NewLine);
            SuperCollection.Clear();
            


            SuperCollection.Add("Earth");            
            SuperCollection.Add("Mars");
            
            SuperCollection[SuperCollection.Count - 1] = "Venus";

            Console.WriteLine("{0}Show elements:", Environment.NewLine);
            foreach (string i in SuperCollection)
            {
                Console.WriteLine(i);
            }


            
            SuperCollection.Remove("Venus");
            

            string a = SuperCollection[0];
            Console.WriteLine("{0}get element -{1}", Environment.NewLine,a);


            Console.ReadKey();

        }
    }
}


