﻿using System;
using TextParser;
using System.Threading;


namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string nextStr = null;
            string resStr, errMsg;

            //reading from *.txt file that specified
            do
            {
                nextStr = Console.ReadLine();

                if (nextStr == null) break;

                CoordinatesParser.LineHandleProcess(nextStr, out errMsg, out resStr);

                if (errMsg == "")
                {
                    Console.WriteLine(resStr);
                }
                else
                {
                    Console.WriteLine(errMsg);
                }

            } while (nextStr != null);

            Thread.Sleep(10000);
        }
    }
}
