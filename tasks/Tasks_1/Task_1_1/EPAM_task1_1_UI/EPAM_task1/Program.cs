﻿using System;
using System.IO;
using System.Collections.Specialized;
using TextParser;

namespace EPAM_task1
{
    class Program
    {
        private static string src_file_path = "";
        private static StringCollection user_lines = new StringCollection();

        // Show main menu and get users Key
        static ConsoleKeyInfo ShowMainMenu()
        {
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("______MAIN MENU______");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(Environment.NewLine+"Choose the way you'd like to enter data:" + Environment.NewLine);
            Console.WriteLine("1. manual input");
            Console.WriteLine("2. get data from file (" + src_file_path + ")");
            Console.WriteLine("3. exit");
            return Console.ReadKey();
        }

        // Show User input menu and get users Key
        static ConsoleKeyInfo ShowUserInputMenu()
        {
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("______MANUAL INPUT MENU______");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(Environment.NewLine+"1. Add another string");
            Console.WriteLine("2. Get Result");
            Console.WriteLine("3. return to Main menu");
            return Console.ReadKey();
        }
        
        //read data from file and handle it
        static void Read_from_file_process()
        {
            int counter = 0;
            string src_line;
            string res_line;

            string err_mes;

            Console.Clear();
            Console.WriteLine(Environment.NewLine, Environment.NewLine);
            // Read the file and display it line by line.
            try
            {
                System.IO.StreamReader src_file = new System.IO.StreamReader(src_file_path);    // source data file
                System.IO.StreamWriter res_file = new System.IO.StreamWriter(@"test_res.txt");  // result data file

                Console.WriteLine("Data from input file:");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("                             " + src_file_path + Environment.NewLine);

                Console.ForegroundColor = ConsoleColor.Green;
                // read line by line from src file , handle line and write it to result file and to console
                while ((src_line = src_file.ReadLine()) != null)
                {

                    CoordinatesParser.LineHandleProcess(src_line,out err_mes,out res_line);

                    if (err_mes == "")
                    {
                        res_file.WriteLine(res_line);
                        Console.WriteLine("SRC: " + src_line + "   |    RESULT: " + res_line);
                    }
                    else
                    {
                        res_file.WriteLine(err_mes);
                        Console.WriteLine("SRC: " + src_line + "   |    RESULT: " + err_mes);
                    }
                    
                    counter++; // counter of lines

                }

                src_file.Close();
                res_file.Close();

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(Environment.NewLine + "{0} lines wrote to output file test_res.txt", counter);
                Console.WriteLine(Environment.NewLine);
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(Environment.NewLine + "Press any key to continue ...");
                Console.ReadKey();
            }
            catch (FileNotFoundException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("File not found", Environment.NewLine);
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(Environment.NewLine + "nPress any key to continue ...");
                Console.ReadKey();
            }
            catch (System.ArgumentException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Menu is not available, no path for data file"+ Environment.NewLine);
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(Environment.NewLine + "Press any key to continue ...");
                Console.ReadKey();
            }
        }

        //handle user lines and show result to console
        static void HandleLines(StringCollection lines)
        {
            string res_line;
            int cnt = 0;
            string err_mes;

            Console.Clear();

            try
            {
                System.IO.StreamWriter res_file = new System.IO.StreamWriter(@"test_res.txt");// result data file

                Console.ForegroundColor = ConsoleColor.Green;

                Console.WriteLine("");
                // go throgh collection and handle strings
                for (int i = 0; i < lines.Count; i++)
                {
                    CoordinatesParser.LineHandleProcess(lines[i],out err_mes,out res_line);
                    
                    if (err_mes == "")
                    {
                        res_file.WriteLine(res_line);
                        Console.WriteLine("SRC: " + lines[i] + "   |    RESULT: " + res_line);
                    }
                    else
                    {
                        res_file.WriteLine(err_mes);
                        Console.WriteLine("SRC: " + lines[i] + "   |    RESULT: " + err_mes);
                    }
                        
                    cnt++;
                }
                res_file.Close();

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(" {0} lines wrote to output file test_res.txt" + Environment.NewLine, cnt);
            }
            catch (FileNotFoundException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("File not found");
            }
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        
        static string GetNewLineFromUser()
        {
            Console.Clear();
            Console.WriteLine(Environment.NewLine+"Enter string like '12.34 , 56.78 " + Environment.NewLine);
            return Console.ReadLine();
        }

        // user enters lines and programm handle it
        static void Manual_input_process()
        {
            ConsoleKeyInfo UserKey;  // variable gets the key user pressed

            user_lines.Add(GetNewLineFromUser());

            do
            {
                UserKey = ShowUserInputMenu();
                Console.WriteLine("");

                if (UserKey.KeyChar == '1')
                {
                    user_lines.Add(GetNewLineFromUser());
                }

                if (UserKey.KeyChar == '2')
                {
                    if (user_lines.Count > 0)
                    {
                        HandleLines(user_lines);
                        Console.WriteLine("Press any key to continue ...");
                        Console.ReadKey();
                    }
                };

            }
            while (UserKey.KeyChar != '3');
        }

        static void Main(string[] args)
        {
            ConsoleKeyInfo Keypressed;  // variable gets the key user pressed

            //Check arguments - file path
            if (args.Length == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("No data text file, loading '2. get data from file' menu is not available " + Environment.NewLine);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(Environment.NewLine+"Please enter the path to input file as argument for the programm , next time " + Environment.NewLine);

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Press any key to continue ... " + Environment.NewLine);
                Console.ReadKey();
            }
            else
            {
                src_file_path = args[0]; // set path or filename of src file
            }




            //show menu while user not press "3" key to exit
            do
            {
                Keypressed = ShowMainMenu();
                Console.WriteLine("");

                if (Keypressed.KeyChar == '1')
                {
                    Manual_input_process();
                }

                if (Keypressed.KeyChar == '2')
                {
                    Read_from_file_process();
                };

            }
            while (Keypressed.KeyChar != '3');


        }
    }
}
