﻿using System;

/// <summary>
/// Checking text if it corresponds to conditions
/// </summary>
namespace TextParser
{
    /// <summary>
    /// Class for parsing coordinates
    /// </summary>
   ///
    public class CoordinatesParser 
    {
        /// <summary>
        /// check if string corresponds to format "*dd.dd*"
        /// </summary>
        /// <param name="inStr">string to check</param>
        /// <returns>TRUE - if coordinate format corresponds to format;  FALSE - otherwise"</returns>
        ///         
        public static bool CheckOneCoordinate(string inStr)
        {
            if (inStr == "") return false;
            string tmpStr = inStr;
            int sepIdx = tmpStr.IndexOf('.');
            tmpStr = inStr.Substring(sepIdx + 1);
            sepIdx = tmpStr.IndexOf('.');
            return (sepIdx == -1);
        }

        /// <summary>
        /// function check line and convert it to format - "X: 12,34 . Y: 56,78"   or returns message of Error 
        /// </summary>
        /// <param name="srcText"> string to check</param>
        /// <param name="errMsg"> output error message , if errMsg is empty then check is successful</param>
        /// <param name="resStr"> output resulted string</param>
        ///               
        public static void LineHandleProcess(String srcText, out string errMsg, out string resStr)
        {
            int separatorIdx = 0;
            int lastSeparatorIdx = 0;
            int cnt = 0;  // counter of separators

            string xPart, yPart;      // parts of result string
            string tmpStr = srcText;  // set tmp_str by input string

            // if input line is empty return error
            if (srcText == "")
            {
                errMsg = "empty string";
                resStr = "";
                return;
            }

            // if input line is empty return error
            if (srcText==null)
            {
                errMsg = " NULL string";
                resStr = "";
                return;
            }


            // search separator through the whole string while separator exists 
            do
            {
                separatorIdx = tmpStr.IndexOf(',');   // trying to find separator in string

                if (separatorIdx != -1) // if separator was found somewhere in string ...
                {
                    lastSeparatorIdx = separatorIdx;            // remember found position of separeator
                    tmpStr = tmpStr.Substring(separatorIdx + 1);  // remove already checked separator                    
                    cnt++;                                             // increase counter of separators
                }
            }
            while (separatorIdx != -1);


            //check if there were similar separators->  return error
            if (cnt > 1)
            {
                errMsg = "too many separators";
                resStr = "";
                return;
            }



            //separator found and there is only one separator in string
            if (cnt == 1 && lastSeparatorIdx != -1)
            {

                // get x and y coordinates from string, delete extra spaces
                tmpStr = srcText;
                xPart = srcText.Substring(0, lastSeparatorIdx);
                xPart = xPart.Trim();

                tmpStr = srcText;
                yPart = srcText.Substring(lastSeparatorIdx + 1, srcText.Length - lastSeparatorIdx - 1);
                yPart = yPart.Trim();

                //delete spaces, replace separator ',' to '.' and add prefixes X:   and Y:
                xPart = xPart.Replace(" ", string.Empty);
                yPart = yPart.Replace(" ", string.Empty);

                //check if only one separator existed in line
                if (CheckOneCoordinate(xPart) && CheckOneCoordinate(yPart))
                {
                    xPart = "X: " + xPart.Replace(".", ",");
                    yPart = "Y: " + yPart.Replace(".", ",");

                    //set result string to target format
                    resStr = xPart + " . " + yPart;
                    errMsg = "";
                    return;

                }
                else
                {
                    // one of coordinate has wrong format
                    resStr = "";
                    errMsg = "wrong coordinate";
                    return;
                }
            }
            else
            {
                resStr = "";
                errMsg = "separator not found";
                return;
            }
        }
    }
}
