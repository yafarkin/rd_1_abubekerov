﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TextParser;

namespace WpfApplication1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        /// <summary>
        /// MainWindow constructor        
        /// </summary>         
        ///
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Loads txt file with coordinates to specified listbox         
        /// </summary>
        /// <param name="lstbx">target ListBox</param>        
        /// 
        private void LoadFileDataToListbox(ListBox lstbx)
        {
            string srcLine;
            string srcFilePath;

            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt)|*.txt";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == false) return;

            // Open document
            srcFilePath = dlg.FileName;
            FileNameTextBox.Text = dlg.SafeFileName;

            try
            {
                System.IO.StreamReader srcFile = new System.IO.StreamReader(srcFilePath);    // source data file

                lstbx.Items.Clear();

                while ((srcLine = srcFile.ReadLine()) != null)
                {
                    lstbx.Items.Add(srcLine);
                }

                srcFile.Close();

            }
            catch (System.ArgumentException)
            {

            }
        }

        /// <summary>
        /// Saves data from listbox to specified file
        /// </summary>
        /// <param name="lstbx">source ListBox</param>
        /// 
        private void SaveFileDataFromListbox(ListBox lstbx)
        {
            string resFilePath;

            // Create SaveFileDialog
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt)|*.txt";

            // Display SaveFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == false) return;


            // Save document
            resFilePath = dlg.FileName;

            string save_file_name = dlg.SafeFileName;

            try
            {
                System.IO.StreamWriter res_file = new System.IO.StreamWriter(resFilePath);


                for (int i = 0; i < lstbx.Items.Count; i++)
                {
                    res_file.WriteLine(lstbx.Items[i].ToString());
                }

                res_file.Close();

            }
            catch (System.ArgumentException)
            {

            }
        }

        /// <summary>
        /// Button to open file with data
        /// </summary> 
        /// <param name="sender">-</param>
        /// <param name="e">-</param>
        /// 
        private void btn_openfile_Click(object sender, RoutedEventArgs e)
        {
            LoadFileDataToListbox(lbx_src);
        }

        /// <summary>
        /// Button to run parsing file and add result to ListBox
        /// </summary>
        /// <param name="sender">-</param>
        /// <param name="e">-</param>
        /// 
        private void btn_run_Click(object sender, RoutedEventArgs e)
        {

            string resStr = "";
            string errMsg = "";           

            if (lbx_src.Items.Count > 0)
            {
                lbx_res.Items.Clear();
                for (int cnt = 0; cnt < lbx_src.Items.Count; cnt++)
                {
                    CoordinatesParser.LineHandleProcess(lbx_src.Items[cnt].ToString(), out errMsg, out resStr);

                    if (errMsg == "")
                    {
                        lbx_res.Items.Add(resStr);
                    }
                    else
                    {
                        lbx_res.Items.Add("Error: " + errMsg);
                    }
                }
            }
        }

        /// <summary>
        ///  forbid symbols
        /// </summary> 
        /// <param name="sender">--</param>
        /// <param name="e">--</param>
        /// 
        private void txtbx_input_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //forbid to enter characters
            e.Handled = "0123456789 , .".IndexOf(e.Text) < 0;
        }

        /// <summary>
        /// Button to Clear listbox
        /// </summary> 
        /// <param name="sender">--</param>
        /// <param name="e">--</param>
        /// 
        private void btn_clear_Click(object sender, RoutedEventArgs e)
        {
            lbx_manualinp.Items.Clear();
        }

        /// <summary>
        /// Button to delete items from ListBox
        /// </summary>
        /// <param name="sender">--</param>
        /// <param name="e">--</param>
        /// 
        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            lbx_manualinp.Items.Remove(lbx_manualinp.SelectedItem);
        }

        /// <summary>
        /// Button to convert new text to coordinates and add it to ListBox
        /// </summary> 
        /// <param name="sender">--</param>
        /// <param name="e">--</param>
        /// 
        private void btn_add_new_Click(object sender, RoutedEventArgs e)
        {
            string resStr = "";
            string errMsg = "";

            // parse text
            CoordinatesParser.LineHandleProcess(txtbx_input.Text, out errMsg, out resStr);

            if (errMsg == "")
            {
                lbx_manualinp.Items.Add(resStr);
            }
            else
            {
                MessageBox.Show("Error: " + errMsg);
            }

        }

        /// <summary>
        ///  ___
        /// </summary> 
        /// <param name="sender">___</param>
        /// <param name="e">___</param>
        /// 
        private void txtbx_input_KeyDown(object sender, KeyEventArgs e)
        {
        }

        /// <summary>
        /// Button to save ListBox to file
        /// </summary> 
        /// <param name="sender">--</param>
        /// <param name="e">--</param>
        /// 
        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDataFromListbox(lbx_manualinp);
        }

        /// <summary>
        /// Button to save ListBox to file
        /// </summary> 
        /// <param name="sender">--</param>
        /// <param name="e">--</param>
        /// 
        private void btn_save1_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDataFromListbox(lbx_res);
        }
    }
}
