﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SampleSupport;
using Task;
using Task.Data;
using System;
using System.Text;

// Version Mad01

namespace SampleQueries
{
    [Title("LINQ Module")]
    [Prefix("Linq")]
    public class LinqSamples : SampleHarness
    {
        private readonly DataSource dataSource = new DataSource();

        private bool ValidateZipCode(string val)
        {
            long number;
            return long.TryParse(val, out number);
        }

        private bool ValidatePhone(string phone)
        {
            var pattern = @"^(\([0-9]\))+?";
            return Regex.IsMatch(phone, pattern);
        }

        private List<GroupPriceEntity> SortProductsByPrice()
        {
            var sortedProducts = new List<GroupPriceEntity>();

            foreach (var prod in dataSource.Products)
                if (prod.UnitPrice <= 20M)
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 0 });
                else if ((prod.UnitPrice > 20M) && (prod.UnitPrice <= 50M))
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 1 });
                else
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 2 });

            return sortedProducts;
        }


        [Category("Restriction Operators")]
        [Title("Task 1")]
        [Description("Выдайте список всех клиентов, чей суммарный оборот (сумма всех заказов) превосходит некоторую величину X. Продемонстрируйте выполнение запроса с различными X (подумайте, можно ли обойтись без копирования запроса несколько раз)")]
        public void Linq1()
        {
            int Cond = 0;

            var customers = dataSource
                .Customers
                .Select(x => new { Name = x.CustomerID, Total = x.Orders.Sum(y => y.Total) })
                .Where(x => x.Total > Cond);


            for (Cond = 20000; Cond < 100000; Cond = Cond + 20000)
            {
                ObjectDumper.Write(string.Format("{0} Customers which Orders Total sum > {1}:[{2} elements]", Environment.NewLine, Cond, customers.Count()));
                ObjectDumper.Write(customers);

            }
        }


        [Category("Restriction Operators")]
        [Title("Task 2")]
        [Description("Для каждого клиента составьте список поставщиков, находящихся в той же стране и том же городе. Сделайте задания с использованием группировки и без.")
        ]
        public void Linq2()
        {

            ObjectDumper.Write(string.Format("{0}____________Not using group______________", Environment.NewLine));
            var noGroupQuery = dataSource
             .Customers
             .Select(x =>
                 dataSource.Suppliers
                .Where(v => v.Country == x.Country)
                .Where(v => v.City == x.City)
                .Select(p => new
                {
                    SupCity = p.City,
                    SupName = p.SupplierName,
                    Country = x.Country,
                    CustomerName = x.CustomerID
                }));


            foreach (var str in noGroupQuery)
            {
                foreach (var s in str)
                {
                    ObjectDumper.Write(string.Format("County:{0} City:{1} Customer:{2} Supplier:{3}", s.Country, s.SupCity, s.CustomerName, s.SupName));
                }
            }


            ObjectDumper.Write(string.Format("{0}________________Using group______________", Environment.NewLine));

            var groupQuery = dataSource
                .Customers
                .Join(dataSource.Suppliers, p => p.Country, v => v.Country, (p, v) => new { Country = p.Country, CustCity = p.City, SupCity = v.City, CustomerID = p.CustomerID, SupplierName = v.SupplierName })
                .Where(x => x.CustCity == x.SupCity)
                .Select(x => new { Country = x.Country, City = x.CustCity, Customer = x.CustomerID, Supplier = x.SupplierName });

            foreach (var str in groupQuery)
            {
                ObjectDumper.Write(string.Format("County:{0} City:{1} Customer:{2} Supplier:{3}", str.Country, str.City, str.Customer, str.Supplier));
            }

        }


        [Category("Restriction Operators")]
        [Title("Task 3")]
        [Description("Найдите всех клиентов, у которых были заказы, превосходящие по сумме величину X")
        ]
        public void Linq3()
        {
            int Cond = 5000;
            var customers = dataSource
                .Customers
                .Where(c => c.Orders.Sum(x => x.Total) > Cond)
              .Select(c => c.CustomerID);

            ObjectDumper.Write(string.Format("Customers which have orders > {0}", Cond));
            foreach (string str in customers)
            {
                ObjectDumper.Write(str);
            }
        }


        [Category("Restriction Operators")]
        [Title("Task 4")]
        [Description("Выдайте список клиентов с указанием, начиная с какого месяца какого года они стали клиентами (принять за таковые месяц и год самого первого заказа)")
        ]
        public void Linq4()
        {
            var customers = dataSource
                .Customers
                .Where(c => c.Orders.Any())
                .Select(c => new
                {
                    Name = c.CustomerID,
                    Date = c.Orders.Min(x => x.OrderDate)
                });

            ObjectDumper.Write("Date of customer register:");
            foreach (var str in customers)
            {
                ObjectDumper.Write(string.Format("Name:{0,5}\tRegistered:{1:MM yyyy}", str.Name, str.Date));
            }
        }


        [Category("Restriction Operators")]
        [Title("Task 5")]
        [Description("Сделайте предыдущее задание, но выдайте список отсортированным по году, месяцу, оборотам клиента (от максимального к минимальному) и имени клиента")
        ]
        public void Linq5()
        {
            var customers = dataSource
                 .Customers
                 .Where(c => c.Orders.Any())
                 .Select(c => new
                 {
                     Name = c.CustomerID,
                     Date = c.Orders.Min(x => x.OrderDate),
                     Total = c.Orders.Sum(h => h.Total)
                 })
                 .OrderBy(u => u.Date.Year)
                 .ThenBy(u => u.Date.Month)
                 .ThenByDescending(u => u.Total)
                 .ThenBy(u => u.Name);


            foreach (var str in customers)
            {
                ObjectDumper.Write(string.Format("Date:{0,8: yyyy MM}\tTotal:{1,11:N}\tName:{2,5} ", str.Date, str.Total, str.Name));
            }
        }


        [Category("Restriction Operators")]
        [Title("Task 6")]
        [Description("Укажите всех клиентов, у которых указан нецифровой код или не заполнен регион или в телефоне не указан код оператора (для простоты считаем, что это равнозначно «нет круглых скобочек в начале»).")
        ]
        public void Linq6()
        {
            int t;
            var customers = dataSource
                .Customers
                .Where(c => !int.TryParse(c.PostalCode, out t) || c.Region == null || !c.Phone.Contains("()"))
                .Select(c => new { CustomerID = c.CustomerID, PostalCode = c.PostalCode, Region = c.Region, Phone = c.Phone });

            ObjectDumper.Write(string.Format("{0,10} {1,10} {2,20} {3,10}", "Customer", "PostalCode", "Region", "Phone"));
            foreach (var str in customers)
            {
                ObjectDumper.Write(string.Format("{0,10} {1,10} {2,20} {3,10} ", str.CustomerID, str.PostalCode, str.Region, str.Phone));
            }
        }



        [Category("Restriction Operators")]
        [Title("Task 7")]
        [Description("Сгруппируйте все продукты по категориям, внутри – по наличию на складе, внутри последней группы отсортируйте по стоимости")
        ]
        public void Linq7()
        {
            var prodbycat = dataSource
                .Products
                .GroupBy(c => c.Category)                
                .Select(c => new
                {
                    Category = c.Key,
                    Products = c.GroupBy(x => x.UnitsInStock).Select(y=> new { StockCount=y.Key , Products = y.OrderBy(v => v.UnitPrice) })
                });
            
            ObjectDumper.Write(string.Format("{0}", Environment.NewLine));


            foreach (var category in prodbycat)
            {
                ObjectDumper.Write(string.Format("{0}-------CATEGORY:{1}-------", Environment.NewLine, category.Category));
                foreach (var stock in category.Products)
                {

                    ObjectDumper.Write(string.Format("{0}\t:::COUNT IN STOCK[{1}]::", Environment.NewLine,stock.StockCount));

                    foreach (var product in stock.Products)
                    {
                        ObjectDumper.Write(string.Format("Product: {0,40}\t\tPrice: {1}", product.ProductName, product.UnitPrice));
                    }
                }
            }
        }





        [Category("Restriction Operators")]
        [Title("Task 8")]
        [Description("Сгруппируйте товары по группам «дешевые», «средняя цена», «дорогие». Границы каждой группы задайте сами")
        ]
        public void Linq8()
        {
            var typeOfProd = dataSource
                .Products.Select(c => new
                {
                    Category = c.Category,
                    ProductID = c.ProductID,
                    ProductName = c.ProductName,
                    Price = c.UnitPrice,
                    UnitsInStock = c.UnitsInStock,
                    PriceType = GetTypeofPrice(c.UnitPrice)
                }
                )
                .OrderBy(c => c.Price)
                .GroupBy(c => c.PriceType);



            foreach (var typeOfPrice in typeOfProd)
            {
                ObjectDumper.Write(string.Format("{0}:::::{1}:::::", Environment.NewLine, typeOfPrice.Key));

                foreach (var b in typeOfPrice)
                {
                    ObjectDumper.Write(string.Format("Price:{0,5:N}\tProduct:{1}", b.Price, b.ProductName));
                }
            }
        }
        public string GetTypeofPrice(decimal UnitPrice)
        {
            if (UnitPrice < 30) { return "Very cheap"; }
            else if (UnitPrice <= 50) { return "Cheap"; }
            else { return "Expensive"; };
        }

        [Category("Restriction Operators")]
        [Title("Task 9")]
        [Description("Рассчитайте среднюю прибыльность каждого города (среднюю сумму заказа по всем клиентам из данного города) и среднюю интенсивность (среднее количество заказов, приходящееся на клиента из каждого города)")
        ]
        public void Linq9()
        {
            var averTotal = dataSource
            .Customers
            .Where(c => c.Orders.Any())
            .Select(c => new
            {
                AverageTotal = c.Orders.Average(t => t.Total),
                Customer = c.CustomerID,
                City = c.City
            });

            var averTotalOrders = dataSource
                .Customers
                .Where(c => c.Orders.Any())
                .Select(c => new
                {
                    OrdersCount = c.Orders.Count(),
                    Customer = c.CustomerID,
                    City = c.City
                })
                .Join(
                averTotal,
                x => x.Customer,
                y => y.Customer,
                (x, y) => new
                {
                    City = x.City,
                    OrdersCount = x.OrdersCount,
                    AverageTotal = y.AverageTotal
                })
                .GroupBy(x => x.City)
                .Select(x => new
                {
                    City = x.Key,
                    AvTotal = x.Average(y => y.AverageTotal),
                    AvOrders = x.Average(t => t.OrdersCount)
                });


            ObjectDumper.Write("Average order price and count in cities:");

            foreach (var avTO in averTotalOrders)
            {
                ObjectDumper.Write(string.Format("{0,15}\t\tOrders:{1:N}\tTotal:{2:N}\t", avTO.City, avTO.AvOrders, avTO.AvTotal));
            }
        }

        [Category("Restriction Operators")]
        [Title("Task 10")]
        [Description("Сделайте среднегодовую статистику активности клиентов по месяцам (без учета года), статистику по годам, по годам и месяцам (т.е. когда один месяц в разные годы имеет своё значение).")
        ]
        public void Linq10()
        {
            //Статистика по месяцам
            var StatisticForMonth = dataSource.Customers
                   .SelectMany(x => x.Orders, (a, b) => new { Month = b.OrderDate.Month, OrderID = b.OrderID })
                   .GroupBy(x => x.Month)
                   .Select(x => new { Count = x.Select(y => y.OrderID).Count(), Month = x.Key })
                   .OrderBy(x => x.Month);

            ObjectDumper.Write(string.Format("{0}Statistics by months:", Environment.NewLine));
            foreach (var stat in StatisticForMonth)
                ObjectDumper.Write(string.Format("Month{0,5}\tOrders:{1}", stat.Month, stat.Count));

            //Статистика по годам 
            var StatisticForYear = dataSource.Customers
                  .SelectMany(x => x.Orders, (a, b) => new { Year = b.OrderDate.Year, OrderID = b.OrderID })
                  .GroupBy(x => x.Year)
                  .Select(x => new { Count = x.Select(y => y.OrderID).Count(), Year = x.Key })
                  .OrderBy(x => x.Year);


            ObjectDumper.Write(string.Format("{0}Statistics by years:", Environment.NewLine));
            foreach (var stat in StatisticForYear)
                ObjectDumper.Write(string.Format("Year{0,5}\tOrders:{1}", stat.Year, stat.Count));

            //Статистика по годам и по месяцам
            var StatisticForMonthYear = dataSource.Customers
                  .SelectMany(x => x.Orders, (a, b) => new
                  {
                      Month = b.OrderDate.Month,
                      Year = b.OrderDate.Year,
                      OrderID = b.OrderID
                  })
                  .GroupBy(x => x.Year)
                  .Select(x => new
                  {
                      Year = x.Key,

                      Orders = x.GroupBy(y => y.Month)
                                .Select(y => new
                                {
                                    Count = y.Count(),
                                    Month = y.Key
                                })
                                .OrderBy(g => g.Month)
                  })
                  .OrderBy(x => x.Year);


            ObjectDumper.Write(string.Format("{0}Statistics by years and months:", Environment.NewLine));
            foreach (var yearIt in StatisticForMonthYear)
            {
                ObjectDumper.Write(string.Format("{0}Year-{1}", Environment.NewLine, yearIt.Year));
                foreach (var monthIt in yearIt.Orders)
                    ObjectDumper.Write(string.Format("\tMonth{0,3}    Orders{1,3}", monthIt.Month, monthIt.Count));
            }

        }
    }
}