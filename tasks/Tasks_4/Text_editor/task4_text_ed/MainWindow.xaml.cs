﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Documents;

namespace task4_text_ed
{
    /// <summary>
    /// MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /// <summary>
        /// provides interface to save or open files
        /// </summary>
        public interface IToMemory<T> : IDisposable
        {
            T LoadData();
            T SaveData();
        }

        /// <summary>
        /// provides specific exceptions
        /// </summary>
        public class AppException : Exception
        {
            public AppException(String message) : base(message)
            { }

            public AppException(String message, Exception inner) : base(message, inner) { }
        }

        /// <summary>
        /// provides open save create files
        /// </summary>
        public class textFile : IToMemory<FileStream>
        {
            private string path;
            private string filename;
            public string Filename
            {
                get
                {
                    return filename;
                }

                private set
                {
                    filename = value;
                }
            }

            private FileStream textFileStream;

            /// <summary>
            /// constructor initializes path
            /// </summary>
            /// <param name="pathToTextFile">path to file</param>
            public textFile(string pathToTextFile)
            {
                path = pathToTextFile;

                if (path.Trim() == string.Empty) throw new AppException("Error: path is empty");

                try
                {
                    Filename = System.IO.Path.GetFileName(path);
                }
                catch (Exception e)
                {
                    throw new AppException("Error: path has forbidden chars", e);
                }                
            }


            /// <summary>
            /// Opens filestream to write to file 
            /// </summary>            
            /// <exception cref="AppException">Message about error</exception>
            /// <returns>FileStream</returns>
            public FileStream SaveData()
            {
                try
                {
                    textFileStream = File.Open(path, FileMode.Create);
                }
                catch (Exception e)
                {
                    throw new AppException("Error: Can't create file", e);
                }

                return textFileStream;
            }

            /// <summary>
            /// Opens filestream to read from file (open existing file or create new one)
            /// </summary>            
            /// <exception cref="AppException">Message about error</exception>
            /// <returns>FileStream</returns>
            public FileStream LoadData()
            {
                try
                {
                    textFileStream = File.Open(path, FileMode.Open);
                }
                catch (Exception e)
                {
                    throw new AppException("Error: Can't open file", e);
                }

                return textFileStream;
            }

            /// <summary>
            /// close filestream
            /// </summary>   
            public void Dispose()
            {
                if (textFileStream != null) textFileStream.Close();
            }

        }


        string Path;

        public MainWindow()
        {
            InitializeComponent();
        }


        private void btnSaveFile_Click(object sender, RoutedEventArgs e)
        {
            TextRange txtRng = new TextRange(currentFileRTB.Document.ContentStart, currentFileRTB.Document.ContentEnd);

            using (textFile textDocument = new textFile(Path))
            {
                txtRng.Save(textDocument.LoadData(), DataFormats.Text);
            };

        }

        private void btnSaveAsFile_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Filter = "Text files (*.txt)|*.txt";
            saveFileDlg.Title = "Save a Text document";
            saveFileDlg.DereferenceLinks = false;


            if (saveFileDlg.ShowDialog() == true)
            {
                try
                {
                    using (textFile textDocument = new textFile(saveFileDlg.FileName))
                    {
                        TextRange txtRng = new TextRange(currentFileRTB.Document.ContentStart, currentFileRTB.Document.ContentEnd);
                        try
                        {
                            txtRng.Save(textDocument.SaveData(), System.Windows.DataFormats.Text);
                        }
                        catch (AppException ee)
                        {
                            MessageBox.Show(ee.Message);
                        }
                    }
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Error to get text data");
                }
            }
        }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDlg = new OpenFileDialog();

            openDlg.Filter = "Текстовые файлы(.txt)|*.txt" + "|RichTextFormat|*.rtf" + "|Все файлы (.)|*.*";
            openDlg.CheckFileExists = true;
            openDlg.CheckPathExists = true;
            openDlg.Multiselect = false;

            if (openDlg.ShowDialog() == true)
            {

                Path = openDlg.FileName;
                
                try
                {
                    using (textFile textDocument = new textFile(Path))
                    {
                        Win.Title = "Document: " + textDocument.Filename;
                        TextRange txtRng = new TextRange(currentFileRTB.Document.ContentStart, currentFileRTB.Document.ContentEnd);
                        try
                        {
                            txtRng.Load(textDocument.LoadData(), DataFormats.Text);
                            btnSaveFile.IsEnabled = true;
                        }
                        catch (AppException ee)
                        {
                            MessageBox.Show(ee.Message);
                        }
                    }
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Error to get text data");
                }

            }
        }
    }
}

