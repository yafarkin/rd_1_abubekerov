﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using static task4_text_ed.MainWindow;

namespace task4_text_edTest
{
    [TestClass]
    public class task4TextEdTests
    {

        [TestMethod]
        // [ExpectedException(typeof(task4_text_ed.MainWindow.AppException))]
        // public textFile(string pathToTextFile)
        public void CheckSaveAndRead()
        {
            // arrange

            string srcText = "Testing";
            string resText = "";

            // act

            // writing to file
            using (textFile textDocument = new textFile("test.txt"))
            {                
                byte[] array = System.Text.Encoding.Default.GetBytes(srcText);               
                FileStream a = textDocument.SaveData();
                a.Write(array, 0, array.Length);
            }

            // read from file
            using (textFile textDocument = new textFile("test.txt"))
            {
                FileStream a = textDocument.LoadData();
                byte[] array = new byte[a.Length];
                a.Read(array, 0, array.Length);
                resText = System.Text.Encoding.Default.GetString(array);
            }

            // assert
            Assert.AreEqual(srcText, resText);

        }

        [TestMethod]
        [ExpectedException(typeof(task4_text_ed.MainWindow.AppException))]
        //public void SetPath(string pathToTextFile)    
        public void SetPath_empty()
        {
            textFile newFile = new textFile(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(task4_text_ed.MainWindow.AppException))]
        //public void SetPath(string pathToTextFile)    
        public void SetPath_wrongchars()
        {
            using (textFile newFile = new textFile("<")) { };
        }


        [TestMethod]
        [ExpectedException(typeof(task4_text_ed.MainWindow.AppException))]
        //public FileStream SaveFile()
        public void SaveFile_wrongpath()
        {
            string path = @"SuperDisc:\execution\text.txt";
            using (textFile newFile = new textFile(path))
            {
                FileStream a = newFile.SaveData();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(task4_text_ed.MainWindow.AppException))]
        //public FileStream OpenFile()
        public void OpenFile_wrongpath()
        {
            string path = @"SuperDisc:\execution\text.txt";
            using (textFile newFile = new textFile(path))
            {
                FileStream a = newFile.LoadData();
            }
        }

    }
}
