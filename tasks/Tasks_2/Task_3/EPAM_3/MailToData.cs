﻿using System.Collections.Generic;

namespace EPAM_3
{
    /// <summary>
    /// Provides save and load user's data : Name , City, State, Zip
    /// connect fields to one , and handle similar fields string.
    /// </summary>
    class MailToData
    {
        private static readonly int mixFieldItemsCnt = 3;
        private static readonly char separator = ',';

        private string name;
        private string city;
        private string state;
        private string zip;
       
        public string Address
        {
            get
            {
                return GetFullAddress();
            }
        }
        public string Name
        {
            get
            {
                return name;
            }

            private set
            {
                name = value;
            }
        }
        public string City
        {
            get
            {
                return city;
            }

            private set
            {
                city = value;
            }
        }
        public string State
        {
            get
            {
                return state;
            }

            private set
            {
                state = value;
            }
        }
        public string Zip
        {
            get
            {
                return zip;
            }

            private set
            {
                zip = value;
            }
        }


        /// <summary>
        /// Checks user's strings and writes it to fields
        /// </summary>
        /// <param name="nameIn">person name </param>
        /// <param name="addressIn">person address - City State Zip</param>                  
        /// <returns>true - if data is correct and wtitten to fields,  false - if data is corrupt </returns>
        public bool TrySaveData(string nameIn, string addressIn)
        {
            var bEmptyFieldExists = false;

            nameIn = nameIn.Trim();           
            addressIn = addressIn.Trim();

            var fields = new List<string> { };
            fields.Add(nameIn);            
            fields.Add(addressIn);

            foreach (string s in fields) if (s == string.Empty) { bEmptyFieldExists = true; };

            if (!bEmptyFieldExists)
            {
                if (TryParseMixField(addressIn))
                {
                    Name = nameIn;                   
                    return true;
                }
            }

            ClearFields();
            return false;
        }

        /// <summary>
        /// Splits string using separator and checks if all fields correct
        /// If so , saves data to fields
        /// </summary>
        /// <param name="mixFieldsStr">person separated string - City State Zip </param>                  
        /// <returns>true - if data is correct and wtitten to fields,  false - if data is corrupt </returns>
        private bool TryParseMixField(string mixFieldsStr)
        {
            var fieldsLst = new List<string> { };

            fieldsLst.AddRange(mixFieldsStr.Split(separator));

            if (fieldsLst.Count == mixFieldItemsCnt)
            {
                var bEmptyStrExists = false;

                foreach (string s in fieldsLst) if (s.Trim() == string.Empty) { bEmptyStrExists = true; };

                if (!bEmptyStrExists)
                {
                    City = fieldsLst[0].Trim();
                    State = fieldsLst[1].Trim();
                    Zip = fieldsLst[2].Trim();

                    int zipcode;
                    if(!int.TryParse(Zip,out zipcode)) return false;

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Clears all fields
        /// </summary>        
        public void ClearFields()
        {          
            Zip=State = City = Name = "<no data>";      
        }

        /// <summary>
        /// Connect fields  City, State, Zip to one string and returns it            
        /// </summary>                      
        /// <returns>string : connected fields</returns>
        public string GetFullAddress()
        {
            return string.Format("{0} , {1} , {2}", City, State, Zip);
        }
    }
}
