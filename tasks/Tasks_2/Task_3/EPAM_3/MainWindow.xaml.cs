﻿using System.Windows;


namespace EPAM_3
{
    /// <summary>
    /// interaction logic of MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        MailToData mailtodata = new MailToData();

        /// <summary>
        /// MainWindow constructor
        /// </summary> 
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Save button handler - writes new values of fields to MailToData object
        /// </summary>
        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            var name = txtbx_name.Text;           
            var address = txtbx_mixfield.Text;            

            if (!mailtodata.TrySaveData(name, address)) MessageBox.Show("Wrong values , please correct fields");         


            var nameFromMailData = mailtodata.Name;
            var addressFromMailData = mailtodata.Address;

            txtbx_name_show.Text = nameFromMailData;
            txtbx_mixfield_show.Text = addressFromMailData;
        }

        private void txtbx_name_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = txtbx_name.Text.Length > 50;
        }

        private void txtbx_mixfield_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = txtbx_mixfield.Text.Length > 60;
        }
    }
}
