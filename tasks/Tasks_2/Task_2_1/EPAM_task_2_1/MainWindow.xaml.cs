﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Globalization;


namespace EPAM_task_2_1
{
    /// <summary>
    /// Interaction logic of MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        readonly string charsToHandle = string.Format("0123456789 {0}", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

      
        /// <summary>
        /// MainWindow constructor             
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// handler of textbox for N input            
        /// </summary>
        private void txtbx_N_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = charsToHandle.IndexOf(e.Text) < 0;
        }

        /// <summary>
        /// handler of button to run calculating
        /// </summary>
        private void btn_Run_Click(object sender, RoutedEventArgs e)
        {
            txtbx_mathpow_res.Clear();
            txtbx_own_Pow_res.Clear();
            txtbx_diff.Clear();

            double val, power, accuracy;

            if (
                !double.TryParse(txtbx_N.Text, out power) ||
                !double.TryParse(txtbx_A.Text, out val) ||
                !double.TryParse(txtbx_accuracy.Text, out accuracy)
                )
            {
                MessageBox.Show("Wrong values, try again");
                return;
            }

            EquationCalc equation = EquationCalc.CreateInstance(val, power, accuracy);
            if (equation != null)
            {
                var resManualPow = equation.CalcEquation();
                var resPowMath = Math.Pow(val, 1 / power);               
                var resDiff = Math.Abs(resPowMath - resManualPow);

                string resformat = "F15";
                txtbx_mathpow_res.Text = resPowMath.ToString(resformat);
                txtbx_own_Pow_res.Text = resManualPow.ToString(resformat);
                txtbx_diff.Text = resDiff.ToString(resformat);
            }
            else
            {
                MessageBox.Show(String.Format("Please enter correct values{0} 0<=n<=100 {0} 0<=A<=1000 {0} 0.00000001<=accuracy<=100 ", Environment.NewLine));
            }
        }

        /// <summary>
        /// handler of textbox for A input
        /// </summary>
        private void txtbx_A_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = charsToHandle.IndexOf(e.Text) < 0;
        }

        /// <summary>
        /// handler of textbox for accurancy input
        /// </summary>
        private void txtbx_accuracy_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = charsToHandle.IndexOf(e.Text) < 0;
        }

    }
}

