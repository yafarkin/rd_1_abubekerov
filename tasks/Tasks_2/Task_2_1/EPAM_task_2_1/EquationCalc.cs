﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPAM_task_2_1
{
    /// <summary>
    /// Provides calculation the square root of the number of degree N
    /// </summary>
    class EquationCalc
    {
        double value, power, accuracy;

        /// <summary>
        /// Constructor - creates object of EquationCalc class
        /// </summary>
        /// <param name="value">value under the root </param>
        /// <param name="power"> the power of root  </param>
        /// <param name="accuracy">accuracy of calculating </param>            
        private EquationCalc(double value, double power, double accuracy)
        {
            this.value = value;
            this.power = power;
            this.accuracy = accuracy;
        }

        /// <summary>
        /// Creates an instance of EquationCalc's default constructor.
        /// Parameters range:
        /// power    range is [0..100]
        /// value    range is [0..1000]
        /// accuracy range is [0..100]
        /// </summary>
        /// <param name="value">value under the root </param>
        /// <param name="power"> the power of root  </param>
        /// <param name="accuracy">accuracy of calculating </param>
        /// <returns>reference to new EquationCalc Object or null if paraneters are not in range </returns>
        public static EquationCalc CreateInstance(double value, double power, double accuracy)
        {
            if (
                !(power >= 0 && power <= 100) ||
                !(value >= 0 && value <= 1000) ||
                !(accuracy >= 0.00000001 && accuracy <= 100)
              )
            {
                return null;
            }
            return new EquationCalc(value, power, accuracy);
        }

        /// <summary>
        /// raises a number to specified power (power rounded)
        /// </summary>
        /// <param name="value">value under the root </param>
        /// <param name="power"> the power of root  </param>            
        /// <returns>double number raised in specified power</returns>
        private double Pow(double value, double power)
        {
            power = Math.Round(power);

            if (value == 1) return 1;
            if (power == 1) return value;
            if (power == -1) return 1 / value;
            if (power == 0) return (value != 0) ? 1 : double.NaN;
                   
            value = value * Pow(value, Math.Abs(power) - 1);

            return (power > 0) ? value : (1 / value);
        }


        /// <summary>
        /// Calculates the root of the number using Newton's method (power rounded)            
        /// </summary>
        /// <param name="val">value under the root </param>
        /// <param name="power"> the power of root  </param>      
        /// <param name="accuracy">accuracy of calculating </param>
        /// <param name="x0">X0 - start value of x  </param>
        /// <returns>double number raised in specified power</returns>
        private double Root(double val, double power, double accuracy, double x0 = 1)
        {
            power = Math.Round(power);

            if (val == 0) return 0;
            if (val == 1) return 1;
            if (power == 1) return val;
            if (power == 0) return double.PositiveInfinity;

            double x1 = (1 / power) * ((  (power - 1) * x0 ) + (val / Pow(x0, power - 1)));               

            if ((Math.Abs(x0 - x1) <= accuracy)) return x1;

            return Root(val, power, accuracy, x1);
        }


        /// <summary>
        /// Runs calculation of the root using  Newton's method         
        /// </summary>     
        /// <returns>double: root by Newton's method</returns>
        public double CalcEquation()
        {            
            return Root(value, power, accuracy);           
        }

    }

}
