﻿using System;
using System.Windows;
using System.Windows.Input;


namespace EPAM_2_2
{
    /// <summary>
    /// Interaction logic of MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /// <summary>
        /// MainWindow constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// handler of textbox for number to convert
        /// </summary>
        private void txtbx_number_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        /// <summary>
        /// handler of button to run convertion        
        /// </summary>
        private void btn_run_Click(object sender, RoutedEventArgs e)
        {
            int number;

            txtbx_result.Clear();

            if (int.TryParse(txtbx_number.Text, out number) && number >= 0)
            {
                string binStr = NumberConverter.DecToBin(number);
                txtbx_result.Text = binStr;
            }
            else
            {
                MessageBox.Show(String.Format("Please enter correct value{0} min = 0 {0} max = {1}", Environment.NewLine, int.MaxValue));
            }
        }
    }
}
