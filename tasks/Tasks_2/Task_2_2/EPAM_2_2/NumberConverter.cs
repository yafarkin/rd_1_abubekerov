﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPAM_2_2
{
    /// <summary>
    /// Provide numbers convertion 
    /// </summary>       
    class NumberConverter
    {
        /// <summary>
        /// Converts the number from decimal system to binary
        /// </summary>
        /// <param name="number">number to convert </param>
        /// <returns>string that provide the binary representation of the number </returns>
        public static string DecToBin(int number)
        {
            if (number < 0) return string.Empty;
            if (number == 0) return number.ToString();

            StringBuilder result = new StringBuilder();
            double mod;

            while (number > 1)
            {
                mod = number % 2;

                if (mod > 0) result.Insert(0, "1"); else result.Insert(0, "0");

                number = number / 2;
            }
            result.Insert(0, "1");
            return result.ToString();
        }
    }

}
