﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace NewsProject.DAL.Interfaces
{
    
    /// <summary>
    /// Репозиторий для работы с новостями
    /// </summary>
    public class NewsRepository : IRepository<News>
    {
        private readonly IMyDbcontext _db;
        private bool _disposed;

        /// <summary>
        /// Конструктор принимает IMyDbcontext для контекста данных
        /// </summary>
        /// <param name="context">контекст данных</param>
        public NewsRepository(IMyDbcontext context)
        {
            _db = context;
        }

        /// <summary>
        /// Добавление новости
        /// </summary>
        /// <param name="item">новость</param>
        public void Create(News item)
        {
            _db.News.Add(item);
        }

        /// <summary>
        /// Удаление новости
        /// </summary>
        /// <param name="id">ID новости</param>
        public void Delete(int id)
        {
            var oneNew = _db.News.Find(id);
            if (oneNew != null)
                _db.News.Remove(oneNew);
        }

        
        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Ищет новость по условию
        /// </summary>
        /// <param name="predicate">условие</param>
        /// <returns></returns>
        public IEnumerable<News> Find(Func<News, bool> predicate)
        {
            return _db.News.Where(predicate).ToList();
        }


        /// <summary>
        /// Возвращае новость по ее ID
        /// </summary>
        /// <param name="id">ID новости</param>
        /// <returns></returns>
        public News Get(int id)
        {
            return _db.News.Find(id);
        }


        /// <summary>
        /// Возвращает IEnumerable всех новостей из контекста
        /// </summary>
        /// <returns></returns>
        public IEnumerable<News> GetAll()
        {
            return _db.News;
        }

        /// <summary>
        /// Сохраняет изменеия в контексте
        /// </summary>
        public void Save()
        {
            _db.SaveChanges();
        }

        /// <summary>
        /// Изменяет запись в контексте
        /// </summary>
        /// <param name="item">новость для изменения</param>
        public void Update(News item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }
    }
}