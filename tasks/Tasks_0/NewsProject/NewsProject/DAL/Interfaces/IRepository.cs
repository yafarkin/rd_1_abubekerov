﻿using System;
using System.Collections.Generic;

namespace NewsProject.DAL.Interfaces
{
    /// <summary>
    /// Обеспечивает типичные операции с данными
    /// </summary>
    /// <typeparam name="T">сущность</typeparam>
    public interface IRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> GetAll();        
        T Get(int id);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
        void Save();
    }

}