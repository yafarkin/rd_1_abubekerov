﻿namespace NewsProject.BAL.Interfaces
{
    /// <summary>
    /// Интерфейс сервиса работы с комментариями (используется в контроллерах)
    /// </summary>
    public interface ICommentsService
    {
        void AddComment(CommentsDto item);       
    }
}
