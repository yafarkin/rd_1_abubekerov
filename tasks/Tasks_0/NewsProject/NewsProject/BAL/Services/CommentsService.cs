﻿using NewsProject.BAL.Interfaces;
using NewsProject.DAL;
using NewsProject.DAL.Interfaces;


namespace NewsProject.BAL.Services
{
    /// <summary>
    /// Класс обеспечивает работу с коментариями (сокрывает взаимодействие с уровнем DAL)
    /// </summary>
    public class CommentsService : ICommentsService
    {
        private IMyDbcontext Db { get;}
        private  IRepository<Comments> RepoComments { get; set; }

        /// <summary>
        /// Конструктор обеспечивает IoC для создания контекста данных и репозитория комментариев
        /// </summary>
        /// <param name="db">контекст данных</param>
        /// <param name="repoComments">репозиторий комментариев</param>
        public CommentsService(IMyDbcontext db, IRepository<Comments> repoComments)
        {
            Db = db;
            RepoComments = repoComments;
        }

        /// <summary>
        /// Добавление комментария
        /// </summary>
        /// <param name="item"> DTO объект комментария</param>
        public void AddComment(CommentsDto item)
        {
            IRepository<Comments> repoComments = new CommentsRepository(Db);

            var comment = item.MapCommentsDtoToComments();

            repoComments.Create(comment);
            repoComments.Save();
        }
    }
}