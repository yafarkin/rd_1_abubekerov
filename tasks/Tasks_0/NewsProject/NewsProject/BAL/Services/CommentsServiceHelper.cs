﻿using NewsProject.DAL;

namespace NewsProject.BAL.Services
{
    /// <summary>
    ///     Методы расширения для класса сервиса для работы с комментариями
    ///     Обеспечивают маппинг для взаимодействия между уровнями BAL И DAL
    /// </summary>
    public static class CommentsServiceHelper
    {
        /// <summary>
        ///     Производит маппинг типа Comments на тип CommentsDto
        /// </summary>
        /// <param name="comments">объект типа Comments</param>
        /// <returns>объект типа CommentsDto</returns>
        public static CommentsDto MapCommentsToCommentsDto(this Comments comments)
        {
            return new CommentsDto
            {
                AuthorName = comments.AuthorName,
                Body = comments.Body,
                CreationDate = comments.CreationDate,
                Id = comments.Id,
                NewsId = comments.NewsId
            };
        }

        /// <summary>
        ///     Производит маппинг типа CommentsDto  на тип Comments
        /// </summary>
        /// <param name="comments">объект типа CommentsDto</param>
        /// <returns>объект типа Comments</returns>
        public static Comments MapCommentsDtoToComments(this CommentsDto comments)
        {
            return new Comments
            {
                AuthorName = comments.AuthorName,
                Body = comments.Body,
                CreationDate = comments.CreationDate,
                Id = comments.Id,
                NewsId = comments.NewsId
            };
        }
    }
}