﻿using System.ComponentModel.DataAnnotations;

namespace NewsProject.BAL
{
    /// <summary>
    /// Класс комментариев для передачи между уровнем View и Controller (используется в моделях передачи данных в View)
    /// </summary>
    public class CommentsDto
    {
        [Required]        
        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Поле {0} должно содержать не менее {2} символов.", MinimumLength = 1)]
        [Display(Name = "Комментарий")]
        public string Body { get; set; }
      

        [Required]
        [StringLength(100, ErrorMessage = "Поле {0} должно содержать не менее {2} символов.", MinimumLength = 1)]
        [Display(Name = "Автор")]
        public string AuthorName { get; set; }
        
        [Required]
        [DataType(DataType.DateTime)]
        public System.DateTime CreationDate { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int NewsId { get; set; }
        
    }
}