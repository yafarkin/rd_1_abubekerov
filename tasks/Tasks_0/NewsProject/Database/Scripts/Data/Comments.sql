﻿SET IDENTITY_INSERT dbo.Comments ON
DECLARE @Comments TABLE(
	[Id] INT   NOT NULL PRIMARY KEY, 
    [Body] NTEXT NOT NULL, 
    [CreationDate] DATETIME NOT NULL, 
    [NewsId] INT NOT NULL,
	[AuthorName] NVARCHAR(100) NOT NULL
)


INSERT INTO @Comments ([Id], [Body], [CreationDate], [NewsId],[AuthorName]) VALUES
(1,N'Спасибо очень познавательно','2016-01-03', 1, N'Николай Басков'),
(2,N'так и не понял что хотел сказать автор','2016-01-03', 1, N'Альберт Эйнштейн'),
(3,N'ну и ну','2016-01-03', 2, N'Виктор Франкл'),
(4,N'да видимо это очень крутая новость','2016-01-03', 2, N'Филлип Киркоров'),
(5,N'Куда катится мир','2016-01-03', 3, N'Владимир Владимирович')



MERGE INTO [Comments] AS [target]
USING @Comments AS [source] ON [target].[Id] = [source].[Id]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Body]	= [source].[Body],
			[target].[CreationDate] = [source].[CreationDate],
			[target].[NewsId] = [source].[NewsId],
			[target].[AuthorName] = [source].[AuthorName]

			 
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id], [Body], [CreationDate], [NewsId],[AuthorName]) 
	VALUES ([source].[Id], [source].[Body], [source].[CreationDate],[source].[NewsId],[source].[AuthorName]);

SET IDENTITY_INSERT dbo.Comments OFF
GO

