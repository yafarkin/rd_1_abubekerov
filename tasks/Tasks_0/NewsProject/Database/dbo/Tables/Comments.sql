﻿CREATE TABLE [dbo].[Comments]
(
	[Id] INT  IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [Body] NTEXT NOT NULL, 
    [CreationDate] DATETIME NOT NULL, 
    [NewsId] INT NOT NULL, 
    [AuthorName] NVARCHAR(100) NOT NULL, 
    CONSTRAINT [FK_Comments_ToTable_News] FOREIGN KEY ([NewsId]) REFERENCES [News]([Id])
)
