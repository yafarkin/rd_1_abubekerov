﻿using System;
using NewsProject.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using NewsProject.BAL;
using NewsProject.BAL.Services;
using NewsProject.BAL.Interfaces;
using NewsProject.DAL.Interfaces;

namespace UnitTests
{
    [TestClass]
    public class CommentsServiceTests
    {
        private static readonly IMyDbcontext Db = new NewsDatabaseEntities();
        private static readonly IRepository<Comments> RepoComments = new CommentsRepository(Db);
        private static readonly ICommentsService Service = new CommentsService(Db, RepoComments);

        [TestMethod]
        // тестирование сервиса Comments
        // метод: public void AddComment(CommentsDto item)
        public void CommentsService_AddComment()
        {
            //Arrange     
            const string testAuthor = "тестовыйАвтор";
            var comment = new CommentsDto { AuthorName = testAuthor, Body = "", CreationDate = DateTime.Now, NewsId = 1 };

            //Act
            Service.AddComment(comment);

            var result = Db.Comments.FirstOrDefault(c => c.AuthorName == testAuthor);
            Db.Comments.Remove(result);
            Db.SaveChanges();

            //Assert
            Assert.AreEqual(testAuthor, result.AuthorName);
        }


        [TestMethod]
        // тестирование сервиса Comments
        // метод:  public static CommentsDto MapCommentsToCommentsDTO(this Comments _comments)
        public void CommentsService_MapCommentsToCommentsDTO()
        {
            //Arrange
            const string testAuthor = "тестовый автор";
            var src = new CommentsDto { AuthorName = testAuthor, Body = "", CreationDate = DateTime.Now, NewsId = 1 };
            var dest = new Comments();

            //Act
            dest = src.MapCommentsDtoToComments();

            //Assert
            Assert.AreEqual(src.AuthorName, dest.AuthorName);
            Assert.AreEqual(src.Body, dest.Body);
            Assert.AreEqual(src.CreationDate, dest.CreationDate);
            Assert.AreEqual(src.Id, dest.Id);
            Assert.AreEqual(src.NewsId, dest.NewsId);
        }

        [TestMethod]
        // тестирование сервиса Comments
        // метод:   public static Comments MapCommentsDTOToComments(this CommentsDto _comments)
        public void CommentsService_MapCommentsDTOToComments()
        {
            //Arrange
            const string testAuthor = "тестовый автор";
            var src = new Comments { AuthorName = testAuthor, Body = "", CreationDate = DateTime.Now, NewsId = 1 };
            var dest = new CommentsDto();

            //Act
            dest = src.MapCommentsToCommentsDto();

            //Assert
            Assert.AreEqual(src.AuthorName, dest.AuthorName);
            Assert.AreEqual(src.Body, dest.Body);
            Assert.AreEqual(src.CreationDate, dest.CreationDate);
            Assert.AreEqual(src.Id, dest.Id);
            Assert.AreEqual(src.NewsId, dest.NewsId);
        }
    }
}
