﻿using System.Configuration;

namespace TaskXML
{   
      /// <summary>
      /// Describes config section in App.config
      /// </summary>
      public class ParametersConfigSection : ConfigurationSection
      {
        [ConfigurationProperty("FileNameToSave")]
        public string FileNameToSave
        {
            get { return ((string)(base["FileNameToSave"])); }
            set { base["FileNameToSave"] = value; }
        }

        [ConfigurationProperty("FileNameToLoad")]
        public string FileNameToLoad
        {
            get { return ((string)(base["FileNameToLoad"])); }
            set { base["FileNameToLoad"] = value; }
        }
    }      
}
