﻿using System.Xml.Linq;

namespace TaskXML
{
    /// <summary>
    /// Interface for create and save documents using different generators
    /// </summary>
    interface IGenerator
    {
        void CreateDocument();
        void SaveDocument(string Path);
    }
    
    /// <summary>
    /// Provides implementation IGenerator for XML documents
    /// </summary>
    class XMLGenerator : IGenerator
    {
        public XDocument Xdoc { get; set; }

        public void CreateDocument()
        {
            Xdoc = new XDocument(new XElement("CATALOG",
            new XElement("PLANT",
            new XElement("COMMON", "Cowslip", new XAttribute("isPoison", "true")),
            new XElement("BOTANICAL", "Caltha palustris"),
            new XElement("ZONE", "4"),
            new XElement("LIGHT", "Mostly Shady"),
            new XElement("PRICE", "9.90", new XAttribute("dimension", "dollars")),
            new XElement("AVAILABILITY", "10")
            ),
            new XElement("PLANT",
            new XElement("COMMON", "Dutchman's-Breeches", new XAttribute("isPoison", "false")),
            new XElement("BOTANICAL", "Dicentra cucullaria"),
            new XElement("ZONE", "3"),
            new XElement("LIGHT", "Mostly Shady"),
            new XElement("PRICE", "6.44", new XAttribute("dimension", "dollars")),
            new XElement("AVAILABILITY", "234")
            ),
            new XElement("PLANT",
            new XElement("COMMON", "Ginger, Wild", new XAttribute("isPoison", "true")),
            new XElement("BOTANICAL", "Asarum canadense"),
            new XElement("ZONE", "3"),
            new XElement("LIGHT", "Mostly Shady"),
            new XElement("PRICE", "9.03", new XAttribute("dimension", "euros")),
            new XElement("AVAILABILITY", "3")
            ),
            new XElement("PLANT",
            new XElement("COMMON", "Hepatica", new XAttribute("isPoison", "false")),
            new XElement("BOTANICAL", "Hepatica americana"),
            new XElement("ZONE", "4"),
            new XElement("LIGHT", "Mostly Shady"),
            new XElement("PRICE", "4.45", new XAttribute("dimension", "dollars")),
            new XElement("AVAILABILITY", "56")
            ),
            new XElement("PLANT",
            new XElement("COMMON", "Liverleaf", new XAttribute("isPoison", "false")),
            new XElement("BOTANICAL", "Hepatica americana"),
            new XElement("ZONE", "4"),
            new XElement("LIGHT", "Mostly Shady"),
            new XElement("PRICE", "3.99", new XAttribute("dimension", "euros")),
            new XElement("AVAILABILITY", "37")
            )));
        }

        public void SaveDocument(string Path)
        {
            Xdoc.Save(Path);
        }

    }

    /// <summary>
    /// Provides implementation IGenerator for other documents
    /// </summary>
    class SMTGenerator : IGenerator
    {
        public void CreateDocument() { }
        public void SaveDocument(string Path) { }
    }

    /// <summary>
    /// Provides actions for document
    /// </summary>
    class DocumentGenerator
    {
        IGenerator generator;

        /// <summary>
        /// constructor that creates generator of target type
        /// </summary>
        /// <param name="_generator">target generator</param>
        public DocumentGenerator(IGenerator _generator)
        {
            generator = _generator;
        }

        /// <summary>
        /// Generating document using target generator
        /// </summary>
        public void GenerateDocument()
        {
            generator.CreateDocument();
        }

        /// <summary>
        /// Save document
        /// </summary>
        /// <param name="_path">path to save file</param>
        public void SaveDocument(string _path)
        {
            generator.SaveDocument(_path);
        }
    }
}
