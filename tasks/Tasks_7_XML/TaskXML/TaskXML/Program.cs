﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;

namespace TaskXML
{
    class Program
    {
        /// <summary>
        /// Output Catalog object to console
        /// </summary>
        /// <param name="_catalog">Catalog object to show</param>
        public static void ShowCatalog(Catalog _catalog)
        {
            Plants[] _plant = _catalog.Plant;

            for (int index = 0; index < _plant.Length; index++)
            {
                Console.WriteLine("{0}PLANT[{1}]:", Environment.NewLine, index);
                Console.WriteLine("--- isPoison:{0}", _plant[index].Comon.isPoison);
                Console.WriteLine("--- Value:{0}", _plant[index].Comon.Value);
                Console.WriteLine("--- Botanical:{0}", _plant[index].Botanical);
                Console.WriteLine("--- Zone:{0}", _plant[index].Zone);
                Console.WriteLine("--- Light:{0}", _plant[index].Light);
                Console.WriteLine("--- Dimension:{0}", _plant[index].Price.dimension);
                Console.WriteLine("--- Value:{0}", _plant[index].Price.Value);
                Console.WriteLine("--- Availability:{0}", _plant[index].Availability);
            }
        }

        /// <summary>
        /// Serialize Catalog object to specified file
        /// </summary>
        /// <param name="_catalog">Catalog object</param>
        /// <param name="_path">file path</param>
        public static void SerializeCatalogToFile(Catalog _catalog, string _path)
        {          
            XmlSerializer formatter = new XmlSerializer(typeof(Catalog));
           
            using (FileStream fs = new FileStream(_path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, _catalog);
                Console.WriteLine("{0}Объект сериализован{0}", Environment.NewLine);
            }
        }

        /// <summary>
        /// Deserialize Catalog object from specified file path
        /// </summary>
        /// <param name="_catalog">Catalog object</param>
        /// <param name="_path">file path</param>
        public static void DeserializeCatalogFromFile(ref Catalog _catalog, string _path)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Catalog));

            using (FileStream fs = new FileStream(_path, FileMode.OpenOrCreate))
            {
                _catalog = (Catalog)formatter.Deserialize(fs);
                Console.WriteLine("{0}Объект десериализован{0}", Environment.NewLine);
            }
        }

     
        public static void Main(string[] args)
        {

            var FileNames = (ParametersConfigSection)ConfigurationManager.GetSection("FileNames");
            var GeneratedXMLPath = FileNames.FileNameToLoad;
            var SerialisedXMLPath = FileNames.FileNameToSave;

            XMLGenerator generator = new XMLGenerator();
            DocumentGenerator document = new DocumentGenerator(generator);
            document.GenerateDocument();
            document.SaveDocument(GeneratedXMLPath);

            Catalog newCatalog = new Catalog();
            DeserializeCatalogFromFile(ref newCatalog, GeneratedXMLPath);
            ShowCatalog(newCatalog);
            SerializeCatalogToFile(newCatalog, SerialisedXMLPath);
            ShowCatalog(newCatalog);

            Console.ReadKey();
        }
    }
}
