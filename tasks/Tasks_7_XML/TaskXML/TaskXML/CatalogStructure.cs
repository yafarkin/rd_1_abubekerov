﻿using System;
using System.Xml.Serialization;

namespace TaskXML
{
    /// <summary>
    /// Describes section COMMON
    /// </summary>
    public class Common
    {
        private bool isPoisonField;
        private string valueField;

        [XmlAttributeAttribute()]
        public bool isPoison
        {
            get
            {
                return this.isPoisonField;
            }
            set
            {
                this.isPoisonField = value;
            }
        }

        [XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <summary>
    /// Describes section PRICE
    /// </summary>
    public class Prices
    {
        private string DimensionField;
        private decimal valueField;

        [XmlAttributeAttribute()]
        public string dimension
        {
            get
            {
                return this.DimensionField;
            }
            set
            {
                this.DimensionField = value;
            }
        }

        [XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <summary>
    /// Describes section PLANT
    /// </summary>
    public class Plants
    {

        private Common com = new Common();
        private Prices _price = new Prices();

        [XmlElement("COMMON")]
        public Common Comon
        {
            get { return com; }
            set { com = value; }
        }

        [XmlElement("BOTANICAL")]
        public string Botanical { get; set; }

        [XmlElement("ZONE")]
        public int Zone { get; set; }

        [XmlElement("LIGHT")]
        public string Light { get; set; }

        [XmlElement("PRICE")]
        public Prices Price
        {
            get { return _price; }
            set { _price = value; }
        }

        [XmlElement("AVAILABILITY")]
        public int Availability { get; set; }
    }

    /// <summary>
    /// Describes root section CATALOG
    /// </summary>
    [Serializable]
    [XmlRoot("CATALOG")]
    public class Catalog
    {
        private Plants[] _plant;

        [XmlElement("PLANT")]
        public Plants[] Plant
        {
            get { return _plant; }
            set { _plant = value; }
        }
     
        public Catalog()
        { }

        public Catalog(int plantcount)
        {
            Plant = new Plants[5];
        }
    }
}
